
<div class="sucuriscan-tabs">
    <ul>
        <li>
            <a href="#" data-tabname="settings-general">General Settings</a>
        </li>
        <li>
            <a href="#" data-tabname="settings-notifications">Notification Settings</a>
        </li>
        <li>
            <a href="#" data-tabname="settings-ignorerules">Ignore Notifications</a>
        </li>
    </ul>

    <div class="sucuriscan-tab-containers">
        <div id="sucuriscan-settings-general">
            %%SUCURI.Settings.General%%
        </div>

        <div id="sucuriscan-settings-notifications">
            %%SUCURI.Settings.Notifications%%
        </div>

        <div id="sucuriscan-settings-ignorerules">
            %%SUCURI.Settings.IgnoreRules%%
        </div>
    </div>
</div>
