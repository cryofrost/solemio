jQuery(function($) {
    var // Progress Tracker
        $progress_tracker_option = $('#common_settings').find('input:first'),
        showOrHideProgressTracker = function() {
            $progress_tracker_option.is(':checked') ? $progress_tracker.show() : $progress_tracker.hide();
        },
        // Tabs
        $tabs = $('div.tabbable').find('.nav-tabs'),
        $tab_content = $('div.tab-content'),
        // Update Buttons
        $update_select_service_options = $('#select_service'),
        $update_convenient_time_options = $('#convenient_time'),
        $update_your_details_options = $('#your_details'),
        $update_payment_options = $('#payment'),
        $update_all_done_options = $('#all_done'),
        // Reset Buttons
        $reset_select_service_options = $('#select_service_reset'),
        $reset_convenient_time_options = $('#convenient_time_reset'),
        $reset_your_details_options = $('#your_details_reset'),
        $reset_payment_options = $('#payment_reset'),
        $reset_all_done_options = $('#all_done_reset'),
        // Booking Info
        $booking_info_select_service = $('#ab-booking-info-first'),
        $booking_info_time = $('#ab-booking-info-second'),
        $booking_info_your_details = $('#ab-booking-info-third'),
        $booking_info_payment = $('#ab-booking-info-fourth'),
        $booking_info_all_done = $('#ab-booking-info-fifth'),
        booking_info = {
            'select_service': $.trim($booking_info_select_service.val()),
            'time'          : $.trim($booking_info_time.val()),
            'your_details'  : $.trim($booking_info_your_details.val()),
            'payment'       : $.trim($booking_info_payment.val()),
            'all_done'      : $.trim($booking_info_all_done.val())
        }, // booking_info
        $color_picker = $('.wp-color-picker'),
        $progress_tracker = $('div.ab-progress-tracker'),
        $progress_tracker_li = $progress_tracker.find('li.ab-step-tabs'),
        // Step Options and Elements
        $first_step_options = $('div.ab-first-step-options'),
        first_step_options = {
            renderPreview: function () {
                var $preview = $('div.ab-booking-info-first-preview');
                $('.first-preview').show();
                $preview.show();
                $preview.html($.trim(escapeXSS($booking_info_select_service.val())));
            }, // renderPreview
            render : function() {
                // preview
                this.renderPreview();

                // display first step options
                $first_step_options.show().find('div.ab-booking-details').show();
                $first_step_options.find('.controls').show();
                $('.ab-booking-form:eq(0)').show().find('div').show();

                // clickable week-days
                $('.ab-week-day').on('change', function () {
                    var self = $(this);
                    if (self.is(':checked') && !self.parent().hasClass('active')) {
                        self.parent().addClass('active');
                    } else if (self.parent().hasClass('active')) {
                        self.parent().removeClass('active')
                    }
                });

                // render Progress Tracker
                $progress_tracker_li.filter(':eq(0)').addClass('active');
                showOrHideProgressTracker();
            } // render
        }, // first_step_options
        $second_step_options = $('div.ab-second-step-options'),
        second_step_options = {
            renderPreview: function () {
                var $preview = $('div.ab-booking-info-second-preview');

                $preview.show();
                $preview.html($.trim(escapeXSS($booking_info_time.val())));
            }, // renderPreview
            render : function() {
                // preview
                this.renderPreview();

                // display second step options
                $second_step_options.show().find('div.ab-booking-details').show();
                $second_step_options.find('.controls').show();
                $second_step_options.find('.ab-shortcodes-info').show();

                // render Progress Tracker
                $('.ab-booking-form:eq(1)').show().find('div').show();
                $('div.pt').html('').append($progress_tracker.clone()).show().find('div').show();
                $('div.ab-progress-tracker').filter(':visible').find('li.ab-step-tabs').filter(':eq(1)').addClass('active');
                colorPicker();
                showOrHideProgressTracker();
            }, // render
            renderTimeSlots: function() {
                var $time_slots_wrapper = $('div.ab-columnizer-wrap'),
                    $buttons = $('div.ab-time-buttons');

                $time_slots_wrapper.show().find('div').show();
                $buttons.show();
            }
        }, // second_step_options
        $third_step_options = $('div.ab-third-step-options'),
        third_step_options = {
            renderBookingDetails: function() {
                var $booking_details = $('div.ab-booking-details');

                // display booking details
                $booking_details.show().find('div.ab-row-fluid').show();
                $booking_details.find('div.ab-shortcodes-info').show();
                $booking_details.find('div.ab-details-wrap').show();
            }, // renderBookingDetails
            renderPreview: function () {
                var $preview = $('div.ab-booking-info-third-preview');
                $preview.html($.trim(escapeXSS($booking_info_your_details.val()))).show();
            }, // renderPreview
            render: function() {
                // preview
                this.renderPreview();
                // display third step options
                $third_step_options.show().find('.controls').show();
                $('.ab-your-details-form').show().find('div').show();

                // render Progress Tracker
                $('.ab-booking-form:eq(2)').show().find('div').show();
                $('div.pt').html('').append($progress_tracker.clone()).show().find('div').show();
                $('div.ab-progress-tracker').filter(':visible').find('li.ab-step-tabs').filter(':eq(1), :eq(2)').addClass('active');
                colorPicker();
                showOrHideProgressTracker();
            } // render
        }, // third_step_options
        $fourth_step_options = $('div.ab-fourth-step-options'),
        fourth_step_options = {
            renderPaymentDetails: function() {
                var $payment_details = $('div.ab-payment');
                // display payment details
                $payment_details.show().find('div.ab-row-fluid, div.ab-local-pay-button').show();
                $('div.tab-pane-4').find('div.ab-booking-details').show();
            }, // renderPaymentDetails
            renderPreview: function () {
                var $preview = $('div.ab-booking-info-fourth-preview');

                $preview.show();
                $preview.html($.trim(escapeXSS($booking_info_payment.val())));
            }, // renderPreview
            render: function() {
                // preview
                this.renderPreview();
                // display fourth step options
                $fourth_step_options.show().find('.controls').show();

                // render Progress Tracker
                $('.ab-booking-form:eq(3)').show().find('div').show();
                $('div.pt').html('').append($progress_tracker.clone()).show().find('div').show();
                $('div.ab-progress-tracker').filter(':visible').find('li.ab-step-tabs').filter(':eq(1), :eq(2), :eq(3)').addClass('active');
                colorPicker();
                showOrHideProgressTracker();
            } // render
        }, // fourth_step_options
        $fifth_step_options = $('div.ab-fifth-step-options'),
        fifth_step_options = {
            renderPreview: function () {
                var $preview = $('div.ab-booking-info-fifth-preview');
                $preview.html($.trim(escapeXSS($booking_info_all_done.val()))).show();
            }, // renderPreview
            render: function() {
                // preview
                this.renderPreview();
                $fifth_step_options.show().find('div').show();

                // render Progress Tracker
                $('.ab-booking-form:eq(4)').show().find('div').show();
                $('div.pt').html('').append($progress_tracker.clone()).show().find('div').show();
                $('div.ab-progress-tracker').filter(':visible').find('li.ab-step-tabs').filter(':eq(1), :eq(2), :eq(3), :eq(4)').addClass('active');
                colorPicker();
                showOrHideProgressTracker();
            } // render
        }, // fifth_step_options
        escapeXSS = function (infected) {
            var regexp = /([<|(]("[^"]*"|'[^']*'|[^'">])*[>|)])/gi;
            return infected.replace(regexp, '');
        }; // escapeXSS

    // menu fix for WP 3.8.1
    $('#toplevel_page_ab-system > ul').css('margin-left', '0px');
    // enable/disable Progress Tracker Option
    $progress_tracker_option.on('change', function() {
        var self = $(this),
            progressTrackerOption = {
                data : {
                    action: 'ab_update_progress_tracker_options',
                    progress_tracker_enabled: Number(self.is(':checked'))
                }, // data
                response : function() {
                    return $.post(ajaxurl, this.data);
                }, // response
                save : function() {
                    this.response().done(function(response) {
                        var $response = $.parseJSON(response);
                        if ($response) {
                            self.prop('checked', !!$response.progress_tracker_enabled);
                            showOrHideProgressTracker();
                        }
                    });
                } // save
            }; // progressTrackerOption
        progressTrackerOption.save();
    }); // onchange

    // Tabs
    $tabs.find('.ab-step-tabs').on('click', function() {
        var $step_id = $(this).data('step-id');
            // hide all other tab content and show only current
            $tab_content.find('div[data-step-id!="' + $step_id + '"]').removeClass('active').hide();
            $tab_content.find('div[data-step-id="' + $step_id + '"]').addClass('active').show();
            // Render unique data per step
            switch ($step_id) {
                // Service
                case 1:
                    first_step_options.render();
                    break;
                // Time
                case 2:
                    second_step_options.render();
                    second_step_options.renderTimeSlots();
                    break;
                // Details
                case 3:
                    third_step_options.render();
                    third_step_options.renderBookingDetails();
                    break;
                // Payment
                case 4:
                    fourth_step_options.render();
                    fourth_step_options.renderPaymentDetails();
                    break;
                // Done
                case 5:
                    fifth_step_options.render();
                    break;
            }
    }).filter('li:first').trigger('click');

    // Color Picker
    var colorPicker = function() {
        var color_important = $color_picker.wpColorPicker('color') + '!important';
        $('div.ab-progress-tracker').find('li.ab-step-tabs').filter('.active').find('a').css('color', $color_picker.wpColorPicker('color'));
        $('div.ab-progress-tracker').find('li.ab-step-tabs').filter('.active').find('div.step').css('background', $color_picker.wpColorPicker('color'));
        $('.ab-mobile-step_1 label').css('color', $color_picker.wpColorPicker('color'));
        $('.ab-next-step').css('background', $color_picker.wpColorPicker('color'));
        $('.ab-week-days label').css('background-color', $color_picker.wpColorPicker('color'));
        $('.pickadate__calendar').attr('style', 'background: ' + color_important);
        $('.pickadate__header').attr('style', 'border-bottom: ' + '1px solid ' + color_important);
        /*$('.pickadate__nav--next, .pickadate__nav--prev').attr('style', 'border-left: 6px solid ' + color_important);*/
        $('.pickadate__nav--next:before').attr('style', 'border-left: 6px solid ' + color_important);
        $('.pickadate__nav--prev:before').attr('style', 'border-right: 6px solid ' + color_important);
        $('.pickadate__day:hover').attr('style', 'color: ' + color_important);
        $('.pickadate__day--selected:hover').attr('style', '');
        $('.pickadate__day--selected').attr('style', 'color: ' + color_important);
        $('.pickadate__button--clear').attr('style', 'color: ' + color_important);
        $('.pickadate__button--today').attr('style', 'color: ' + color_important);
        $('.ab-columnizer .ab-available-day').css({
            'background': $color_picker.wpColorPicker('color'),
            'border-color': $color_picker.wpColorPicker('color')
        });
        $('.ab-time-no-resize').css('background-color', $color_picker.wpColorPicker('color'));
        $('.ab-columnizer .ab-available-hour').hover(
            function() { // mouse-on
                $(this).css({
                    'color': $color_picker.wpColorPicker('color'),
                    'border': '2px solid ' + $color_picker.wpColorPicker('color')
                });
                $(this).find('.ab-hour-icon').css({
                    'border-color': $color_picker.wpColorPicker('color'),
                    'color': $color_picker.wpColorPicker('color')
                });
                $(this).find('.ab-hour-icon > span').css({
                    'background': $color_picker.wpColorPicker('color')
                });
            },
            function() { // mouse-out
                $(this).css({
                    'color': '#333333',
                    'border': '1px solid ' + '#cccccc'
                });
                $(this).find('.ab-hour-icon').css({
                    'border-color': '#333333',
                    'color': '#cccccc'
                });
                $(this).find('.ab-hour-icon > span').css({
                    'background': '#cccccc'
                });
            }
        );
        $('div.ab-details-list > label.ab-formLabel').css('color', $color_picker.wpColorPicker('color'));
        $('.ab-to-second-step, .ab-to-fourth-step, .ab-to-third-step, .ab-final-step')
            .css('background', $color_picker.wpColorPicker('color'));
    };
    $color_picker.wpColorPicker({
        change : function() {
            colorPicker();
        } 
    }); 
    colorPicker();

    $('.ab-requested-date-from').pickadate({
        dateMin: true,
        clear: false,
        onRender: function() {
            colorPicker();
        }
    });

    /**
     * Update Triggers
     */

    // Update Service Options
    $update_select_service_options.on('click', function() {
        var $controls = $('.ab-first-step-options').find('.controls'),
            $alert = $('.alert').clone(),
            data = {
                action: 'ab_update_appearance_options',
                step_id: 1,
                options: {
                    // booking form color
                    'ab_appearance_booking_form_color': $color_picker.wpColorPicker('color'),
                    // booking-info
                    'ab_appearance_first_step_booking_info': booking_info.select_service
                } // options
            }; // data

        // update data and show spinner while updating
        $.post(ajaxurl, data, function (response) {
            $controls.css('margin-right', '410px');
            $('#select_service_spinner').show().fadeOut(1700, function() {
                $controls.css('margin-right', '450px');
                if ($alert.is(':not(:visible)') && $alert.length <= 1) {
                    $('.ab-title').prepend($alert);
                    $alert.show();
                }
            });
        });
    });

    // Update Time Options
    $update_convenient_time_options.on('click', function() {
        var $controls = $('.ab-second-step-options').find('.controls'),
            $alert = $('.alert').clone(),
            data = {
                action: 'ab_update_appearance_options',
                step_id: 2,
                options: {
                    // booking form color
                    'ab_appearance_booking_form_color': $color_picker.wpColorPicker('color'),
                    // booking-info
                    'ab_appearance_second_step_booking_info': booking_info.time
                }
            }; // data

        // update data and show spinner while updating
        $.post(ajaxurl, data, function (response) {
            $controls.css('margin-right', '410px');
            $('#convenient_time_spinner').show().fadeOut(1700, function() {
                $controls.css('margin-right', '450px');
                if ($alert.is(':not(:visible)') && $alert.length <= 1) {
                    $('.ab-title').prepend($alert);
                    $alert.show();
                }
            });
        });
    });

    // Update Details Options
    $update_your_details_options.on('click', function() {
        var $controls = $('.ab-third-step-options').find('.controls'),
            $alert = $('.alert').clone(),
            data = {
                action: 'ab_update_appearance_options',
                step_id: 3,
                options: {
                    // booking form color
                    'ab_appearance_booking_form_color': $color_picker.wpColorPicker('color'),
                    // booking-info
                    'ab_appearance_third_step_booking_info': booking_info.your_details
                } // options
            }; // data

        // update data and show spinner while updating
        $.post(ajaxurl, data, function (response) {
            $controls.css('margin-right', '410px');
            $('#your_details_spinner').show().fadeOut(1700, function() {
                $controls.css('margin-right', '450px');
                if ($alert.is(':not(:visible)') && $alert.length <= 1) {
                    $('.ab-title').prepend($alert);
                    $alert.show();
                }
            });
        });
    });

    // Update Payment Options
    $update_payment_options.on('click', function() {
        var $controls = $('.ab-fourth-step-options').find('.controls'),
            $alert = $('.alert').clone(),
            data = {
                action: 'ab_update_appearance_options',
                step_id: 4,
                options: {
                    // booking form color
                    'ab_appearance_booking_form_color': $color_picker.wpColorPicker('color'),
                    // booking-info
                    'ab_appearance_fourth_step_booking_info': booking_info.payment
                } // options
            }; // data

        // update data and show spinner while updating
        $.post(ajaxurl, data, function (response) {
            $controls.css('margin-right', '410px');
            $('#payment_spinner').show().fadeOut(1700, function() {
                $controls.css('margin-right', '450px');
                if ($alert.is(':not(:visible)') && $alert.length <= 1) {
                    $('.ab-title').prepend($alert);
                    $alert.show();
                }
            });
        });
    });

    // Update Done Options
    $update_all_done_options.on('click', function() {
        var $controls = $('.ab-fifth-step-options').find('.controls'),
            $alert = $('.alert').clone(),
            data = {
                action: 'ab_update_appearance_options',
                step_id: 5,
                options: {
                    // booking form color
                    'ab_appearance_booking_form_color': $color_picker.wpColorPicker('color'),
                    // booking-info
                    'ab_appearance_fifth_step_booking_info': booking_info.all_done
                }
            }; // data

        // update data and show spinner while updating
        $.post(ajaxurl, data, function (response) {
            $controls.css('margin-right', '410px');
            $('#all_done_spinner').show().fadeOut(1700, function() {
                $controls.css('margin-right', '450px');
                if ($alert.is(':not(:visible)') && $alert.length <= 1) {
                    $('.ab-title').prepend($alert);
                    $alert.show();
                }
            });
        });
    });

    /**
     * Reset Triggers
     */

    // Reset Select Service Options
    $reset_select_service_options.on('click', function() {
        var defaults = {
                reset_booking_info: function() {
                    var info = escapeXSS($.trim($('.tab-pane-1').find('textarea').data('selected')))
                        .replace(/^\s*|\s*$/g,'');

                    $booking_info_select_service.empty().val(info);
                    $('.ab-booking-info-first-preview').html(info);
                }, // reset_booking_info
                reset_booking_form_color: function() {
                    $color_picker.wpColorPicker('color', $color_picker.data('selected'));
                },
                init: function() {
                    var getAllMethods = function(object) { // collect all reset-methods
                            return Object.getOwnPropertyNames(object).filter(function(property) {
                                return typeof object[property] === 'function';
                            });
                        }, // getAllMethods
                        all_methods = getAllMethods(this);
                    // remove `init` from all methods
                    all_methods.splice(-1, 1);
                    // call each reset-method
                    all_methods.forEach(function(method) {
                        eval('defaults.' + method + '();');
                    });
                } // init
        }; // defaults
        defaults.init();
    });

    // Reset Convenient Time Options
    $reset_convenient_time_options.on('click', function() {
        var defaults = {
                reset_booking_info: function() {
                    var info = escapeXSS($.trim($('.tab-pane-2').find('textarea').data('selected')))
                        .replace(/^\s*|\s*$/g,'');

                    $booking_info_time.empty().val(info);
                    $('.ab-booking-info-second-preview').html(info);
                }, //reset_booking_info
                reset_booking_form_color: function() {
                    $color_picker.wpColorPicker('color', $color_picker.data('selected'));
                },
                init: function() {
                    var getAllMethods = function(object) { // collect all reset-methods
                            return Object.getOwnPropertyNames(object).filter(function(property) {
                                return typeof object[property] === 'function';
                            });
                        }, // getAllMethods
                        all_methods = getAllMethods(this);
                    // remove `init` from all methods
                    all_methods.splice(-1, 1);
                    // call each reset-method
                    all_methods.forEach(function(method) {
                        if (method == 'reset_booking_info' || method == 'reset_slot_font_color') {
                            eval('defaults.' + method + '();');
                        } else {
                            for (var i = 0; i < 2; i++) {
                                eval('defaults.' + method + '();');
                            }
                        }
                    });
                } // init
        }; // defaults
        defaults.init();
    });

    // Reset Your Details Options
    $reset_your_details_options.on('click', function() {
        var defaults = {
            reset_booking_info: function() {
                var info = escapeXSS($.trim($('.tab-pane-3').find('textarea').data('selected')))
                    .replace(/^\s*|\s*$/g,'');

                $booking_info_your_details.empty().val(info);
                $('.ab-booking-info-third-preview').html(info);
            }, //reset_booking_info
            reset_booking_form_color: function() {
                $color_picker.wpColorPicker('color', $color_picker.data('selected'));
            },
            init: function() {
                var getAllMethods = function(object) { // collect all reset-methods
                        return Object.getOwnPropertyNames(object).filter(function(property) {
                            return typeof object[property] === 'function';
                        });
                    }, // getAllMethods
                    all_methods = getAllMethods(this);
                // remove `init` from all methods
                all_methods.splice(-1, 1);
                // call each reset-method
                all_methods.forEach(function(method) {
                    if (method == 'reset_booking_info' || method == 'reset_font_color') {
                        eval('defaults.' + method + '();');
                    } else {
                        for (var i = 0; i < 2; i++) {
                            eval('defaults.' + method + '();');
                        }
                    }
                });
            } // init
        };
        defaults.init();
    });

    // Reset Payment Options
    $reset_payment_options.on('click', function() {
        var defaults = {
            reset_booking_info: function() {
                var info = escapeXSS($.trim($('.tab-pane-4').find('textarea').data('selected')))
                    .replace(/^\s*|\s*$/g,'');

                $booking_info_payment.empty().val(info);
                $('.ab-booking-info-fourth-preview').html(info);
            }, //reset_booking_info
            reset_booking_form_color: function() {
                $color_picker.wpColorPicker('color', $color_picker.data('selected'));
            },
            init: function() {
                var getAllMethods = function(object) { // collect all reset-methods
                        return Object.getOwnPropertyNames(object).filter(function(property) {
                            return typeof object[property] === 'function';
                        });
                    }, // getAllMethods
                    all_methods = getAllMethods(this);
                // remove `init` from all methods
                all_methods.splice(-1, 1);
                // call each reset-method
                all_methods.forEach(function(method) {
                    if (method == 'reset_booking_info' || method == 'reset_font_color') {
                        eval('defaults.' + method + '();');
                    } else {
                        for (var i = 0; i < 2; i++) {
                            eval('defaults.' + method + '();');
                        }
                    }
                });
            } // init
        };
        defaults.init();
    });

    // Reset All Done Options
    $reset_all_done_options.on('click', function() {
        var defaults = {
            reset_booking_info: function() {
                var info = escapeXSS($.trim($('.tab-pane-5').find('textarea').data('selected')))
                    .replace(/^\s*|\s*$/g,'');

                $booking_info_all_done.empty().val(info);
                $('.ab-booking-info-fifth-preview').html(info);
            }, // reset_booking_info
            reset_booking_form_color: function() {
                $color_picker.wpColorPicker('color', $color_picker.data('selected'));
            },
            init: function() {
                var getAllMethods = function(object) { // collect all reset-methods
                        return Object.getOwnPropertyNames(object).filter(function(property) {
                            return typeof object[property] === 'function';
                        });
                    }, // getAllMethods
                    all_methods = getAllMethods(this);
                // remove `init` from all methods
                all_methods.splice(-1, 1);
                // call each reset-method
                all_methods.forEach(function(method) {
                    if (method == 'reset_booking_info' || method == 'reset_font_color') {
                        eval('defaults.' + method + '();');
                    } else {
                        for (var i = 0; i < 2; i++) {
                            eval('defaults.' + method + '();');
                        }
                    }
                });
            } // init
        };
        defaults.init();
    });

    /**
     * Booking Info Triggers
     */

    // Booking Info for Select Service
    $booking_info_select_service
        .on('change', function() { // pass to `booking_info` for saving
            booking_info.select_service = $.trim(escapeXSS($(this).val()));
        })
        .on('keyup', function () { // update preview
            $('div.ab-booking-info-first-preview').html(escapeXSS($(this).val()));
        });
    // Booking Info for Convenient Time
    $booking_info_time
        .on('change', function() { // pass to `booking_info` for saving
            booking_info.time = $.trim(escapeXSS($(this).val()));
        })
        .on('keyup', function () { // update preview
            $('div.ab-booking-info-second-preview').html(escapeXSS($(this).val()));
        });
    // Booking Info for Your Details
    $booking_info_your_details
        .on('change', function() { // pass to `booking_info` for saving
            booking_info.your_details = $.trim(escapeXSS($(this).val()));
        })
        .on('keyup', function () { // update preview
            $('div.ab-booking-info-third-preview').html(escapeXSS($(this).val()));
        });
    // Booking Info for Payment
    $booking_info_payment
        .on('change', function() { // pass to `booking_info` for saving
            booking_info.payment = $.trim(escapeXSS($(this).val()));
        })
        .on('keyup', function () { // update preview
            $('div.ab-booking-info-fourth-preview').html(escapeXSS($(this).val()));
        });
    // Booking Info for All Done
    $booking_info_all_done
        .on('change', function() { // pass to `booking_info` for saving
            booking_info.all_done = $.trim(escapeXSS($(this).val()));
        })
        .on('keyup', function () { // update preview
            $('div.ab-booking-info-fifth-preview').html(escapeXSS($(this).val()));
        });
}); // jQuery