<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<!-- ab-booking-info-third-preview -->
<div class="ab-booking-form" style="overflow: hidden">
    <!-- Progress Tracker-->
    <div class="pt"></div>
    <div class="ab-booking-info-fourth-preview ab-row-fluid"></div>
    <!-- payment -->
    <div class="ab-payment">
        <!-- label -->
        <div class="ab-row-fluid">
            <label>
                <input type="radio" name="payment" class="ab-local-payment" checked="checked" value="local"/>
                <?php _e( 'I will pay locally', 'ab' ) ?>
            </label>
        </div>
        <div class="ab-row-fluid">
            <label>
                <input type="radio" name="payment" class="ab-paypal-payment" value="paypal"/>
                <?php _e( 'I will pay now with PayPal', 'ab' ) ?>
            </label>
        </div>
        <!-- buttons -->
        <div class="ab-local-pay-button ab-row-fluid ab-nav-steps last-row">
            <button class="ab-left ab-to-third-step ladda-button orange zoom-in" style="margin-right: 10px;">
                <span><?php _e( 'Back', 'ab' ) ?></span>
            </button>
            <button class="ab-right ab-final-step ladda-button orange zoom-in">
                <span><?php _e( 'Next', 'ab' ) ?></span>
            </button>
        </div>
    </div>
</div>
<!-- fourth step options -->
<div class="ab-fourth-step-options">
    <!-- booking-info -->
    <label class="ab-formLabel" for="ab-booking-info-fourth"><b><?php _e( 'Booking Info', 'ab' ); ?>:</b></label>
    <div class="ab-booking-details">
        <textarea data-selected="<?php _e( strip_tags( $this->steps_options[4][ 'booking-info' ] ) ); ?>" id="ab-booking-info-fourth" style="width: 948px; height: 100px;"><?php _e( strip_tags( $this->steps_options[4][ 'booking-info' ] ) ); ?></textarea>
    </div>
    <!-- controls -->
    <div class="controls" style="margin-right:0">
        <!-- spinner -->
        <span id="payment_spinner" class="spinner"></span>
        <!-- update button -->
        <button id="payment" class="btn btn-info ab-update-button ab-appearance-update">
            <?php _e( 'Update', 'ab' ); ?>
        </button>
        <!-- reset button -->
        <button id="payment_reset" class="ab-reset-form ab-appearance-reset" type="reset">
            <?php _e( 'Reset', 'ab' ) ?>
        </button>
    </div>
</div>