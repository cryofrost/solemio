<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="alert" style="font-size: 14px;">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php _e( 'Settings saved.', 'ab' ); ?>
</div>
<div class="ab-booking-form" style="overflow: hidden">
    <!-- Progress Tracker-->
    <?php renderProgressTracker(); ?>
    <div class="ab-wrapper-content">
        <div style="margin-bottom: 15px!important;" class="ab-row-fluid">
            <div class="ab-booking-info-first-preview ab-bold"><p>Please select service:</p>
            </div>
        </div>
        <div class="ab-mobile-step_1 ab-row-fluid">
            <div class="ab-category-list ab-left">
                <label class="ab-category-title"><?php _e( 'Category', 'ab' ); ?></label>
                <div class="ab-select-wrap">
                    <select class="select-list ab-select-mobile ab-select-category" style="width: 100%">
                        <option value=""><?php _e( 'Select category', 'ab' ); ?></option>
                        <option value="1">Cosmetic Dentistry</option>
                        <option value="2">Invisalign</option>
                        <option value="3">Orthodontics</option>
                        <option value="4">Dentures</option>
                    </select>
                </div>
            </div>
            <div class="ab-category-list ab-category-list-center ab-left">
                <label class="ab-category-title"><?php _e( 'Service', 'ab' ); ?></label>
                <div class="ab-select-wrap">
                    <select class="select-list ab-select-mobile ab-select-service" style="width: 100%">
                        <option value=""><?php _e( 'Select service', 'ab' ); ?></option>
                        <option value="1">Crown and Bridge</option>
                        <option value="2">Teeth Whitening</option>
                        <option value="3">Veneers</option>
                        <option value="4">Invisalign (invisable braces)</option>
                        <option value="5">Orthodontics (braces)</option>
                        <option value="6">Wisdom tooth Removal</option>
                        <option value="7">Root Canal Treatment</option>
                        <option value="8">Dentures</option>
                    </select>
                </div>
            </div>
            <div class="ab-category-list ab-left">
                <label class="ab-category-title"><?php _e( 'Employee', 'ab' ); ?></label>
                <div class="ab-select-wrap">
                    <select class="select-list ab-select-mobile ab-select-employee" style="width: 100%">
                        <option value=""><?php _e( 'Any', 'ab' ); ?></option>
                        <option value="1">Nick Knight</option>
                        <option value="2">Jane Howard</option>
                        <option value="3">Ashley Stamp</option>
                        <option value="4">Bradley Tannen</option>
                        <option value="5">Wayne Turner</option>
                        <option value="6">Emily Taylor</option>
                        <option value="7">Hugh Canberg</option>
                        <option value="8">Jim Gonzalez</option>
                        <option value="9">Nancy Stinson</option>
                        <option value="10">Marry Murphy</option>
                    </select>
                </div>
            </div>
            <button class="ab-right ab-mobile-next-step ladda-button orange zoom-in" onclick="return false">
                <span><?php _e( 'Next', 'ab' ) ?></span>
            </button>
        </div>
        <div class="ab-mobile-step_2">
            <div class="ab-row-fluid">
                <div class="ab-left ab-available-date">
                    <label><?php _e( 'I\'m available on or after', 'ab' ); ?></label>
                    <div class="ab-input-wrap">
                    <span class="ab-requested-date-wrap">
                       <input style="height: 30px!important;" class="ab-requested-date-from select-list" type="text" value="29 November, 2013" readonly="">
                    </span>
                    </div>
                </div>
                <div class="ab-left ab-available-days">
                    <ul class="ab-week-days">
                        <li>
                            <div class="ab-bold"><?php _e('Sun', 'ab' ) ?></div>
                            <!-- #11055: all days are checked by default -->
                            <label class="active">
                                <input class="ab-week-day ab-week-day-1" value="1" checked="checked" type="checkbox">
                            </label>
                        </li>
                        <li>
                            <div class="ab-bold"><?php _e( 'Mon', 'ab' ) ?></div>
                            <!-- #11055: all days are checked by default -->
                            <label class="active">
                                <input class="ab-week-day ab-week-day-2" value="2" checked="checked" type="checkbox">
                            </label>
                        </li>
                        <li>
                            <div class="ab-bold"><?php _e( 'Tue', 'ab' ) ?></div>
                            <!-- #11055: all days are checked by default -->
                            <label class="active">
                                <input class="ab-week-day ab-week-day-3" value="3" checked="checked" type="checkbox">
                            </label>
                        </li>
                        <li>
                            <div class="ab-bold"><?php _e( 'Wed', 'ab' ) ?></div>
                            <!-- #11055: all days are checked by default -->
                            <label class="active">
                                <input class="ab-week-day ab-week-day-4" value="4" checked="checked" type="checkbox">
                            </label>
                        </li>
                        <li>
                            <div class="ab-bold"><?php _e( 'Thu', 'ab' ) ?></div>
                            <!-- #11055: all days are checked by default -->
                            <label class="active">
                                <input class="ab-week-day ab-week-day-5" value="5" checked="checked" type="checkbox">
                            </label>
                        </li>
                        <li>
                            <div class="ab-bold"><?php _e( 'Fri', 'ab' ) ?></div>
                            <!-- #11055: all days are checked by default -->
                            <label class="active">
                                <input class="ab-week-day ab-week-day-6" value="6" checked="checked" type="checkbox">
                            </label>
                        </li>
                        <li>
                            <div class="ab-bold"><?php _e( 'Sat', 'ab' ) ?></div>
                            <!-- #11055: all days are checked by default -->
                            <label class="active">
                                <input class="ab-week-day ab-week-day-7" value="7" checked="checked" type="checkbox">
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="ab-left ab-time-range">
                    <div class="ab-left ab-time-from">
                        <label><?php _e( 'Start from', 'ab' ); ?></label>
                        <div class="ab-select-wrap">
                            <select class="select-list ab-requested-time-from" style="width: auto">
                                <option value="00:00">12:00 am</option>
                                <option value="01:00">1:00 am</option>
                                <option value="02:00">2:00 am</option>
                                <option value="03:00">3:00 am</option>
                                <option value="04:00">4:00 am</option>
                                <option value="05:00">5:00 am</option>
                                <option value="06:00">6:00 am</option>
                                <option value="07:00">7:00 am</option>
                                <option value="08:00" selected="selected">8:00 am</option>
                                <option value="09:00">9:00 am</option>
                                <option value="10:00">10:00 am</option>
                                <option value="11:00">11:00 am</option>
                                <option value="12:00">12:00 pm</option>
                                <option value="13:00">1:00 pm</option>
                                <option value="14:00">2:00 pm</option>
                                <option value="15:00">3:00 pm</option>
                                <option value="16:00">4:00 pm</option>
                                <option value="17:00">5:00 pm</option>
                                <option value="18:00">6:00 pm</option>
                                <option value="19:00">7:00 pm</option>
                                <option value="20:00">8:00 pm</option>
                                <option value="21:00">9:00 pm</option>
                                <option value="22:00">10:00 pm</option>
                                <option value="23:00">11:00 pm</option>
                            </select>
                        </div>
                    </div>
                    <div class="ab-left ab-time-to">
                        <label><?php _e( 'Finish by', 'ab' ); ?></label>
                        <div class="ab-select-wrap">
                            <select class="select-list ab-requested-time-to" style="width: auto">
                                <option value="09:00">9:00 am</option>
                                <option value="10:00">10:00 am</option>
                                <option value="11:00">11:00 am</option>
                                <option value="12:00">12:00 pm</option>
                                <option value="13:00">1:00 pm</option>
                                <option value="14:00">2:00 pm</option>
                                <option value="15:00">3:00 pm</option>
                                <option value="16:00">4:00 pm</option>
                                <option value="17:00">5:00 pm</option>
                                <option value="18:00">6:00 pm</option>
                                <option value="19:00">7:00 pm</option>
                                <option value="20:00">8:00 pm</option>
                                <option value="21:00">9:00 pm</option>
                                <option value="22:00">10:00 pm</option>
                                <option value="23:00">11:00 pm</option>
                                <option value="23:59">12:00 am</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ab-row-fluid ab-nav-steps last-row ab-clear">
                <button class="ab-right ab-mobile-prev-step ladda-button orange zoom-in">
                    <span><?php _e( 'Back', 'ab' ) ?></span>
                </button>
                <button class="ab-right ab-next-step ladda-button orange zoom-in">
                    <span><?php _e( 'Next', 'ab' ) ?></span>
                </button>
            </div>
        </div>
    </div>
</div>


<!-- first step options -->
<div class="ab-first-step-options">
    <!-- booking-info -->
    <label class="ab-formLabel" for="ab-booking-info-first"><b><?php _e( 'Booking Info', 'ab' ); ?>:</b></label>
    <div class="ab-booking-details">
        <textarea data-selected="<?php _e( strip_tags( $this->steps_options[1][ 'booking-info' ] ) ); ?>" id="ab-booking-info-first" style="width: 948px; height: 100px;"><?php _e( strip_tags( $this->steps_options[1][ 'booking-info' ] ) ); ?></textarea>
    </div>
    <!-- controls -->
    <div class="controls" style="margin-right:0">
        <!-- spinner -->
        <span id="select_service_spinner" class="spinner"></span>
        <!-- update button -->
        <button id="select_service" class="btn btn-info ab-update-button ab-appearance-update">
            <?php _e( 'Update', 'ab' ); ?>
        </button>
        <!-- reset button -->
        <button id="select_service_reset" class="ab-reset-form ab-appearance-reset" type="reset">
            <?php _e( 'Reset', 'ab' ) ?>
        </button>
    </div>
</div>