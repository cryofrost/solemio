<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<!-- ab-booking-info-third-preview -->
<div class="ab-booking-form" style="overflow: hidden">
<!-- Progress Tracker-->
<div class="pt"></div>
  <div class="ab-booking-info-third-preview ab-row-fluid"></div>
  <form class="ab-your-details-form ab-row-fluid">
    <div class="ab-details-list ab-left">
      <label class="ab-formLabel"><?php _e( 'Name', 'ab' ) ?></label>
      <div class="ab-details-wrap">
        <input class="ab-full-name" type="text" value="" maxlength="60">
      </div>
      <div class="ab-full-name-error ab-bold"></div>
    </div>
    <div class="ab-details-list ab-left">
      <label class="ab-formLabel"><?php _e( 'Phone', 'ab' ) ?></label>
      <div class="ab-details-wrap">
        <input class="ab-user-phone" maxlength="30" type="text" value="">
      </div>
      <div class="ab-user-phone-error ab-bold"></div>
    </div>
    <div class="ab-details-list ab-left">
      <label class="ab-formLabel"><?php _e( 'Email', 'ab' ) ?></label>
      <div class="ab-details-wrap" style="margin-right: 0">
        <input class="ab-user-email" maxlength="40" type="text" value="">
      </div>
      <div class="ab-user-email-error ab-bold"></div>
    </div>
  </form>
  <div class="ab-row-fluid last-row ab-nav-steps ab-clear">
      <button class="ab-left ab-to-second-step ladda-button orange zoom-in" style="margin-right: 10px;">
          <span><?php _e( 'Back', 'ab' ) ?></span>
      </button>
      <button class="ab-right ab-to-fourth-step ladda-button orange zoom-in">
          <span><?php _e( 'Next', 'ab' ) ?></span>
      </button>
  </div>
</div>
<!-- third step options -->
<div class="ab-third-step-options">
    <!-- booking-info -->
    <label class="ab-formLabel" for="ab-booking-info-third"><b><?php _e( 'Booking Info', 'ab' ); ?>:</b></label>
    <div class="ab-booking-details">
        <textarea data-selected="<?php _e( strip_tags( $this->steps_options[3][ 'booking-info' ] ) ); ?>" id="ab-booking-info-third" style="width: 948px; height: 100px;"><?php _e( strip_tags( $this->steps_options[3][ 'booking-info' ] ) ); ?></textarea>
        <!-- shortcodes-info -->
        <div class="ab-shortcodes-info"><br>
            <p><?php _e( '<b>[[STAFF_NAME]]</b> - name of staff,  <b>[[SERVICE_NAME]]</b> - name of service,', 'ab' ); ?></p>
            <p><?php _e( '<b>[[SERVICE_TIME]]</b> - time of service,  <b>[[SERVICE_DATE]]</b> - date of service', 'ab' ); ?></p>
            <!--<p><?php _e( '<b>[[SERVICE_PRICE]]</b> - price of service.', 'ab' ); ?></p>-->
        </div>
    </div>
    <!-- controls -->
    <div class="controls" style="margin-right:0">
        <!-- spinner -->
        <span id="your_details_spinner" class="spinner"></span>
        <!-- update button -->
        <button id="your_details" class="btn btn-info ab-update-button ab-appearance-update">
            <?php _e( 'Update', 'ab' ); ?>
        </button>
        <!-- reset button -->
        <button id="your_details_reset" class="ab-reset-form ab-appearance-reset" type="reset">
            <?php _e( 'Reset', 'ab' ) ?>
        </button>
    </div>
</div>