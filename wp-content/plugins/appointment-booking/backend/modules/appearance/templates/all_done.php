<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<!-- ab-booking-info-third-preview -->
<div class="ab-booking-form" style="overflow: hidden">
    <!-- Progress Tracker-->
    <div class="pt"></div>
    <div class="ab-booking-info-fifth-preview"></div>
</div>
<!-- fifth step options -->
<div class="ab-fifth-step-options">
    <!-- booking-info -->
    <label class="ab-formLabel" for="ab-booking-info-fifth"><b><?php _e( 'Booking Info', 'ab' ); ?>:</b></label>
    <div class="ab-booking-details">
        <textarea data-selected="<?php _e( strip_tags( $this->steps_options[5][ 'booking-info' ] ) ); ?>" id="ab-booking-info-fifth" style="width: 948px; height: 100px;"><?php _e( strip_tags( $this->steps_options[5][ 'booking-info' ] ) ); ?></textarea>
    </div>
    <!-- controls -->
    <div class="controls" style="margin-right:0">
        <!-- spinner -->
        <span id="all_done_spinner" class="spinner"></span>
        <!-- update button -->
        <button id="all_done" class="btn btn-info ab-update-button ab-appearance-update">
            <?php _e( 'Update', 'ab' ); ?>
        </button>
        <!-- reset button -->
        <button id="all_done_reset" class="ab-reset-form ab-appearance-reset" type="reset">
            <?php _e( 'Reset', 'ab' ) ?>
        </button>
    </div>
</div>