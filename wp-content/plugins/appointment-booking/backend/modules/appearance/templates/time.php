<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<!-- ab-booking-info-second-preview -->
<div id="ab-booking-form" class="ab-booking-form" style="overflow: hidden">
<!-- Progress Tracker-->
<div class="pt"></div>
<div class="ab-booking-info-second-preview ab-row-fluid"></div>
<!-- timeslots -->
<div class="ab-columnizer-wrap" style="height: 400px;">
    <div class="ab-columnizer">
        <div class="ab-time-screen">
            <div class="ab-column">
                <button class="ab-available-day ab-first-child" value="">Wed, Jul 31</button>
                <button class="ab-available-hour" value="2013-07-31 13:00">
                    <i class="ab-hour-icon"><span></span></i>1:00 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 13:15">
                    <i class="ab-hour-icon"><span></span></i>1:15 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 13:30">
                    <i class="ab-hour-icon"><span></span></i>1:30 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 13:45">
                    <i class="ab-hour-icon"><span></span></i>1:45 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 14:00">
                    <i class="ab-hour-icon"><span></span></i>2:00 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 14:15">
                    <i class="ab-hour-icon"><span></span></i>2:15 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 14:30">
                    <i class="ab-hour-icon"><span></span></i>2:30 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 14:45">
                    <i class="ab-hour-icon"><span></span></i>2:45 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 15:00">
                    <i class="ab-hour-icon"><span></span></i>3:00 pm
                </button>
            </div>
            <div class="ab-column">
                <button class="ab-available-hour ab-first-child" value="2013-07-31 15:15">
                    <i class="ab-hour-icon"><span></span></i>3:15 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 15:30">
                    <i class="ab-hour-icon"><span></span></i>3:30 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 15:45">
                    <i class="ab-hour-icon"><span></span></i>3:45 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 16:00">
                    <i class="ab-hour-icon"><span></span></i>4:00 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 16:15">
                    <i class="ab-hour-icon"><span></span></i>4:15 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 16:30">
                    <i class="ab-hour-icon"><span></span></i>4:30 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 16:45">
                    <i class="ab-hour-icon"><span></span></i>4:45 pm
                </button>
                <button class="ab-available-hour" value="2013-07-31 17:00">
                    <i class="ab-hour-icon"><span></span></i>5:00 pm
                </button>
                <button class="ab-available-day ab-first-child" value="">Thu, Aug 01
                </button>
                <button class="ab-available-hour ab-last-child" value="2013-08-01 10:00">
                    <i class="ab-hour-icon"><span></span></i>10:00 am
                </button>
            </div>
            <div class="ab-column">
                <button class="ab-available-hour ab-first-child" value="2013-08-01 10:15">
                    <i class="ab-hour-icon"><span></span></i>10:15 am
                </button>
                <button class="ab-available-hour" value="2013-08-01 10:30">
                    <i class="ab-hour-icon"><span></span></i>10:30 am
                </button>
                <button class="ab-available-hour" value="2013-08-01 10:45">
                    <i class="ab-hour-icon"><span></span></i>10:45 am
                </button>
                <button class="ab-available-hour" value="2013-08-01 11:00">
                    <i class="ab-hour-icon"><span></span></i>11:00 am
                </button>
                <button class="ab-available-hour" value="2013-08-01 11:15">
                    <i class="ab-hour-icon"><span></span></i>11:15 am
                </button>
                <button class="ab-available-hour" value="2013-08-01 11:30">
                    <i class="ab-hour-icon"><span></span></i>11:30 am
                </button>
                <button class="ab-available-hour" value="2013-08-01 11:45">
                    <i class="ab-hour-icon"><span></span></i>11:45 am
                </button>
                <button class="ab-available-hour" value="2013-08-01 12:00">
                    <i class="ab-hour-icon"><span></span></i>12:00 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 12:15">
                    <i class="ab-hour-icon"><span></span></i>12:15 pm
                </button>
                <button class="ab-available-hour ab-last-child" value="2013-08-01 12:30">
                    <i class="ab-hour-icon"><span></span></i>12:30 pm
                </button>
            </div>
            <div class="ab-column">
                <button class="ab-available-hour ab-first-child" value="2013-08-01 12:45">
                    <i class="ab-hour-icon"><span></span></i>12:45 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 13:00">
                    <i class="ab-hour-icon"><span></span></i>1:00 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 13:15">
                    <i class="ab-hour-icon"><span></span></i>1:15 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 15:30">
                    <i class="ab-hour-icon"><span></span></i>3:30 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 15:45">
                    <i class="ab-hour-icon"><span></span></i>3:45 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 16:00">
                    <i class="ab-hour-icon"><span></span></i>4:00 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 16:15">
                    <i class="ab-hour-icon"><span></span></i>4:15 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 16:30">
                    <i class="ab-hour-icon"><span></span></i>4:30 pm
                </button>
                <button class="ab-available-hour" value="2013-08-01 16:45">
                    <i class="ab-hour-icon"><span></span></i>4:45 pm
                </button>
                <button class="ab-available-hour ab-last-child" value="2013-08-01 17:00">
                    <i class="ab-hour-icon"><span></span></i>5:00 pm
                </button>
            </div>
        </div>
    </div>
</div>
<div class="ab-time-buttons ab-row-fluid ab-nav-steps last-row ab-clear">
    <a href="javascript:void(0)" class="ab-time-no-resize ab-time-next ab-right"></a>
    <a href="javascript:void(0)" class="ab-time-no-resize ab-time-prev ab-right"></a>
    <button class="ab-time-no-resize ab-left ab-to-first-step ladda-button orange zoom-in">
        <span><?php _e( 'Back', 'ab' ) ?></span>
    </button>
</div>
</div>
<!-- second step options -->
<div class="ab-second-step-options">

    <!-- booking-info -->
    <label class="ab-formLabel" for="ab-booking-info-second"><b><?php _e( 'Booking Info', 'ab' ); ?>:</b></label>
    <div class="ab-booking-details">
        <textarea data-selected="<?php _e( wpautop( strip_tags( $this->steps_options[2][ 'booking-info' ] ) ) ); ?>" id="ab-booking-info-second" style="width: 948px; height: 100px;"><?php _e( strip_tags( $this->steps_options[2][ 'booking-info' ] ) ); ?></textarea>
        <!-- shortcodes-info -->
        <div class="ab-shortcodes-info"><br>
            <p><?php _e( '<b>[[SERVICE_NAME]]</b> - name of service, <b>[[STAFF_NAME]]</b> - name of staff', 'ab' ); ?></p>
        </div>
    </div>
    <!-- controls -->
    <div class="controls" style="margin-right:0">
        <!-- spinner -->
        <span id="convenient_time_spinner" class="spinner"></span>
        <!-- update button -->
        <button id="convenient_time" class="btn btn-info ab-update-button ab-appearance-update">
            <?php _e( 'Update', 'ab' ); ?>
        </button>
        <!-- reset button -->
        <button id="convenient_time_reset" class="ab-reset-form ab-appearance-reset" type="reset">
            <?php _e( 'Reset', 'ab' ) ?>
        </button>
    </div>
</div>