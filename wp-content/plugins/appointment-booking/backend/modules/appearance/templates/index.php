<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php
    function renderProgressTracker() {
        echo '<div class="ab-progress-tracker">
                <ul class="ab-progress-bar nav-3">
                    <li class="ab-step-tabs first">
                        <a href="javascript:void(0)">1. ' . __( "Service", "ab" ) . '</a>
                        <div class="step"></div>
                    </li>
                    <li class="ab-step-tabs">
                        <a href="javascript:void(0)">2. ' . __( "Time", "ab" ) . '</a>
                        <div class="step"></div>
                    </li>
                    <li class="ab-step-tabs">
                        <a href="javascript:void(0)">3. ' . __( "Details", "ab" ) . '</a>
                        <div class="step"></div>
                    </li>
                    <li class="ab-step-tabs">
                        <a href="javascript:void(0)">4. ' . __( "Payment", "ab" ) . '</a>
                        <div class="step"></div>
                    </li>
                    <li class="ab-step-tabs last">
                        <a href="javascript:void(0)">5. ' . __( "Done", "ab" ) . '</a>
                        <div class="step"></div>
                    </li>
                </ul>
            </div>';
    }
?>
<div class="ab-title"><?php _e( 'Appearance', 'ab' ) ?></div>
<input type="hidden" class="wp-color-picker" name="color"
       value="<?php _e( get_option( 'ab_appearance_booking_form_color' ) ); ?>"
       data-selected="<?php _e( get_option( 'ab_appearance_booking_form_color' ) ); ?>"><br />
<div style="max-width: 960px;">
    <form method="post" id="common_settings" style="margin-right: 15px">
        <legend id='main_form'>
            <input id="progress_tracker" name="progress_tracker[]" <?php echo $this->progress_tracker_options[ 'enabled' ] ? 'checked="1"' : '' ?>  type=checkbox />
            <label style="display: inline" for="progress_tracker">
                <b><?php _e( 'Show Form Progress Tracker', 'ab' ); ?></b>
            </label>
        </legend>
    </form>
    <!-- Tabs -->
    <div class="tabbable" style="margin-top: 20px;">
        <ul class="nav nav-tabs ab-nav-tabs">
            <?php foreach ( $this->steps as $step_id => $step_name ) : ?>
                <li class="ab-step-tab-<?php esc_attr_e( $step_id ) ?> ab-step-tabs<?php esc_attr_e( 1 == $step_id ? ' active' : '' ) ?>" data-step-id="<?php esc_attr_e( $step_id ) ?>">
                    <a href="#" data-toggle="tab"><?php esc_attr_e( $step_id ); esc_attr_e( '. ' . $step_name ); ?></a>
                </li>
            <?php endforeach ?>
        </ul>
        <!-- Tabs-Content -->
        <div class="tab-content">
            <?php foreach ( $this->steps as $step_id => $step_name ) : ?>
                <div class="tab-pane-<?php esc_attr_e( $step_id ); esc_attr_e( 1 == $step_id ? ' active' : '' ) ?>" data-step-id="<?php esc_attr_e( $step_id ) ?>">
                    <?php
                        // Render unique data per step
                        switch ( $step_id ) {
                            // Service
                            case 1:
                                include 'select_service.php';
                                break;
                            // Time
                            case 2:
                                include 'time.php';
                                break;
                            // Details
                            case 3:
                                include 'your_details.php';
                                break;
                            // Payment
                            case 4:
                                include 'payment.php';
                                break;
                            // Done
                            case 5:
                                include 'all_done.php';
                                break;
                        }
                    ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>