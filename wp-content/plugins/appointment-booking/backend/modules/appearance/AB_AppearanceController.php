<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class AB_AppearanceController
 */
class AB_AppearanceController extends AB_Controller {

    /**
     *  Default Action
     */
    public function index() {
        // initialize Progress Tracker options
        $this->progress_tracker_options = array(
            'enabled' => intval( get_option( 'ab_appearance_show_progress_tracker' ) == 1 ) ? 1 : 0,
            'type'    => get_option( 'ab_appearance_progress_tracker_type' )
        );

        // initialize steps (tabs)
        $this->steps = array(
            1 => __( 'Service', 'ab' ),
            2 => __( 'Time', 'ab' ),
            3 => __( 'Details', 'ab' ),
            4 => __( 'Payment', 'ab' ),
            5 => __( 'Done', 'ab' )
        );

        // initialize steps options
        $this->steps_options = array(
            // Select Service
            1 => array(
                // booking-info
                'booking-info' => get_option( 'ab_appearance_first_step_booking_info' ) == '' ?
                    '' : get_option( 'ab_appearance_first_step_booking_info' )
            ),
            // Convenient Time
            2 => array(
                // booking-info
                'booking-info' => get_option( 'ab_appearance_second_step_booking_info' ) == '' ?
                    '' : get_option( 'ab_appearance_second_step_booking_info' )
            ),
            // Your Details
            3 => array(
                // booking-info
                'booking-info' => get_option( 'ab_appearance_third_step_booking_info' ) == '' ?
                    '' : get_option( 'ab_appearance_third_step_booking_info' )
            ),
            // Payment
            4 => array(
                // booking-info
                'booking-info' => get_option( 'ab_appearance_fourth_step_booking_info' ) == '' ?
                    '' : get_option( 'ab_appearance_fourth_step_booking_info' )
            ),
            // Done
            5 => array(
                // booking-info
                'booking-info' => get_option( 'ab_appearance_fifth_step_booking_info' ) == '' ?
                    '' : get_option( 'ab_appearance_fifth_step_booking_info' )
            )
        );

        // render general layout
        $this->render( 'index' );
    } // index

    /**
     *  Update options
     */
    public function executeUpdateAppearanceOptions() {
        if ( count( $this->getPost() ) ) {
            $get_option = $this->getParameter( 'options' );
            $step_id = $this->getParameter( 'step_id' );
            if ( isset( $step_id, $get_option ) ) {
                $new_options = array();
                $old_options = array();
                switch ( $step_id ) {
                    // Select Service
                    case 1:
                        $new_options = array(
                            // booking-info
                            'ab_appearance_first_step_booking_info' =>
                                $get_option[ 'ab_appearance_first_step_booking_info' ],
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                $get_option[ 'ab_appearance_booking_form_color' ]
                        ); // $new_options
                        $old_options =  array(
                            // booking-info
                            'ab_appearance_first_step_booking_info' =>
                                get_option( 'ab_appearance_first_step_booking_info' ),
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                get_option( 'ab_appearance_booking_form_color' )
                        ); // $old_options
                        break;
                    // Convenient Time
                    case 2:
                        $new_options = array(
                            // booking-info
                            'ab_appearance_second_step_booking_info' =>
                                $get_option[ 'ab_appearance_second_step_booking_info' ],
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                $get_option[ 'ab_appearance_booking_form_color' ]
                        ); // $new_options
                        $old_options =  array(
                            // booking-info
                            'ab_appearance_second_step_booking_info' =>
                                get_option( 'ab_appearance_second_step_booking_info' ),
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                get_option( 'ab_appearance_booking_form_color' )
                        ); // $old_options
                        break;
                    // Your Details
                    case 3:
                        $new_options = array(
                            // booking-info
                            'ab_appearance_third_step_booking_info' =>
                                $get_option[ 'ab_appearance_third_step_booking_info' ],
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                $get_option[ 'ab_appearance_booking_form_color' ]
                        ); // $new_options
                        $old_options =  array(
                            // booking-info
                            'ab_appearance_third_step_booking_info' =>
                                get_option( 'ab_appearance_third_step_booking_info' ),
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                get_option( 'ab_appearance_booking_form_color' )
                        ); // $old_options
                        break;
                    // Payment
                    case 4:
                        $new_options = array(
                            // booking-info
                            'ab_appearance_fourth_step_booking_info' =>
                                $get_option[ 'ab_appearance_fourth_step_booking_info' ],
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                $get_option[ 'ab_appearance_booking_form_color' ]
                        ); // $new_options
                        $old_options =  array(
                            // booking-info
                            'ab_appearance_fourth_step_booking_info' =>
                                get_option( 'ab_appearance_fourth_step_booking_info' ),
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                get_option( 'ab_appearance_booking_form_color' )
                        ); // $old_options
                        break;
                    // Done
                    case 5:
                        $new_options = array(
                            // booking-info
                            'ab_appearance_fifth_step_booking_info' =>
                                $get_option[ 'ab_appearance_fifth_step_booking_info' ],
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                $get_option[ 'ab_appearance_booking_form_color' ]
                        ); // $new_options
                        $old_options =  array(
                            // booking-info
                            'ab_appearance_fifth_step_booking_info' =>
                                get_option( 'ab_appearance_fifth_step_booking_info' ),
                            // booking form color
                            'ab_appearance_booking_form_color' =>
                                get_option( 'ab_appearance_booking_form_color' )
                        ); // $old_options

                        break;
                }

                // if there are difference in values between new and old options
                if ( count( $difference = array_diff( $new_options, $old_options ) ) ) {
                    foreach ( $difference as $option_name => $option_value ) {
                        update_option( esc_html( $option_name ), esc_html( $option_value ) );
                    }
                }
            }
        }
        exit;
    } // executeUpdateAppearanceOptions

    /**
     *  Progress Tracker Options for all steps
     */
    public function executeUpdateProgressTrackerOptions() {
        $progress_tracker_enabled = $this->getParameter( 'progress_tracker_enabled' );
        $progress_tracker_enabled_option = intval( get_option( 'ab_appearance_show_progress_tracker' ) == 1 ) ? 1 : 0;

        // enable/disable Progress Tracker
        if ( isset( $progress_tracker_enabled ) && $progress_tracker_enabled == 0 &&
            $progress_tracker_enabled_option != $progress_tracker_enabled ) {
                update_option( 'ab_appearance_show_progress_tracker', 0 );
        } elseif ( $progress_tracker_enabled == 1 && $progress_tracker_enabled_option != $progress_tracker_enabled ) {
            update_option( 'ab_appearance_show_progress_tracker', 1 );
        }

        echo json_encode( array(
            'progress_tracker_enabled' => get_option( 'ab_appearance_show_progress_tracker' )
        ) );
        exit;
    } // executeUpdateProgressTrackerOptions

    /**
     * Override parent method to add 'wp_ajax_ab_' prefix
     * so current 'execute*' methods look nicer.
     */
    protected function registerWpActions( $prefix = '' ) {
        parent::registerWpActions( 'wp_ajax_ab_' );
    } // registerWpActions

} // AB_AppearanceController
