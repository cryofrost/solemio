<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php if ( $collection && !empty ( $collection ) ): ?>
    <?php $total = 0 ?>
    <?php foreach ( $collection as $i => $payment ): ?>
    <tr style="<?php echo $i%2 ? 'background-color: #eee;' : '' ?>">
        <td><?php echo date( get_option( 'date_format' ), strtotime( $payment->created ) ) ?></td>
        <td><?php echo $payment->type == 'paypal' ? 'PayPal' : __( 'Local', 'ab' ) ?></td>
        <td><?php esc_html_e( $payment->customer ) ?></td>
        <td><?php esc_html_e( $payment->provider ) ?></td>
        <td><?php esc_html_e( $payment->service ) ?></td>
        <td><div class=pull-right><?php echo $payment->amount ?></div></td>
        <td><?php echo date( get_option( 'date_format' ), strtotime( $payment->date ) ) ?></td>
        <?php $total += $payment->amount ?>
    </tr>
    <?php endforeach ?>
    <tr style="<?php echo (++$i)%2 ? 'background-color: #eee;' : '' ?>">
        <td colspan=6><div class=pull-right><strong><?php _e( 'Total: ', 'ab' ); ?> <?php echo $total;?></strong></div></td>
        <td></td>
    </tr>
<?php endif ?>