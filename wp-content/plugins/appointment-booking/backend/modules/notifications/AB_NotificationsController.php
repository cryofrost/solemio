<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

include 'AB_NotificationsForm.php';

class AB_NotificationsController extends AB_Controller {

    public function index() {
        wp_enqueue_style( 'ab-style', plugins_url( 'resources/css/ab_style.css', dirname(__FILE__).'/../../AB_Backend.php' ) );
        wp_enqueue_style( 'bootstrap', plugins_url( 'resources/bootstrap/css/bootstrap.min.css', dirname(__FILE__).'/../../AB_Backend.php' ) );
        wp_enqueue_script( 'bootstrap', plugins_url( 'resources/bootstrap/js/bootstrap.min.js', dirname(__FILE__).'/../../AB_Backend.php' ), array( 'jquery' ) );

        $this->form = new AB_NotificationsForm();

        // save action
        if ( count( $this->getPost() ) ) {
            $this->form->bind( $this->getPost(), $_FILES );
            $this->form->save();
            $this->message = __( 'Notification settings were updated successfully.', 'ab' );
            // sender name
            if ( $this->getParameter( 'sender_name' ) ) {
                update_option( 'ab_settings_sender_name', esc_html( $this->getParameter( 'sender_name' ) ) );
            }
            // sender email
            if ( $this->getParameter( 'sender_email' ) ) {
                update_option( 'ab_settings_sender_email', esc_html( $this->getParameter( 'sender_email' ) ) );
            }
        }

        $this->render( 'index' );
    }

    /**
     * Send all types of notifications to admin (for testing purposes).
     *
     * To execute, run in browser's console: jQuery.get('admin-ajax.php', {action: 'ab_send_test_notifications'})
     *
     * @global wpdb $wpdb
     */
    public function executeSendTestNotifications() {
        /** @var wpdb $wpdb */
        global $wpdb;

        add_filter('wp_mail_from', create_function( '$content_type',
            'return get_option( \'ab_settings_sender_email\' ) == \'\' ?
                get_option( \'blogname\' ) : get_option( \'ab_settings_sender_email\' );'
        ) );
        add_filter('wp_mail_from_name', create_function( '$name',
            'return get_option( \'ab_settings_sender_name\' ) == \'\' ?
                get_option( \'admin_email\' ) : get_option( \'ab_settings_sender_name\' );'
        ) );

        $admin_emails = AB_CommonUtils::getAdminEmails();

        $notifications = array(
            'client_info'      => $wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "client_info"' ),
            'provider_info'    => $wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "provider_info"' ),
            'evening_next_day' => $wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "evening_next_day"' ),
            'evening_after'    => $wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "evening_after"' ),
            'event_next_day'   => $wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "event_next_day"' )
        ); // $notifications

        $fixture = array(
            // default notifications
            'client_name'         => 'Jack Daniels',
            'client_phone'        => '+37379222333',
            'client_email'        => 'client@fake.com',
            'client_notes'        => "First line\nSecond line\nTest <b>tags</b>",
            'booked_datetime'     => '2013-06-28 10:15',
            'service_name'        => 'Eye Brow Tidy',
            // event_next_day notification
            'event_next_day'      => '<table><tr><td>10:15-11:00<td><td>Eye Brow Reshape<td><td><td></tr></table>'
        ); // $fixtured_data

        foreach( $notifications as $notification ) {
            switch ( $notification->slug ) {
                case 'event_next_day':
                    $replacement = new AB_NotificationReplacement();
                    $replacement->setClientName( $fixture[ 'client_name' ] );
                    $replacement->setAppointmentTime( $fixture[ 'booked_datetime' ] );
                    $replacement->setServiceName( $fixture[ 'service_name' ] );
                    $replacement->setNextDayAgenda( $fixture[ 'event_next_day' ] );
                    $message = wpautop( $replacement->replace( $notification->message ) );

                    // prepare subject
                    $date = new DateTime();
                    $date->modify('+1 day');
                    $tomorrow_date = $date->format('Y-m-d');
                    $date = date_i18n( get_option( 'date_format' ), strtotime( $tomorrow_date ) );
                    $subject = $notification->subject;

                    if ( preg_match( '/\[\[.*?\]\]/', $subject ) ) {
                        $subject = preg_replace( '/\[\[.*?\]\]/', $date, $subject );
                    }

                    wp_mail( $admin_emails, $subject, $message );
                    break;
                case 'provider_info':
                    $replacement = new AB_NotificationReplacement();
                    $replacement->setClientName( $fixture[ 'client_name' ] );
                    $replacement->setClientPhone( $fixture[ 'client_phone'] );
                    $replacement->setClientEmail( $fixture[ 'client_email' ] );
                    $replacement->setClientNotes( $fixture[ 'client_notes' ] );
                    $replacement->setAppointmentTime( $fixture[ 'booked_datetime' ] );
                    $replacement->setServiceName( $fixture[ 'service_name' ] );
                    $replacement->setClientPhone( $fixture[ 'client_phone' ] );
                    $replacement->setClientEmail( $fixture[ 'client_email' ] );
                    $message = wpautop( $replacement->replace( $notification->message ) );

                    wp_mail( $admin_emails, $notification->subject, $message );
                    break;
                default:
                    $replacement = new AB_NotificationReplacement();
                    $replacement->setClientName( $fixture[ 'client_name' ] );
                    $replacement->setAppointmentTime( $fixture[ 'booked_datetime' ] );
                    $replacement->setServiceName( $fixture[ 'service_name' ] );
                    $message = wpautop( $replacement->replace( $notification->message ) );
                    $subject = $replacement->replaceSubject( $notification->subject );

                    wp_mail( $admin_emails, $subject, $message );
                    break;
            } // switch
        } // foreach

        echo '<p>Emails were sent successfully.</p><p>Receivers:<ol>';
        foreach ($admin_emails as $email) {
            echo '<li>' . $email . '</li>';
        }
        echo '</ol></p>';
        exit;
    }

    // Protected methods.

    /**
     * Override parent method to add 'wp_ajax_ab_' prefix
     * so current 'execute*' methods look nicer.
     */
    protected function registerWpActions( $prefix = '' ) {
        parent::registerWpActions( 'wp_ajax_ab_' );
    }
}