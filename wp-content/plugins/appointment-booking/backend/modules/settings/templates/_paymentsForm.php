<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<form method="post" action="<?php echo $current_url . '&type=_payments' ?>" class="ab-staff-form">
    <?php if (isset($message_p)) : ?>
    <div id="message" style="margin: 0px!important;" class="updated below-h2">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p><?php echo $message_p ?></p>
    </div>
    <?php endif ?>
    <table class="form-horizontal">
        <tr>
            <td style="width: 170px;"><?php _e( 'Currency','ab' ) ?></td>
            <td>
                <select name="ab_paypal_currency" style="width: 200px;">
                    <?php foreach ( PayPal::getCurrencyCodes() as $code ): ?>
                        <option value="<?php echo $code ?>" <?php echo get_option( 'ab_paypal_currency' ) == $code ? 'selected="selected"' : '' ?> ><?php echo $code ?></option>
                    <?php endforeach ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><div class="ab-payments-title"><?php _e( 'Service paid locally','ab' ) ?></div></td>
        </tr>
        <tr>
            <td colspan="2">
                <select name="ab_local_mode" style="width: 200px;">
                    <?php foreach ( array( __( 'Disabled', 'ab' ) => '0', __( 'Enabled', 'ab' ) => '1' ) as $text => $mode ): ?>
                        <option value="<?php echo $mode ?>" <?php echo get_option( 'ab_local_mode' ) == $mode ? 'selected="selected"' : '' ?> ><?php echo $text ?></option>
                    <?php endforeach ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><div class="ab-payments-title"><?php _e( 'PayPal','ab' ) ?></div></td>
        </tr>
        <tr>
            <td colspan="2">
                <select name="ab_paypal_type" id="ab_paypal_type" style="width: 200px;">
                    <?php foreach ( array( __( 'Disabled', 'ab' ) => 'disabled', 'PayPal Express Checkout' => 'ec' ) as $text => $mode ): ?>
                        <option value="<?php echo $mode ?>" <?php echo get_option( 'ab_paypal_type' ) == $mode ? 'selected="selected"' : '' ?> ><?php echo $text ?></option>
                    <?php endforeach ?>
                </select>
            </td>
        </tr>
        <tr class="paypal_ec">
            <td><?php _e( 'API Username','ab' ) ?></td>
            <td><input type="text" size="33" name="ab_paypal_api_username" value="<?php echo get_option( 'ab_paypal_api_username' ) ?>"/></td>
        </tr>
        <tr class="paypal_ec">
            <td><?php _e( 'API Password','ab' ) ?></td>
            <td><input type="text" size="33" name="ab_paypal_api_password" value="<?php echo get_option( 'ab_paypal_api_password' ) ?>"/></td>
        </tr>
        <tr class="paypal_ec">
            <td><?php _e( 'API Signature','ab' ) ?></td>
            <td><input type="text" size="33" name="ab_paypal_api_signature" value="<?php echo get_option( 'ab_paypal_api_signature' ) ?>"/></td>
        </tr>
        <tr class="paypal_ec">
            <td><?php _e( 'Sandbox Mode','ab' ) ?></td>
            <td>
                <select name="ab_paypal_ec_mode" style="width: 200px;">
                    <?php foreach ( array( 'Yes' => '.sandbox', 'No' => '' ) as $text => $mode ): ?>
                        <option value="<?php echo $mode ?>" <?php echo get_option( 'ab_paypal_ec_mode' ) == $mode ? 'selected="selected"' : '' ?> ><?php echo $text ?></option>
                    <?php endforeach ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<?php _e( 'Save', 'ab' ) ?>" class="btn btn-info ab-update-button" />
                <button class="ab-reset-form" type="reset"><?php _e( 'Reset', 'ab' ) ?></button>
            </td>
            <td></td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    jQuery(function($) {
        var $paypal_ec = $('.paypal_ec');
        $('#ab_paypal_type').change(function() {
            $paypal_ec.hide();
            if ($(this).val() == 'ec') {
                $paypal_ec.show();
            }
        }).change();
    });
</script>