<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function load_data() {
    /** @global wpdb $wpdb */
    global $wpdb;

    for ( $i = 0; $i < 7; $i++ ) {
        $wpdb->insert( 'ab_schedule_item', array( 'name' => AB_DateUtils::getWeekDayByNumber( $i ) ) );
    }

    // notifications
    $notifications = array(
        'client_info'                 => __( 'Notification to Customer about Appointment details', 'ab' ),
        'provider_info'               => __( 'Notification to Staff Member about Appointment details', 'ab' ),
        'evening_next_day'            => __( 'Evening reminder to Customer about next day Appointment', 'ab' ),
        'evening_after'               => __( 'Follow-up message on the day after Appointment', 'ab' ),
        'event_next_day'              => __( 'Evening notification with the next day agenda to Staff Member', 'ab' )
    );

    $notifications_messages = array(
        'client_info'                 => wpautop( __('
            Dear [[CLIENT_NAME]].

            This is confirmation that you have booked [[SERVICE_NAME]].

            We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]] .

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
        ', 'ab' ) ),
        'provider_info'               => wpautop( __('
            Hello.

            You have new booking.

            Service: [[SERVICE_NAME]]
            Date: [[APPOINTMENT_DATE]]
            Time: [[APPOINTMENT_TIME]]
            Client name: [[CLIENT_NAME]]
            Client phone: [[CLIENT_PHONE]]
            Client email: [[CLIENT_EMAIL]]
        ', 'ab' ) ),
        'evening_next_day'            => wpautop( __('
            Dear [[CLIENT_NAME]].

            We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
        ', 'ab' ) ),
        'evening_after'               => wpautop( __('
            Dear [[CLIENT_NAME]].

            Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

            Thank you and we look forward to seeing you again soon,

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
        ', 'ab' ) ),
        'event_next_day'              => wpautop( __('
            Hello.

            Your agenda for tomorrow is:

            [[NEXT_DAY_AGENDA]]
        ', 'ab' ) ),
    );

    $notifications_subjects = array(
        'client_info'                 => __( 'Your Appointment Information', 'ab' ),
        'provider_info'               => __( 'New booking information', 'ab' ),
        'evening_next_day'            => __( 'Your appointment at [[COMPANY_NAME]]', 'ab' ),
        'evening_after'               => __( 'Your visit to [[COMPANY_NAME]]', 'ab' ),
        'event_next_day'              => __( 'Your agenda for [[TOMORROW_DATE]]', 'ab' )
    );

    foreach ( $notifications as $slug => $name ) {
        $wpdb->insert( 'ab_notifications', array( 'slug' => $slug, 'name' => $name ) );
    }

    // messages
    foreach ( $notifications_messages as $slug => $message ) {
        $wpdb->update( 'ab_notifications', array( 'message' => $message ), array( 'slug' => $slug ) );
    }

    // subjects
    foreach ( $notifications_subjects as $slug => $subject ) {
        if ( $slug == 'client_info' ) { // "Send email notification to client with appointment info" is active by default
            $wpdb->update( 'ab_notifications', array( 'subject' => $subject, 'active' => 1 ), array( 'slug' => $slug ) );
        } else {
            $wpdb->update( 'ab_notifications', array( 'subject' => $subject ), array( 'slug' => $slug ) );
        }
    }

    // booking-info
    $booking_info = array(
        // Service
        'first_step' => wpautop( __( "Please select service: ", "ab" ) ),
        // Time
        'second_step' => wpautop( __(
            "Below you can find list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].\nClick on a time slot to proceed with booking.", "ab"
        ) ),
        // Details
        'third_step' => wpautop( __(
            "You selected to book [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. Price for the service is [[SERVICE_PRICE]].\nPlease provide your details in form below to proceed with booking.", "ab"
        ) ),
        // Payment
        'fourth_step' => wpautop( __( "Please tell us how you would like to pay: ", "ab" ) ),
        // Done
        'fifth_step' => wpautop( __( "Thank you! Booking process is completed. An email with details of your booking has been sent to you.", "ab" ) )
    ); // $booking_info

    update_option( 'ab_appearance_first_step_booking_info',  $booking_info[ 'first_step' ] );
    update_option( 'ab_appearance_second_step_booking_info', $booking_info[ 'second_step' ] );
    update_option( 'ab_appearance_third_step_booking_info',  $booking_info[ 'third_step' ] );
    update_option( 'ab_appearance_fourth_step_booking_info', $booking_info[ 'fourth_step' ] );
    update_option( 'ab_appearance_fifth_step_booking_info',  $booking_info[ 'fifth_step' ] );
    update_option( 'ab_settings_sender_email', get_option( 'admin_email' ) );
    update_option( 'ab_settings_sender_name', get_option( 'blogname' ) );
} // load_data