<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

  function drop_tables() {
      /** @var wpdb $wpdb */
      global $wpdb;

      $wpdb->query( 'ALTER TABLE ab_service DROP FOREIGN KEY fk_ab_service_category_id' );
      $wpdb->query( 'ALTER TABLE ab_staff_service DROP FOREIGN KEY fk_ab_staff_service_staff_id' );
      $wpdb->query( 'ALTER TABLE ab_staff_service DROP FOREIGN KEY fk_ab_staff_service_service_id' );
      $wpdb->query( 'ALTER TABLE ab_staff_schedule_item DROP FOREIGN KEY fk_ab_staff_schedule_item_staff_id' );
      $wpdb->query( 'ALTER TABLE ab_staff_schedule_item DROP FOREIGN KEY fk_ab_staff_schedule_item_schedule_item_id' );
      $wpdb->query( 'ALTER TABLE ab_schedule_item_break DROP FOREIGN KEY fk_ab_schedule_item_break_staff_schedule_item_id' );
      $wpdb->query( 'ALTER TABLE ab_appointment DROP FOREIGN KEY fk_ab_appointment_staff_id' );
      $wpdb->query( 'ALTER TABLE ab_appointment DROP FOREIGN KEY fk_ab_appointment_service_id' );
      $wpdb->query( 'ALTER TABLE ab_appointment DROP FOREIGN KEY fk_ab_appointment_customer_id' );
      $wpdb->query( 'ALTER TABLE ab_payment DROP FOREIGN KEY fk_ab_payment_customer_id' );
      $wpdb->query( 'ALTER TABLE ab_payment_appointment DROP FOREIGN KEY fk_ab_payment_appointment_appointment_id' );
      $wpdb->query( 'ALTER TABLE ab_payment_appointment DROP FOREIGN KEY fk_ab_payment_appointment_payment_id' );
      $wpdb->query( 'ALTER TABLE ab_email_notification DROP FOREIGN KEY fk_ab_email_notification_customer_id' );
      $wpdb->query( 'ALTER TABLE ab_email_notification DROP FOREIGN KEY fk_ab_email_notification_staff_id' );
      $wpdb->query( 'ALTER TABLE ab_holiday DROP FOREIGN KEY fk_ab_holiday_staff_id' );

      $wpdb->query( 'DROP TABLE IF EXISTS ab_category' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_service' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_staff' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_staff_service' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_schedule_item' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_staff_schedule_item' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_schedule_item_break' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_notifications' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_appointment' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_customer' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_payment' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_payment_appointment' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_holiday' );
      $wpdb->query( 'DROP TABLE IF EXISTS ab_email_notification' );
  }