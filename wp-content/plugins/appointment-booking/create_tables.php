<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

  function create_tables() {
      /** @var wpdb $wpdb */
      global $wpdb;

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_staff (
            id int unsigned NOT NULL auto_increment PRIMARY KEY,
            wp_user_id bigint(20) unsigned,
            avatar_url varchar(255) default '',
            avatar_path varchar(255) default '',
            full_name varchar(128) default '',
            email varchar(128) default '',
            phone varchar(128) default ''
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_category (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR( 255 ) NOT NULL
         ) ENGINE = INNODB
         DEFAULT CHARACTER SET = utf8
         COLLATE = utf8_general_ci"
      );

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_service (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            title VARCHAR( 255 ) DEFAULT '',
            duration INT NOT NULL DEFAULT 900,
            price DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.00',
            color VARCHAR( 255 ) NOT NULL DEFAULT '#FFFFFF',
            category_id INT UNSIGNED ,
            INDEX ab_service_category_id_idx (category_id),
            CONSTRAINT fk_ab_service_category_id
                FOREIGN KEY ab_service_category_id_idx (category_id)
                REFERENCES ab_category(id)
                ON DELETE SET NULL
                ON UPDATE CASCADE
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_staff_service (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            staff_id INT UNSIGNED NOT NULL,
            service_id INT UNSIGNED NOT NULL,
            price DECIMAL( 10, 2 ) NOT NULL DEFAULT '0.00',
            UNIQUE KEY ab_staff_service_unique_ids_idx (staff_id, service_id),
            INDEX ab_staff_service_staff_id_idx (staff_id),
            INDEX ab_staff_service_service_id_idx (service_id),
            CONSTRAINT fk_ab_staff_service_staff_id
                FOREIGN KEY ab_staff_service_staff_id_idx (staff_id)
                REFERENCES ab_staff(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            CONSTRAINT fk_ab_staff_service_service_id
                FOREIGN KEY ab_staff_service_service_id_idx (service_id)
                REFERENCES ab_service(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_schedule_item (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR( 255 ) NOT NULL
         ) ENGINE = INNODB
         DEFAULT CHARACTER SET = utf8
         COLLATE = utf8_general_ci"
      );

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_staff_schedule_item (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            staff_id INT UNSIGNED NOT NULL,
            schedule_item_id INT UNSIGNED NOT NULL,
            start_time TIME,
            end_time TIME,
            UNIQUE KEY ab_staff_schedule_item_unique_ids_idx (staff_id, schedule_item_id),
            INDEX ab_staff_schedule_item_staff_id_idx (staff_id),
            INDEX ab_staff_schedule_item_schedule_item_id_idx (schedule_item_id),
            CONSTRAINT fk_ab_staff_schedule_item_staff_id
                FOREIGN KEY ab_staff_schedule_item_staff_id_idx (staff_id)
                REFERENCES ab_staff(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            CONSTRAINT fk_ab_staff_schedule_item_schedule_item_id
                FOREIGN KEY ab_staff_schedule_item_schedule_item_id_idx (schedule_item_id)
                REFERENCES ab_schedule_item(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
         ) ENGINE = INNODB
         DEFAULT CHARACTER SET = utf8
         COLLATE = utf8_general_ci"
      );

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_schedule_item_break (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            staff_schedule_item_id INT UNSIGNED NOT NULL,
            start_time TIME,
            end_time TIME,
            INDEX ab_schedule_item_break_staff_schedule_item_id_idx (staff_schedule_item_id),
            CONSTRAINT fk_ab_schedule_item_break_staff_schedule_item_id
                FOREIGN KEY ab_schedule_item_break_staff_schedule_item_id_idx (staff_schedule_item_id)
                REFERENCES ab_staff_schedule_item(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
         ) ENGINE = INNODB
         DEFAULT CHARACTER SET = utf8
         COLLATE = utf8_general_ci"
      );

      $wpdb->query(
          "CREATE TABLE IF NOT EXISTS ab_notifications (
            id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name        VARCHAR ( 255 ) NOT NULL DEFAULT '',
            slug        VARCHAR ( 255 ) NOT NULL DEFAULT '',
            active      TINYINT ( 1 ) NOT NULL DEFAULT '0',
            copy        TINYINT ( 1 ) NOT NULL DEFAULT '0',
            subject     VARCHAR ( 255 ) NOT NULL DEFAULT '',
            message     TEXT
        ) DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
          "CREATE TABLE IF NOT EXISTS ab_customer (
            id      INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name    VARCHAR ( 255 ) NOT NULL DEFAULT '',
            phone   VARCHAR ( 255 ) NOT NULL DEFAULT '',
            email   VARCHAR ( 255 ) NOT NULL DEFAULT ''
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
          "CREATE TABLE IF NOT EXISTS  ab_email_notification (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            customer_id INT UNSIGNED,
            staff_id INT UNSIGNED,
            type VARCHAR( 60 ) NOT NULL,
            created DATETIME NOT NULL,
            INDEX ab_email_notification_customer_id_idx (customer_id),
            INDEX ab_email_notification_staff_id_idx (staff_id),
            CONSTRAINT      fk_ab_email_notification_customer_id
                FOREIGN KEY ab_email_notification_customer_id_idx (customer_id)
                REFERENCES  ab_customer(id)
                ON DELETE   CASCADE
                ON UPDATE   CASCADE,
            CONSTRAINT      fk_ab_email_notification_staff_id
                FOREIGN KEY ab_email_notification_staff_id_idx (staff_id)
                REFERENCES  ab_staff(id)
                ON DELETE   CASCADE
                ON UPDATE   CASCADE
          ) ENGINE = INNODB
          DEFAULT CHARACTER SET = utf8
          COLLATE = utf8_general_ci"
      );

      $wpdb->query(
          "CREATE TABLE IF NOT EXISTS ab_appointment (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            staff_id INT UNSIGNED NOT NULL,
            service_id INT UNSIGNED,
            customer_id INT UNSIGNED,
            start_date DATETIME NOT NULL,
            end_date DATETIME NOT NULL,
            notes TEXT,
            INDEX ab_appointment_staff_id_idx (staff_id),
            INDEX ab_appointment_service_id_idx (service_id),
            INDEX ab_appointment_customer_id_idx (customer_id),
            CONSTRAINT fk_ab_appointment_staff_id
                FOREIGN KEY ab_appointment_staff_id_idx (staff_id)
                REFERENCES ab_staff(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            CONSTRAINT fk_ab_appointment_service_id
                FOREIGN KEY ab_appointment_service_id_idx (service_id)
                REFERENCES ab_service(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            CONSTRAINT fk_ab_appointment_customer_id
                FOREIGN KEY ab_appointment_customer_id_idx (customer_id)
                REFERENCES ab_customer(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
          "CREATE TABLE IF NOT EXISTS ab_payment (
            id              INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            created         DATETIME NOT NULL,
            type            ENUM('local', 'paypal') NOT NULL DEFAULT 'local',
            customer_id     INT UNSIGNED NOT NULL,
            token           VARCHAR(255) NOT NULL,
            status          VARCHAR(255) NOT NULL,
            transaction     VARCHAR(255) NOT NULL,
            total           DECIMAL(10, 2) NOT NULL DEFAULT  '0.00',
            INDEX           ab_payment_customer_id_idx (customer_id),
            CONSTRAINT      fk_ab_payment_customer_id
                FOREIGN KEY ab_payment_customer_id_idx (customer_id)
                REFERENCES  ab_customer(id)
                ON DELETE   CASCADE
                ON UPDATE   CASCADE
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
          "CREATE TABLE IF NOT EXISTS ab_payment_appointment (
            id              INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            date            DATETIME NOT NULL,
            payment_id      INT UNSIGNED NOT NULL,
            appointment_id  INT UNSIGNED NOT NULL,
            amount          DECIMAL(10, 2) NOT NULL DEFAULT  '0.00',
            INDEX           ab_payment_appointment_appointment_id_idx (appointment_id),
            INDEX           ab_payment_appointment_payment_id_idx (payment_id),
            CONSTRAINT      fk_ab_payment_appointment_appointment_id
                FOREIGN KEY ab_payment_appointment_appointment_id_idx (appointment_id)
                REFERENCES  ab_appointment(id)
                ON DELETE   CASCADE
                ON UPDATE   CASCADE,
            CONSTRAINT      fk_ab_payment_appointment_payment_id
                FOREIGN KEY ab_payment_appointment_payment_id_idx (payment_id)
                REFERENCES  ab_payment(id)
                ON DELETE   CASCADE
                ON UPDATE   CASCADE
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci"
      );

      $wpdb->query(
        "CREATE TABLE IF NOT EXISTS ab_holiday (
              id        INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
              staff_id  INT UNSIGNED NULL DEFAULT NULL,
              parent_id  INT UNSIGNED NULL DEFAULT NULL,
              holiday   DATETIME NOT NULL,
              repeat_event TINYINT ( 1 ) NOT NULL DEFAULT '0',
              title     VARCHAR(255) NOT NULL DEFAULT '',
              CONSTRAINT fk_ab_holiday_staff_id FOREIGN KEY ab_holiday_staff_id_idx(staff_id) REFERENCES ab_staff(id) ON DELETE CASCADE
          ) ENGINE = INNODB
          DEFAULT CHARACTER SET = utf8
          COLLATE = utf8_general_ci"
      );
  }