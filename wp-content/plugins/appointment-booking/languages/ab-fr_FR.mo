��    �      t  9  �      �  9  �  j  �  T  V  3  �  g   �     G     U     h     p     u     z     �     �     �     �     �     �  
     /         P     h  P     ,   �  U   �  R   S  4   �  m   �  V   I     �     �     �     �     �     �     �     �               (  	   5     ?     F  
   J     U     [     l     r     �     �     �  �   �          &     5     >     E     N     T     \     i     v          �  	   �     �     �     �     �  !   �  !   �      �          &     ;     L     T     ]     b     k     |     �     �     �  =   �  7   �     )      2   	   9   .   C      r      v   	   {      �      �      �      �   �   �   t   X!     �!     �!     �!     �!      "     "     
"     "  
   #"     ."     ?"     E"     K"     O"     W"     ["     `"     m"     z"     �"     �"     �"     �"     �"  -   �"  	    #  '   
#     2#     8#  0   I#  2   z#  6   �#     �#     �#     �#     �#     $     $     $     $     &$     ,$  #   2$     V$  *   n$     �$     �$     �$     �$     �$     �$     �$     �$     �$     �$     %     %  
   %     '%     5%     C%     L%     \%     q%     �%     �%     �%  	   �%     �%     �%     �%     �%     �%     �%     &     &     ,&     :&  
   H&     S&     [&  d   _&  /   �&  '   �&  K   '  7   h'  ,   �'  
   �'     �'     �'     �'     �'  O   �'     :(     @(     S(     [(     _(     d(     r(     {(     �(  �   �(     :)     Y)     a)     e)  	   j)  �   t)     D*  !   a*  $   �*     �*  *   �*  *   �*  0   +      N+      o+      �+  -   �+     �+  &   �+  !   &,  +   H,  /   t,  "   �,     �,  $   �,  	   -     -     -     ,-     ;-     G-     T-     b-  s  e-  3  �/  R  1  $  `2  >  �3  h   �4     -5     ?5     Y5     h5     k5     p5     w5     �5  #   �5  (   �5     �5  ,   6     G6  <   P6     �6     �6  N   �6  +   7  V   97  P   �7  2   �7  y   8  R   �8     �8     �8      9  %   9     :9     M9     U9     g9     x9     �9     �9     �9     �9  	   �9  	   �9  	   �9     �9     :     
:     :     !:     (:  �   /:     �:     �:  
   �:     	;  
   ;     ;  	   %;     /;     >;     R;     Y;     r;     y;     �;     �;     �;  	   �;     �;     �;     �;     �;     �;     <     <     '<     3<     8<     ?<     U<     \<     e<  "   m<     �<  ,   �<     �<     �<     �<  ,   �<     )=     -=     0=     <=     D=     Y=     x=  r   �=  r   >     ~>     �>     �>     �>     �>     �>     �>     �>     �>     �>     ?     ?     ?     ?     ?     #?     '?     ;?     J?     a?  $   u?     �?     �?     �?  0   �?     �?  +    @     ,@     2@  4   C@  7   x@  B   �@     �@     A     
A     A     A     A  	   &A     0A     9A     EA  *   KA     vA  3   �A     �A     �A     �A     �A     �A     B     B     )B     6B     :B     FB     \B     rB     �B     �B     �B     �B  &   �B      C  %   C     ?C     RC  	   bC     lC     tC     �C  	   �C     �C      �C     �C     �C     �C     �C     D     D     !D  z   %D     �D  *   �D  E   �D  B   0E  :   sE     �E     �E     �E     �E     �E  e   �E     .F     :F     PF     WF     [F     `F  
   pF     {F     �F  }  �F  #   H     6H     ?H     CH     KH  �   PH     7I  #   WI  '   {I  '   �I  *   �I  +   �I  9   "J  #   \J     �J  /   �J  -   �J  '   �J  &   &K  /   MK  (   }K  3   �K  !   �K     �K  %   L     @L     RL     ZL     kL     |L     �L     �L     �L     r   =   �   �   �   �   Z   o   �   ]   �   �       �   �   �      �   �   �   �   �   �       �      �   �   O   �   X   *   M          ^   �   �       K   �   S       �   �   L       <               V   �   +   U          }   �   5   �   t   )      k   P   7   e       I       �       [           &      �       �   A   @   �         �          ?   a          \       �   y   �   E   �      /                         �      �   �   `   T   !       �   �      x   �   �   u   �   l   �   �   b   
      �           �   3          �   �   �   ;   B   "       ,          �      J   '       �   �   6       �       �   �   �   9   v               �       �       #   |   �             �       0       $           �       �   N   �   �   �   D   �   �       �      �   .   F   �       �   %       �               	       �       �   Q           8   �   g   �       1   m       :      �       {   �   �       n   _   4           Y   �   w               �   �                       >   �   �   �   z   �       f   2   �   �   W   (   p   �   �   G      �       c   �       �   �       -       �       �   ~          H   �   �       i   R       q      �            �       s   C   �   �   d   �       j   h       �   �   �    
            Dear [[CLIENT_NAME]].

            Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

            Thank you and we look forward to seeing you again soon,

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Dear [[CLIENT_NAME]].

            This is confirmation that you have booked [[SERVICE_NAME]].

            We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]] .

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Dear [[CLIENT_NAME]].

            We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Hello.

            You have new booking.

            Service: [[SERVICE_NAME]]
            Date: [[APPOINTMENT_DATE]]
            Time: [[APPOINTMENT_TIME]]
            Client name: [[CLIENT_NAME]]
            Client phone: [[CLIENT_PHONE]]
            Client email: [[CLIENT_EMAIL]]
         
            Hello.

            Your agenda for tomorrow is:

            [[NEXT_DAY_AGENDA]]
          All services  No staff selected  Reset   to  %d h %d min * Invalid email * Invalid phone number * Please select a service * Please tell us your email * Please tell us your name * Please tell us your phone * Required * The start time must be less than the end time -- Select a customer -- -- Select a service -- <b>[[SERVICE_NAME]]</b> - name of service, <b>[[STAFF_NAME]]</b> - name of staff <b>[[SERVICE_PRICE]]</b> - price of service. <b>[[SERVICE_TIME]]</b> - time of service,  <b>[[SERVICE_DATE]]</b> - date of service <b>[[STAFF_NAME]]</b> - name of staff,  <b>[[SERVICE_NAME]]</b> - name of service, <h3>No time is available for selected criteria.</h3> <h3>Thank you! Booking process is completed. An email with details of your booking has been sent to you.</h3> <h3>The selected time is not available anymore. Please, choose another time slot.</h3> API Password API Signature API Username Add Booking Form Add Service Address All Services All customers All payment types All providers All services All staff Amount Any Appearance Apply Appointment Date April Are you sure? August Avatar Back Below you can find list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking. Booking Info Business hours Calendar Cancel Category Clear Company Company logo Company name Currency Custom Range Customer Customers Date Day Days off December Default value for category select Default value for employee select Default value for service select Delete Delete current photo Delete this user Details Disabled Done Duration Edit appointment Email Employee Enabled Error adding the break interval Evening notification with the next day agenda to Staff Member Evening reminder to Customer about next day Appointment February Filter Finish by Follow-up message on the day after Appointment Fri From Full name Holidays I will pay locally I will pay now with PayPal I'm available on or after If this staff member requires separate login to access personal calendar, a regular WP user needs to be created for this purpose. If you will leave this field blank, this staff member will not be able to access personal calendar using WP backend. Insert Invalid date or time Invalid number January July June Last 30 Days Last 7 Days Last Month Last appointment Local March May Message Mon Name New Category New Customer New Staff Member New appointment New booking information New customer Next No customers No payments for selected period and criteria. No result No services found. Please add services. Notes Notes (optional) Notification settings were updated successfully. Notification to Customer about Appointment details Notification to Staff Member about Appointment details Notifications November OFF October PayPal Payment Payments Period Phone Photo Please select at least one service. Please select service:  Please tell us how you would like to pay:  Price Provider Purchase Code Quick search customer Repeat every year Required Reset Sandbox Mode Sat Save Save Changes Save Member Save break Save category Save customer Schedule Select category Select from WP users Select service Send copy to administrators Sender email Sender name September Service Service paid locally Services Settings Settings saved. Show Form Progress Tracker Staff Staff Member Staff Members Staff members Start from Subject Sun Thank you! Booking process is completed. An email with details of your booking has been sent to you. The appointment has been successfully canceled. The requested interval is not available The selected period does't match default duration for the selected service! The selected period is occupied by another appointment! The start time must be less than the end one This Month Thu Time Title To To send scheduled notifications please execute following script with your cron: Today Total appointments Total:  Tue Type Uncategorized Untitled Update User User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings. We are not working on this day Website Wed Week Yesterday You selected to book [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. Price for the service is [[SERVICE_PRICE]].
Please provide your details in form below to proceed with booking. Your Appointment Information Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your visit to [[COMPANY_NAME]] [[APPOINTMENT_DATE]] - date of appointment [[APPOINTMENT_TIME]] - time of appointment [[CANCEL_APPOINTMENT]] - cancel appointment link [[CLIENT_EMAIL]] - сlient email [[CLIENT_NAME]] - name of client [[CLIENT_PHONE]] - сlient phone [[COMPANY_ADDRESS]] - address of your company [[COMPANY_LOGO]] - company logo [[COMPANY_NAME]] - name of the company [[COMPANY_PHONE]] - company phone [[COMPANY_WEBSITE]] - this web-site address [[NEXT_DAY_AGENDA]] - staff agenda for next day [[SERVICE_NAME]] - name of service [[STAFF_NAME]] - name of staff [[TOMORROW_DATE]] - date of next day add break breaks: characters max characters min is too long is too short staff members to Project-Id-Version: Bookly v1.2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-10 14:54+0200
PO-Revision-Date: 2014-07-10 14:54+0200
Last-Translator: Fred P. <contact@frederic-pastel.com>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.6.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
 
            Cher [[CLIENT_NAME]].

            Merci d'avoir choisi [[COMPANY_NAME]]. Nous espèrons vous avoir donné satisfaction lors du [[SERVICE_NAME]].

            Nous restons à votre disposition,

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Cher [[CLIENT_NAME]].

            Nous vous confirmons la réservation pour [[SERVICE_NAME]].

            Votre rendez-vous aura lieu le [[APPOINTMENT_DATE]] à [[APPOINTMENT_TIME]] .

            Merci de votre confiance.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Cher [[CLIENT_NAME]].

            Nous vous rappelons que vous avez pris rendez-vous pour [[SERVICE_NAME]] demain à [[APPOINTMENT_TIME]].

            Merci de votre confiance.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Bonjour.

            Il y a de nouvelles réservations.

            Service: [[SERVICE_NAME]]
            Date: [[APPOINTMENT_DATE]]
            Heure: [[APPOINTMENT_TIME]]
            Nom du Client: [[CLIENT_NAME]]
            Téléphone: [[CLIENT_PHONE]]
            E-mail: [[CLIENT_EMAIL]]
         
            Bonjour.

            Votre planning pour demain:

            [[NEXT_DAY_AGENDA]]
         Tous les Services Aucun Staff sélectionné Réinitialiser à %d h %d min * Adresse e-mail invalide * N° de téléphone invalide * Veuillez sélectionner un service * Veuillez indiquer votre adresse e-mail * Veuillez indiquer votre nom * Veuillez indiquer votre n° de téléphone * Requis * L'heure de début doit être antérieure à l'heure de fin -- Choisir un client -- -- Choisir un Service -- <b>[[SERVICE_NAME]]</b> - nom du Service, <b>[[STAFF_NAME]]</b> - nom du Staff <b>[[SERVICE_PRICE]]</b> - prix du service. <b>[[SERVICE_TIME]]</b> - heure du Service,  <b>[[SERVICE_DATE]]</b> - date du Service <b>[[STAFF_NAME]]</b> - nom du Staff,  <b>[[SERVICE_NAME]]</b> - nom du Service, <h3>Pas de disponibilité dans ces critères.</h3> <h3>Merci! La réservation est terminée. Vous allez recevoir un e-mail contenant les détails de votre rendez-vous.</h3> <h3>Ce créneau horaire n'est plus disponible. Veuillez modifier votre choix.</h3> Mot de passe API Signature API Nom utilisateur API Ajouter un formulaire de Réservation Ajouter un Service Adresse Tous les Services Tous les clients Tous les types de paiement Tous les Prestataires Tous les services Tous les Staff Montant N'importe Apparence Appliquer Date du Rendez-Vous Avril Etes-vous sûr ? Août Avatar Retour Les créneaux disponibles sont affichés ci-dessous pour [[SERVICE_NAME]] avec [[STAFF_NAME]].
Cliquez sur un bloc pour démarrer le processus de réservation. Informations de réservation Heures de travail Calendrier Annuler Catégorie Nettoyer Société Logo Société NOm de la Société Devise Intervalle personnalisé Client Clients Date Jour Jours de repos Décembre Catégorie par défaut Employé par défaut Service par défaut Effacer Effacer la photo actuelle Effacer cet utilisateur Détails Désactivé Fait Durée Editer le Rendez-vous E-mail Employé Activé Erreur pendant l'ajout de la pause Notification au Staff la veille Rappel de Rendez-Vous au Client le lendemain Février Filtrer Se termine à Message de suivi le lendemain du Rendez-Vous Ven De Nom complet Congés Je paierai sur place Je paie maintenant avec PayPal Je suis disponible à partir du Si le membre du Staff requiert un accès séparé à son calendrier, un utilisateur standard WP doit être créé. SI vous laissez ce champ vide, ce membre du Staff ne pourra pas accéder à son agenda depuis l'administration WP. Insérer Date ou heure invalide Numéro invalide Janvier Juillet Juin 30 derniers jours 7 derniers jours Mois dernier Dernier Rendez-Vous Local Mars Mai Message Lun Nom Nouvelle catégorie Nouveau Client Nouveau mebre du Staff Nouveau Rendez-Vous Nouvelle information de réservation Nouveau client Suivant Pas de client Pas de paiement pour la période sélectionnée. Aucun résultat Aucun service. Veuillez ajouter un Service. Notes Notas (opcional) Les réglages de notifications ont été mis à jour Notification au Client pour les détails du Rendez-Vous Notification aux membres du Staff pour les détails de Rendez-Vous Notifications Novembre OFF Octobre PayPal Paiement Paiements Période Téléphone Photo Veuillez sélectionner au moins un Service Choisissez le Service: Veuillez nous indiquer le moyen de paiement choisi: Prix Fournisseur Code Commande Recherche rapide de Client Répéter chaque année Requis Réinitialiser Mode Sandbox Sam Sauvegarder Sauvegarder 'Changes' Sauvegarder le Membre Sauvegarder la pause Sauvegarder la Catégorie Sauvegarder le Client Agenda Selectionner la Catégorie Sélectionner dans les utilisateurs WP Sélectionner le Service Envoyer une copie aux Administrateurs E-mail expéditeur Nom expéditeur Septembre Service Service payé sur place Services Réglages Réglages enregistrés Afficher la barre de progression Staff Membre du Staff Membres du Staff Membres du Staff Commence à Sujet Dim Merci! La demande de Rendez-vous est terminée. Vous allez recevoir un email contenant les détails de votre réservation. Le Rendez-Vous a été annulé La période demandée n'est pas disponible La période sélectionnée ne correspond pas à la durée du Service! La période sélectionnée est réservée à un autre Rendez-vous! L'heure de début doit être antérieure à l'heure de fin Ce mois Jeu Heure Titre A Para enviar notificaciones programadas por favor ejecute siguiente secuencia de comandos con el cron: Aujourd'hui Total des Rendez-vous Total: Mar Type Sans catégorie Sans titre Mise à jour Utilisateur User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings.Les utilisateurs "administrateurs" ont accès aux calendriers et réglages de tous les Membres de Staff, les autres utilisateurs ont accès uniquement à leur calendrier et à leurs réglages personnels. Nous ne travaillons par ce jour là SIte Web Mer Semaine Hier Vous avez choisi de réserver [[SERVICE_NAME]] avec [[STAFF_NAME]] à [[SERVICE_TIME]] le [[SERVICE_DATE]]. Le prix de ce service est de [[SERVICE_PRICE]].
Veuillez renseigner le formulaire ci-dessous pour valider la réservation. Vos informations de Rendez-vous Votre agenda pour [[TOMORROW_DATE]] Votre Rendez-vous avec [[COMPANY_NAME]] Votre Rendez-vous avec [[COMPANY_NAME]] [[APPOINTMENT_DATE]] - date de Rendez-Vous [[APPOINTMENT_TIME]] - heure de rendez-Vous [[CANCEL_APPOINTMENT]] - lien d'annulation du Rendez-vous [[CLIENT_EMAIL]] - e-mail du client [[CLIENT_NAME]] - nom du client [[CLIENT_PHONE]] - N° de téléphone du client [[COMPANY_ADDRESS]] - Adresse de la société [[COMPANY_LOGO]] - Logo de la Société [[COMPANY_NAME]] - Nom de la Société [[COMPANY_PHONE]] - Téléphone de la Société [[COMPANY_WEBSITE]] - Adresse de ce site [[NEXT_DAY_AGENDA]] - planning du staff pour demain [[SERVICE_NAME]] - Nom du Service [[STAFF_NAME]] - nom du staff [[TOMORROW_DATE]] - date de lendemain ajouter une pause pauses: caractères max. caractères min. est trop long est trop court membres du Staff à 