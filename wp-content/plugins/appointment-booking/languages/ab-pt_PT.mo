��    �      4  3  L        9    j  S  T  �  3    g   G     �     �     �     �     �     �               :     U  
   q  /   |     �     �  P   �  ,   ,  U   Y  R   �  4     m   7  V   �     �     	          $     5     A     I     V     d     v     �  	   �     �     �  
   �     �     �     �     �     �     �     �  �   �     u     �     �     �     �     �     �     �     �     �     �     �  	   �     �                  !     !   8      Z     {     �     �     �     �     �     �     �     �     �     �     �  =     7   M     �     �  	   �  .   �     �     �  	   �     �     �     �        �   2      �      �      �      �      �      �      �      �   
   
!     !     &!     ,!     2!     6!     >!     B!     G!     T!     a!     r!     �!     �!     �!     �!  -   �!  	   �!  '   �!     "  0   "  2   P"  6   �"     �"     �"     �"     �"     �"     �"     �"     �"     �"     #  #   #     ,#  *   D#     o#     u#     ~#     �#     �#     �#     �#     �#     �#     �#     �#     �#  
   �#     �#     $     $     "$     2$     G$     V$     r$     $  	   �$     �$     �$     �$     �$     �$     �$     �$     �$     %  
   %     %     $%     (%  d   .%  '   �%  7   �%  ,   �%  
    &     +&     /&     4&     :&  O   =&     �&     �&     �&     �&     �&     �&     �&     �&     �&  �   �&     �'     �'     �'     �'  	   �'  �   �'     �(  !   �(  $   �(     �(  *   )  *   E)      p)      �)      �)  -   �)     *  &   !*  !   H*  +   j*  "   �*     �*  $   �*  	   �*     +     +     +     -+     9+     F+     T+  m  W+  �  �-  �  ^/  ~  �0  J  y2  g   �3     ,4     @4     X4     a4     h4     {4     �4      �4  -   �4  -   
5     85  :   G5     �5     �5  Q   �5  .   6  [   <6  S   �6  F   �6  t   37  d   �7     8     8     .8  #   =8     a8  	   t8     ~8     �8     �8     �8     �8     �8     �8     �8  
   �8     	9     9     $9     *9     99     @9     G9  �   N9     �9     :     :      :  	   ):     3:     ::     B:     V:     f:     l:     �:     �:     �:     �:     �:     �:  $   �:  '   �:  #   ;     &;     /;     I;     b;  	   k;     u;  	   ~;     �;     �;     �;     �;  %   �;  G   �;  M   $<  	   r<     |<     �<  "   �<     �<     �<     �<     �<     �<     �<      �<  �   =     �=     �=     �=     �=     �=     �=     �=     �=     	>     >     )>     />     6>     ;>     D>     H>     M>     \>     i>     ~>     �>     �>     �>     �>  7   �>     	?  9   ?     R?  <   X?  >   �?  F   �?     @     *@     3@     <@     D@  	   K@  
   U@     `@     i@     r@  *   w@     �@  >   �@      A  
   A     A     $A     @A     VA     cA     jA     wA     |A     �A     �A     �A     �A     �A     �A     �A     �A     B  )   !B     KB     _B     qB     zB     �B  	   �B     �B     �B  (   �B     �B     �B     C     C     "C     *C  
   .C  �   9C  ,   �C  8   �C  ;   -D  	   iD     sD     wD     �D     �D  O   �D     �D     �D     �D     �D     E     E     E  	   %E  
   /E  �   :E  "   F     9F     >F     BF     IF    OF     XG  +   wG  $   �G     �G  *   �G  )   H  $   ;H  !   `H  &   �H  .   �H  &   �H  "   �H  (   "I  .   KI  $   zI     �I  (   �I     �I     �I     �I     J     )J     9J     IJ     YJ     �   =   �           Q              �   �       3           �   x   B   D   C       _   �   g       "   c       d   $         �      G   T   �   �              �       Z   :      A   9       �      �   |   �   5   l   `       W      ;       �      �   ~   �               8   -      �      v       �           z   �   0   {   �   #       b   �      �   �   I   F   �   �   �       �   (   )   �       �   �   X      o   L       N   ]   q   �   �   �   M   �         �   �       �   �   �   K   �       +   �              �   �   ?   �   �   �         �   �   �       �   �   �             �           ^       �   @       �   4   w       �   �   �   7       s           
      P   p   �   O   �       H       �       a   �           �               /   !   �   J       �          Y   m       �   �       �   �       �   �   �       �           2   �   �           \       u       E   j   �   �      &       �   h   y       �          �   .   �       R   �   U   �   �   �   �       k   �       �   �   �           i                 �       <   �   %          V          e   [   �   ,       '   r   >   S   6   *   n   f               �   �   }   t       �   1   �       	       �       �   �       
            Dear [[CLIENT_NAME]].

            Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

            Thank you and we look forward to seeing you again soon,

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Dear [[CLIENT_NAME]].

            This is confirmation that you have booked [[SERVICE_NAME]].

            We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]] .

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Dear [[CLIENT_NAME]].

            We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Hello.

            You have new booking.

            Service: [[SERVICE_NAME]]
            Date: [[APPOINTMENT_DATE]]
            Time: [[APPOINTMENT_TIME]]
            Client name: [[CLIENT_NAME]]
            Client phone: [[CLIENT_PHONE]]
            Client email: [[CLIENT_EMAIL]]
         
            Hello.

            Your agenda for tomorrow is:

            [[NEXT_DAY_AGENDA]]
          All services  No staff selected  Reset   to  * Invalid email * Invalid phone number * Please select a service * Please tell us your email * Please tell us your name * Please tell us your phone * Required * The start time must be less than the end time -- Select a customer -- -- Select a service -- <b>[[SERVICE_NAME]]</b> - name of service, <b>[[STAFF_NAME]]</b> - name of staff <b>[[SERVICE_PRICE]]</b> - price of service. <b>[[SERVICE_TIME]]</b> - time of service,  <b>[[SERVICE_DATE]]</b> - date of service <b>[[STAFF_NAME]]</b> - name of staff,  <b>[[SERVICE_NAME]]</b> - name of service, <h3>No time is available for selected criteria.</h3> <h3>Thank you! Booking process is completed. An email with details of your booking has been sent to you.</h3> <h3>The selected time is not available anymore. Please, choose another time slot.</h3> API Password API Signature API Username Add Booking Form Add Service Address All Services All customers All payment types All providers All services All staff Amount Any Appearance Apply Appointment Date April Are you sure? August Avatar Back Below you can find list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking. Booking Info Business hours Calendar Cancel Category Clear Company Company logo Company name Currency Custom Range Customer Customers Date Day Days off December Default value for category select Default value for employee select Default value for service select Delete Delete current photo Delete this user Details Disabled Done Duration Edit appointment Email Employee Enabled Error adding the break interval Evening notification with the next day agenda to Staff Member Evening reminder to Customer about next day Appointment February Filter Finish by Follow-up message on the day after Appointment Fri From Full name Holidays I will pay locally I will pay now with PayPal I'm available on or after If this staff member requires separate login to access personal calendar, a regular WP user needs to be created for this purpose. Insert Invalid date or time Invalid number January July June Last 30 Days Last 7 Days Last Month Last appointment Local March May Message Mon Name New Category New Customer New Staff Member New appointment New booking information New customer Next No customers No payments for selected period and criteria. No result No services found. Please add services. Notes Notification settings were updated successfully. Notification to Customer about Appointment details Notification to Staff Member about Appointment details Notifications November OFF October PayPal Payment Payments Period Phone Photo Please select at least one service. Please select service:  Please tell us how you would like to pay:  Price Provider Purchase Code Quick search customer Repeat every year Required Reset Sandbox Mode Sat Save Save Changes Save Member Save break Save category Save customer Schedule Select category Select from WP users Select service Send copy to administrators Sender email Sender name September Service Service paid locally Services Settings Settings saved. Show Form Progress Tracker Staff Staff Members Staff members Start from Subject Sun Tags  Thank you! Booking process is completed. An email with details of your booking has been sent to you. The requested interval is not available The selected period is occupied by another appointment! The start time must be less than the end one This Month Thu Time Title To To send scheduled notifications please execute following script with your cron: Today Total appointments Total:  Tue Type Uncategorized Untitled Update User User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings. We are not working on this day Website Wed Week Yesterday You selected to book [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. Price for the service is [[SERVICE_PRICE]].
Please provide your details in form below to proceed with booking. Your Appointment Information Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your visit to [[COMPANY_NAME]] [[APPOINTMENT_DATE]] - date of appointment [[APPOINTMENT_TIME]] - time of appointment [[CLIENT_EMAIL]] - сlient email [[CLIENT_NAME]] - name of client [[CLIENT_PHONE]] - сlient phone [[COMPANY_ADDRESS]] - address of your company [[COMPANY_LOGO]] - company logo [[COMPANY_NAME]] - name of the company [[COMPANY_PHONE]] - company phone [[COMPANY_WEBSITE]] - this web-site address [[SERVICE_NAME]] - name of service [[STAFF_NAME]] - name of staff [[TOMORROW_DATE]] - date of next day add break breaks: characters max characters min is too long is too short staff members to Project-Id-Version: Bookly v1.2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-10 14:55+0200
PO-Revision-Date: 2014-07-10 14:55+0200
Last-Translator: Jonadabe <jondabe@hotmail.com>
Language-Team: 
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 
            Prezado [[CLIENT_NAME]].

            Obrigado por escolher os nossos serviços da empresa [[COMPANY_NAME]]. Esperamos que tenhamos estado à altura das suas espectativas com o serviço de [[SERVICE_NAME]].

            Obrigado e aguardamos por uma nova visita e usufruto dos nossos serviços,

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Prezado [[CLIENT_NAME]].

            Isto é uma confirmação de uma marcação para [[SERVICE_NAME]].

            Aguardamos por si nas nossas instalações situada na [[COMPANY_ADDRESS]] a [[APPOINTMENT_DATE]] às [[APPOINTMENT_TIME]] .

            Obrigado por escolher os nossos serviços.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Prezado [[CLIENT_NAME]].

            Gostariamos de relembrar que agendou uma marcação para [[SERVICE_NAME]] amanhã às [[APPOINTMENT_TIME]]. Aguardamos por si nas nossas instalações na [[COMPANY_ADDRESS]].

            Obrigado por escolher os nossos serviços.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Olá.

            Tem uma nova marcação.

            Serviço: [[SERVICE_NAME]]
            Data: [[APPOINTMENT_DATE]]
            Horário: [[APPOINTMENT_TIME]]
            Nome do cliente: [[CLIENT_NAME]]
            Telefone do cliente: [[CLIENT_PHONE]]
            E-mail do cliente: [[CLIENT_EMAIL]]
         
            Olá.

            A sua agenda para amanhã é:

            [[NEXT_DAY_AGENDA]]
          Todos os serviços  Nenhum staff escolhido  Limpar   para  * E-mail inválido * Número de telefone inválido * Por favor escolha um serviço * Por favor indique o seu e-mail * Por favor indique o seu número de telefone * Por favor indique o seu número de telefone * Obrigatório * O horário inicial tem de ser menor que o horário final – Escolher um cliente – – Escolher um serviço – <b>[[SERVICE_NAME]]</b> - nome do serviço, <b>[[STAFF_NAME]]</b> - nome do staff <b>[[SERVICE_PRICE]]</b> - preço do serviço. <b>[[SERVICE_TIME]]</b> - horário do serviço,  <b>[[SERVICE_DATE]]</b> - data do serviço <b>[[STAFF_NAME]]</b> - nome do staff,  <b>[[SERVICE_NAME]]</b> - nome do serviço, <h3>Nenhum horário está disponível para o critério escolhido.</h3> <h3>Obrigado! O processo de marcação está completo. Um e-mail com os detalhes foi enviado para o seu e-mail.</h3> <h3>O horário escolhido já não se encontra disponível. Por favor escolha um outro horário.</h3> Palavra-passe API Assinatura API Utilizador API Adicionar Formulário de Marcação Adicionar Serviço Endereço Todos os serviços Todos os clientes Tipos de pagamentos Todos os fornecedores Todos os serviços Todo o staff Quantia Qualquer Aparência Aplicar Data da marcação Abril Tem a certeza? Agosto Avatar Voltar Abaixo pode encontrar uma lista dos horários disponíveis para [[SERVICE_NAME]] com [[STAFF_NAME]].
Clique no horário para prosseguir com a marcação. Informação da Marcação Horário laboral Calendário Cancelar Categoria Limpar Empresa Logotipo da empresa Nome da empresa Moeda Intervalo Personalizado Cliente Clientes Data Dia Dias de folga Dezembro Valor padrão da categoria escolhida Valor padrão do funcionário escolhido Valor padrão do serviço escolhido Eliminar Eliminar fotografia atual Eliminar este utilizador Detalhes inválido Concluir Duração Editar marcação E-mail Funcionário Ativado Erro a adicionar o intervalo de pausa Notificação noturna com a agenda do dia seguinte para Membro do Staff Lembrete noturno para o Cliente acerca da Marcação agendada no dia seguinte Fevereiro Filtro Até Mensagem após o dia da Marcação Sex De Nome completo Feriados Pago no local Vou pagar agora com PayPal Estou disponível a ou depois de Se este membro do staff requer um login separado para aceder ao calendário, um utilizador normal do WP tem de ser criado para este propósito. Inserir Hora ou data inválida Número inválido Janeiro Julho Junho Últimos 30 Dias Últimos 7 Dias Último Mês Última marcação Local Março Maio Mensagem Seg Nome Nova Categoria Novo Cliente Novo Membro do Staff Nova marcação Informação de nova marcação Novo cliente Seguinte Sem clientes Nenhum pagamento para o período e/ou filtro escolhido. Sem resultados Nenhum serviço encontrado. Por favor adicione serviços. Notas Definições de notificação foram atualizadas com sucesso. Notificação para o Cliente acerca dos detalhes da Marcação Notificação para o Membro do Staff acerca dos detalhes da Marcação Notificações Novembro Desligar Outubro PayPal Pagamento Pagamentos Período Telefone Foto Por favor escolher pelo menos um serviço. Por favor escolha um serviço: Por favor queira informar-nos como deseja efetuar o pagamento: Preço Fornecedor Código de Compra Pesquisa rápida de cliente Repetir todos os anos Obrigatório Limpar Modo Sandbox Sáb Guardar Guardar Alterações Guardar Membro Guardar pausa Guardar categoria Guardar cliente Agendar Escolher categoria Escolher utilizadores do WP Escolher serviço Enviar uma cópia para os administradores E-mail do remetente Nome do remetente Setembro Serviço Serviço pago localmente Serviços Definições Definições guardadas. Ver processo de progresso no formulário Staff Membros do Staff Membros do Staff Desde Assunto Dom Etiquetas  Muito obrigado! O processo de agendamento da marcação está completo. Um e-mail com os seus detalhes da sua marcação foi enviado para si. O intervalo escolhido não está disponível O período escolhido está ocupado por outra marcação! O horário inicial tem de ser inferior que o horário final Este Mês Qui Horário Título Para Para enviar notificações agendadas por favor executar o seguinte script cron: Hoje Total de marcações Total: Ter Tipo Não categorizado Sem título Atualizar Utilizador Utilizador com permissões de "Administrador" terá acesso aos calendários e definições de todos os membros do staff. Um utilizador com outra permissão terá acesso apenas ao calendário pessoal e suas definições. Não estamos a trabalhar neste dia Site Qua Semana Ontem Escolheu uma marcação para [[SERVICE_NAME]] com [[STAFF_NAME]] às [[SERVICE_TIME]] em [[SERVICE_DATE]]. O preço para este serviço é de [[SERVICE_PRICE]].
Por favor forneça os seus detalhes no formulário abaixo para proceder com o agendamento da marcação. Informação da sua Marcação A sua agenda para amanhã [[TOMORROW_DATE]] A sua marcação na [[COMPANY_NAME]] A sua visita [[COMPANY_NAME]] [[APPOINTMENT_DATE]] - data da marcação  [[APPOINTMENT_TIME]] - hora da marcação [[CLIENT_EMAIL]] - e-mail do cliente [[CLIENT_NAME]] - nome do cliente [[CLIENT_PHONE]] - telefone do cliente [[COMPANY_ADDRESS]] - endereço da tua empresa [[COMPANY_LOGO]] - logotipo da empresa [[COMPANY_NAME]] - nome da empresa [[COMPANY_PHONE]] - telefone da empresa  [[COMPANY_WEBSITE]] - endereço deste website  [[SERVICE_NAME]] - nome do serviço  [[STAFF_NAME]] - nome do staff [[TOMORROW_DATE]] - data do dia seguinte adicionar pausa pausas: máximo de carateres mínimo de carateres demasiado longo demasiado curto mebros do staff para 