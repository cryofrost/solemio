��    �      |  =  �      �  9  �  j    T  v  3  �  g   �     g     u     �     �     �     �     �     �     �     �     �       
   5  /   @     p     �  P   �  ,   �  U     R   s  4   �  m   �  V   i     �     �     �     �     �                    (     :     H  	   U     _     f  
   j     u     {     �     �     �     �     �  �   �     9     F     U     ^     e     n     t     |     �     �     �     �  	   �     �     �     �     �  !   �  !   �           ?     F     [     l     t     }     �     �     �     �     �     �  =   �  7         I      R   	   Y   .   c      �      �   	   �      �      �      �      �   �   �   t   x!     �!     �!     	"     "      "     %"     *"     7"  
   C"     N"     _"     e"     k"     o"     w"     {"     �"     �"     �"     �"     �"     �"     �"     �"  -   �"  	    #  '   *#     R#     X#  0   i#  2   �#  6   �#     $     $     $     $     '$     .$     6$     ?$     F$     L$  #   R$     v$  *   �$     �$     �$     �$     �$     �$     �$     %     %     %     %     #%     0%  
   <%     G%     U%     c%     l%     |%     �%     �%     �%     �%  	   �%     �%     �%     �%     &     &     &     9&     ?&     L&     Z&  
   h&     s&     {&     &  d   �&  /   �&  '   '  K   B'  7   �'  ,   �'  
   �'     �'     (     (     (  O   (     `(     f(     y(     �(     �(     �(     �(     �(     �(  �   �(     `)     )     �)     �)  	   �)  �   �)     j*  !   �*  $   �*     �*  *   �*  *   +  0   C+      t+      �+      �+  -   �+     ,  &   %,  !   L,  +   n,  /   �,  "   �,     �,  $   -  	   1-     ;-     C-     R-     a-     m-     z-     �-  �  �-  �  %0  �  �1  �  �3  �  U5  �   �6     �7  "   �7     �7     �7     �7  	   �7  4   �7  2   "8  5   U8  Q   �8  6   �8  O   9     d9  U   }9  %   �9  #   �9  m   :  3   �:  g   �:  n   ';  _   �;  �   �;  �   �<     n=     {=     �=  4   �=     �=  
   �=     �=     
>       >  &   A>     h>     ~>  
   �>  
   �>     �>     �>     �>     �>     �>     ?     ?  
   +?    6?  0   U@     �@     �@     �@     �@     �@     �@     �@     A     )A  )   6A     `A     mA     |A     �A     �A     �A  A   �A  A   �A  =   ;B     yB  &   �B  2   �B     �B     �B     C  "   C  )   2C  !   \C     ~C     �C  E   �C  s   �C  n   ZD     �D     �D     �D  Y   �D     UE     ZE     ]E     qE  "   �E  !   �E  *   �E    �E  �   �F     �G  ,   �G  #   H     ,H     9H     BH     KH     jH     �H  !   �H     �H     �H     �H     �H     �H     I     	I     'I     ?I     ]I  ;   wI     �I  
   �I     �I  k   �I     ZJ  Z   xJ     �J  1   �J  U   K  W   nK  [   �K     "L     9L     FL     YL     hL     oL     |L     �L     �L     �L  L   �L  4   �L  e   2M     �M     �M  !   �M  (   �M  &   �M     &N     =N     NN     [N     `N  %   sN  %   �N  !   �N  %   �N  !   O     )O  #   6O  1   ZO     �O  >   �O  8   �O     "P     @P     QP  .   ^P     �P     �P  &   �P  4   �P     	Q     Q     'Q     8Q     IQ     YQ     bQ     gQ  �   pQ  8   ;R  A   tR  �   �R  I   dS  S   �S     T     T  
   !T     ,T     =T  �   BT     �T     U     U     'U     ,U     3U     MU     _U     pU  n  �U  /   �V     (W     8W     =W  
   JW    UW  9   jX  ,   �X  +   �X  %   �X  .   #Y  0   RY  H   �Y  C   �Y  '   Z  0   8Z  1   iZ  2   �Z  *   �Z  3   �Z  6   -[  \   d[  2   �[  *   �[  :   \     Z\     z\     �\     �\     �\     �\     ]     ]     s           D   �       b   �      �   �          �   j   �   v   A   �   �   �   �       �   �       2   �   �   �   �   �   �   �   �   l   ]   �   y       �   6   m   �   �   B   N              �   �       �   �   �       f   5   }   C   _   u       �   �       �         �       ?   �       L      �   �       w   �       �      �       �      �   �   �   �       7           �      �       �   `   �      �           �       �   {   �   T   H   �       &   @           	   q   �      t       g   (                   �   +   -   �           x   �      �   �   �   p   3   �   �       �       h   �   \           U   �             �   V   i       �   �   n   �   9   �   �   �   �   R      *                 o       a   e   �   �   1   ;   )   E   %   M   [          �         8      �           k   �              �   �   Q   �   �       �   �   <       r   �   O   =          �   �               W   �           S               Z   '   Y      �   ^       4          ~   $       J   �   #   !       F       .       |   �       z   �   �   �       /      
   �   K   �       �   X   :       �       �       �       I               G   �   �   �   �              >       �       �   c      ,              �   "   �   �   P   0   d    
            Dear [[CLIENT_NAME]].

            Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

            Thank you and we look forward to seeing you again soon,

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Dear [[CLIENT_NAME]].

            This is confirmation that you have booked [[SERVICE_NAME]].

            We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]] .

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Dear [[CLIENT_NAME]].

            We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

            Thank you for choosing our company.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Hello.

            You have new booking.

            Service: [[SERVICE_NAME]]
            Date: [[APPOINTMENT_DATE]]
            Time: [[APPOINTMENT_TIME]]
            Client name: [[CLIENT_NAME]]
            Client phone: [[CLIENT_PHONE]]
            Client email: [[CLIENT_EMAIL]]
         
            Hello.

            Your agenda for tomorrow is:

            [[NEXT_DAY_AGENDA]]
          All services  No staff selected  Reset   to  %d h %d min * Invalid email * Invalid phone number * Please select a service * Please tell us your email * Please tell us your name * Please tell us your phone * Required * The start time must be less than the end time -- Select a customer -- -- Select a service -- <b>[[SERVICE_NAME]]</b> - name of service, <b>[[STAFF_NAME]]</b> - name of staff <b>[[SERVICE_PRICE]]</b> - price of service. <b>[[SERVICE_TIME]]</b> - time of service,  <b>[[SERVICE_DATE]]</b> - date of service <b>[[STAFF_NAME]]</b> - name of staff,  <b>[[SERVICE_NAME]]</b> - name of service, <h3>No time is available for selected criteria.</h3> <h3>Thank you! Booking process is completed. An email with details of your booking has been sent to you.</h3> <h3>The selected time is not available anymore. Please, choose another time slot.</h3> API Password API Signature API Username Add Booking Form Add Service Address All Services All customers All payment types All providers All services All staff Amount Any Appearance Apply Appointment Date April Are you sure? August Avatar Back Below you can find list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking. Booking Info Business hours Calendar Cancel Category Clear Company Company logo Company name Currency Custom Range Customer Customers Date Day Days off December Default value for category select Default value for employee select Default value for service select Delete Delete current photo Delete this user Details Disabled Done Duration Edit appointment Email Employee Enabled Error adding the break interval Evening notification with the next day agenda to Staff Member Evening reminder to Customer about next day Appointment February Filter Finish by Follow-up message on the day after Appointment Fri From Full name Holidays I will pay locally I will pay now with PayPal I'm available on or after If this staff member requires separate login to access personal calendar, a regular WP user needs to be created for this purpose. If you will leave this field blank, this staff member will not be able to access personal calendar using WP backend. Insert Invalid date or time Invalid number January July June Last 30 Days Last 7 Days Last Month Last appointment Local March May Message Mon Name New Category New Customer New Staff Member New appointment New booking information New customer Next No customers No payments for selected period and criteria. No result No services found. Please add services. Notes Notes (optional) Notification settings were updated successfully. Notification to Customer about Appointment details Notification to Staff Member about Appointment details Notifications November OFF October PayPal Payment Payments Period Phone Photo Please select at least one service. Please select service:  Please tell us how you would like to pay:  Price Provider Purchase Code Quick search customer Repeat every year Required Reset Sandbox Mode Sat Save Save Changes Save Member Save break Save category Save customer Schedule Select category Select from WP users Select service Send copy to administrators Sender email Sender name September Service Service paid locally Services Settings Settings saved. Show Form Progress Tracker Staff Staff Member Staff Members Staff members Start from Subject Sun Tags  Thank you! Booking process is completed. An email with details of your booking has been sent to you. The appointment has been successfully canceled. The requested interval is not available The selected period does't match default duration for the selected service! The selected period is occupied by another appointment! The start time must be less than the end one This Month Thu Time Title To To send scheduled notifications please execute following script with your cron: Today Total appointments Total:  Tue Type Uncategorized Untitled Update User User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings. We are not working on this day Website Wed Week Yesterday You selected to book [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. Price for the service is [[SERVICE_PRICE]].
Please provide your details in form below to proceed with booking. Your Appointment Information Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your visit to [[COMPANY_NAME]] [[APPOINTMENT_DATE]] - date of appointment [[APPOINTMENT_TIME]] - time of appointment [[CANCEL_APPOINTMENT]] - cancel appointment link [[CLIENT_EMAIL]] - сlient email [[CLIENT_NAME]] - name of client [[CLIENT_PHONE]] - сlient phone [[COMPANY_ADDRESS]] - address of your company [[COMPANY_LOGO]] - company logo [[COMPANY_NAME]] - name of the company [[COMPANY_PHONE]] - company phone [[COMPANY_WEBSITE]] - this web-site address [[NEXT_DAY_AGENDA]] - staff agenda for next day [[SERVICE_NAME]] - name of service [[STAFF_NAME]] - name of staff [[TOMORROW_DATE]] - date of next day add break breaks: characters max characters min is too long is too short staff members to Project-Id-Version: Bookly v1.2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-10 14:55+0200
PO-Revision-Date: 2014-08-29 21:28+0400
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.6.8
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 
            Дорогой(ая) [[CLIENT_NAME]].

            Спасибо за выбор [[COMPANY_NAME]]. Мы надеемся, что Вы останетесь довольны [[SERVICE_NAME]].

            Спасибо Вам, ждем с нетерпением скорейшей встречи с Вами,

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Дорогой(ая) [[CLIENT_NAME]].

            Это подтверждение того, что Вы забронировали [[SERVICE_NAME]].

            Мы ждем Вас по адресу [[COMPANY_ADDRESS]] на [[APPOINTMENT_DATE]] в [[APPOINTMENT_TIME]] .

            Спасибо за выбор нашей компании.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Дорогой(ая) [[CLIENT_NAME]].

            Мы бы хотели Вам напомнить, что вы забронировали [[SERVICE_NAME]] завтра в [[APPOINTMENT_TIME]]. Мы ждем Вас по адресу [[COMPANY_ADDRESS]].

            Спасибо за выбор нашей компании.

            [[COMPANY_NAME]]
            [[COMPANY_PHONE]]
            [[COMPANY_WEBSITE]]
         
            Здравствуйте.

            У вас новя встреча.

            Сервис: [[SERVICE_NAME]]
            Дата: [[APPOINTMENT_DATE]]
            Время: [[APPOINTMENT_TIME]]
            Имя клиента: [[CLIENT_NAME]]
            Телефон клиента: [[CLIENT_PHONE]]
            Электронная почта клиента: [[CLIENT_EMAIL]]
         
            Здравствуйте.

            Ваш дневник завтрашних встреч:

            [[NEXT_DAY_AGENDA]]
         Все сервисы Персонал не выбран Сбросить до %d ч %d мин * Неверный электронный адрес * Неверный телефонный номер * Пожалуйста, выбирете сервис * Пожалуйста, введите свой электронный адрес * Пожалуйста, введите свое имя * Пожалуйста, введите свой телефонный номер * Обязательно * Начальное время должно быть меньше конечного -- Выберите клиента -- -- Выберите услугу -- <b>[[SERVICE_NAME]]</b> - название  сервиса, <b>[[STAFF_NAME]]</b> - имя работника <b>[[SERVICE_PRICE]]</b> - цена сервиса. <b>[[SERVICE_TIME]]</b> - время сервиса,  <b>[[SERVICE_DATE]]</b> - дата сервиса <b>[[STAFF_NAME]]</b> - имя работника,  <b>[[SERVICE_NAME]]</b> - название сервиса, <h3>По выбранным критериям нет доступного времени.</h3> <h3>Благодарим Вас! Ваше бронирование завершено. Вам отослано электронное письмо с деталями о Вашем бронировании.</h3> <h3>Выбранное время больше не доступно. Пожалуйста, выберите другой временной интервал.</h3> API Password API Signature API Username Добавить форму бронирования Добавить сервис Адрес Все сервисы Все клиенты Все типы платежей Все провайдеры услуг Все сервисы Весь персонал Сумма Любой Внешний вид Применить Дата встречи Апрель Вы уверены? Август Аватар Назад Ниже Вы можете найти список доступных временных интервалов для [[SERVICE_NAME]] от [[STAFF_NAME]].
Нажмите на кнопку временного интервала, чтобы продолжить бронирование. Информация о бронировании Часы работы Календарь Отмена Категория Очистить Компания Логотип компании Имя компании Валюта Произвольный диапазон Клиент Клиенты Дата День Нерабочие дни Декабрь Стандартное значение для категории Стандартное значение для работника Стандартное значение для сервиса Удалить Удалить текущее фото Удалить этого пользователя Детали Выключено Готово Продолжительность Редактировать встречу Электронная почта Работник Включено Ошибка добавления интервала перерыва Вечернее уведомление с данными о завтрашнем дне для персонала  Вечернее уведомление для клиента о завтрашнем бронировании Февраль Фильтр Закончить в Последующее сообщение в день после бронирования Пт С Полное имя Праздники Я оплачу наличными Я оплачу через PayPal Выберите удобное время Если этому сотруднику требуется отдельный логин для доступа в личный календарь, тогда для этой цели должен быть создан обычный пользователь WP. Если Вы не заполните это поле, работник  не сможет получить доступ к календарю через административный интерфейс. Вставить Неправильная дата/время Неправильный номер Январь Июль Июнь Последние 30 дней Последние 7 дней Прошлый месяц Последняя встреча За наличные Март Май Сообщение Пн Имя Новая категория Новый клиент Новый сотрудник Новая встреча Новая информация о бронировании Новый клиент Далее Нет клиентов Нет платежей за выбранный период и по выбранным критериям. Нет результатов Сервисы не найдены. Пожалуйста, добавьте сервисы. Стоимость Примечания (необязательно) Настройки уведомлений были успешно сохранены. Уведомление для клиента о деталях бронирования Уведомление для персонала о деталях бронирования Уведомления Ноябрь ВЫКЛЮЧИТЬ Октябрь PayPal Оплата Платежи Период Телефон Фото Пожалуйста, выберите минимум одну услугу. Пожалуйста, выбирете сервис: Пожалуйста, скажите нам - как бы Ва хотели расплатиться: Цена Провайдер Лицензионный ключ Быстрый поиск клиента Повторять каждый год Обязательно Сбросить Sandbox Mode Сб Сохранить Сохранить изменения Сохранить работника Сохранить перерыв Сохранить категорию Сохранить клиента График Выберите категорию Выбрать из пользователей WP Выберите услугу Отправлять копию администраторам Электронная почта отправителя Имя отправителя Сентябрь Услуга Оплата сервиса наличными Услуги Настройки Настройки сохранены. Показывать трекер прогресса Персонал Мастер Персонал Персонал Начать с Тема Вс Теги Благодарим Вас! Ваше бронирование завершено. Вам отослано электронное письмо с деталями о Вашем бронировании. Встреча была успешно отменена. Запрашиваемый интервал не доступен Выбранный период времени не совпадает со стандартным временем длительности выбранной услуги! Выбранный период занят другой встречей! Начальное время должно быть меньше конечного Текущий месяц Чт Время Название По Чтобы отправлять запланированные уведомления, пожалуйста используете следующий скрипт вашим cron: Сегодня Все встречи Всего: Вт Тип Без категории Без имени Обновить Пользователь Пользователь с ролью \ \"Администратор "  будет иметь доступ к календарям и настройкам всех сотрудников, пользователь с какой-то другой ролью будет иметь доступ только к личным календарем и настройкам. Мы не работаем в этот день Веб-сайт Ср Неделя Вчера Вы выбрали [[SERVICE_NAME]] от [[STAFF_NAME]] на [[SERVICE_TIME]] в [[SERVICE_DATE]]. Цена на этот сервис [[SERVICE_PRICE]].
Пожалуйста, заполните Ваши данные, чтобы продолжить бронирование. Ваша информация о бронировании Ваш дневник за [[TOMORROW_DATE]] Ваша встреча в [[COMPANY_NAME]] Ваш визит в [[COMPANY_NAME]] [[APPOINTMENT_DATE]] - дата встречи [[APPOINTMENT_TIME]] - время встречи [[CANCEL_APPOINTMENT]] - ссылка для отмены встречи [[CLIENT_EMAIL]] - электронная почта клиента [[CLIENT_NAME]] - имя клиента [[CLIENT_PHONE]] - телефон клиента [[COMPANY_ADDRESS]] - адрес компании [[COMPANY_LOGO]] - логотип компании [[COMPANY_NAME]] - имя компании [[COMPANY_PHONE]] - телефон компании [[COMPANY_WEBSITE]] - веб-сайт компании [[NEXT_DAY_AGENDA]] - дневник сотрудника на завтрашний день [[SERVICE_NAME]] - название сервиса [[STAFF_NAME]] - имя работника [[TOMORROW_DATE]] - дата завтрашнего дня добавить перерыв перерывы: знаков максимум знаков минимум слишком длинное слишком короткое персонал до 