<?php
 
class AB_ReminderEveningNextMorningJob extends AB_ScheduleJob {

    public function run() {
        $notification = $this->wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "evening_next_morning" AND active = 1' );

        if ( $notification ) {
            $date = new DateTime();
            if ( $date->format('H') >= 18 ) {
                $rows = $this->wpdb->get_results( '
                    SELECT a.*, c.*, s.*
                    FROM ab_appointment a
                    LEFT JOIN ab_customer c ON c.id = a.customer_id
                    LEFT JOIN ab_service s ON s.id = a.service_id
                    WHERE DATE(DATE_ADD(NOW(), INTERVAL 1 DAY)) = DATE(start_date) AND TIME(start_date) <= "12:00:00"
                      AND NOT EXISTS (SELECT id FROM ab_email_notification aen WHERE DATE(aen.created) = DATE(NOW()) AND aen.type = "reminder_evening_next_morning" AND aen.customer_id = a.customer_id)
                    ' );

                $replacement = new AB_NotificationReplacement();
                foreach ( $rows as $row ) {
                    $replacement->setClientName( $row->name );
                    $replacement->setAppointmentTime( $row->start_date );
                    $replacement->setServiceName( $row->title );
                    $message = $replacement->replace( $notification->message );

                    if ( wp_mail( $row->email, $notification->subject, $message ) ) {
                        $customerNotification = new AB_Email_Notification();
                        $customerNotification->set( 'customer_id', $row->customer_id );
                        $customerNotification->set( 'type', 'reminder_evening_next_morning' );
                        $customerNotification->set( 'created', $date->format('Y-m-d H:i:s') );
                        $customerNotification->save();
                    }
                }
            }
        }
    }
}
