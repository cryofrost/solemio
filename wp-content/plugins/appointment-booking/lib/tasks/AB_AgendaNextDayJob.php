<?php
 
class AB_AgendaNextDayJob extends AB_ScheduleJob {

    public function run() {
        $notification = $this->wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "event_next_day" AND active = 1' );

        if ( $notification ) {
            $date = new DateTime();
            if ( $date->format('H') >= 18 ) {
                $rows = $this->wpdb->get_results( '
                    SELECT a.*, c.*, s.*, st.full_name, st.email AS staff_email
                    FROM ab_appointment a
                    LEFT JOIN ab_customer c ON c.id = a.customer_id
                    LEFT JOIN ab_service s ON s.id = a.service_id
                    LEFT JOIN ab_staff st ON st.id = a.staff_id
                    WHERE DATE(DATE_ADD(NOW(), INTERVAL 1 DAY)) = DATE(a.start_date) AND TIME(a.start_date) <= "12:00:00"
                      AND NOT EXISTS (SELECT id FROM ab_email_notification aen WHERE DATE(aen.created) = DATE(NOW()) AND aen.type = "agenda_next_day" AND aen.staff_id = a.staff_id)
                    '
                );

                $staff_schedules = array();
                $staff_emails = array();
                foreach ( $rows as $row ) {
                    $staff_schedules[$row->staff_id][] = $row;
                    $staff_emails[$row->staff_id] = $row->staff_email;
                }

                foreach ( $staff_schedules as $staff_id => $collection ) {
                    $schedule = '<table>';
                    foreach ( $collection as $object ) {
                        $startDate = new DateTime($object->start_date);
                        $endDate = new DateTime($object->end_date);
                        $schedule .= '<tr>';
                        $schedule .= sprintf( '<td>%s<td>', ($startDate->format( 'H:i' ) . '-' . $endDate->format( 'H:i' )) );
                        $schedule .= sprintf( '<td>%s<td>', $object->title );
                        $schedule .= sprintf( '<td>%s<td>', $object->name );
                        $schedule .= '</tr>';
                    }
                    $schedule .= '</table>';

                    $replacement = new AB_NotificationReplacement();
                    $replacement->setNextDayAgenda( $schedule );
                    $message = $replacement->replace( $notification->message );

                    if ( wp_mail( $staff_emails[$staff_id], $notification->subject, $message ) ) {
                        $customerNotification = new AB_Email_Notification();
                        $customerNotification->set( 'staff_id', $staff_id );
                        $customerNotification->set( 'type', 'agenda_next_day' );
                        $customerNotification->set( 'created', $date->format('Y-m-d H:i:s') );
                        $customerNotification->save();
                    }
                }
            }
        }
    }
}
