<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
abstract class AB_ScheduleJob {

    /**
     * @var wpdb $wpdb
     */
    protected $wpdb;

    public function __construct() {
      global $wpdb;

      // Reference to global database object.
      $this->wpdb = $wpdb;
    }

    abstract public function run();
}
