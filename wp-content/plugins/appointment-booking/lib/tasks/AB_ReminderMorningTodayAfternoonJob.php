<?php
 
class AB_ReminderMorningTodayAfternoonJob extends AB_ScheduleJob {

    public function run() {
        $notification = $this->wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "morning_todays_afternoon" AND active = 1' );

        if ( $notification ) {
            $date = new DateTime();
            if ( $date->format('H') >= 8 ) {
                $rows = $this->wpdb->get_results( '
                    SELECT a.*, c.*, s.*
                    FROM ab_appointment a
                    LEFT JOIN ab_customer c ON c.id = a.customer_id
                    LEFT JOIN ab_service s ON s.id = a.service_id
                    WHERE DATE(NOW()) = DATE(start_date) AND TIME(start_date) >= "12:00:00"
                      AND NOT EXISTS (SELECT id FROM ab_email_notification aen WHERE DATE(aen.created) = DATE(NOW()) AND aen.type = "reminder_morning_today_afternoon" AND aen.customer_id = a.customer_id)
                    ' );

                $replacement = new AB_NotificationReplacement();
                foreach ( $rows as $row ) {
                    $replacement->setClientName( $row->name );
                    $replacement->setAppointmentTime( $row->start_date );
                    $replacement->setServiceName( $row->title );
                    $message = $replacement->replace( $notification->message );

                    if ( wp_mail( $row->email, $notification->subject, $message ) ) {
                        $customerNotification = new AB_Email_Notification();
                        $customerNotification->set( 'customer_id', $row->customer_id );
                        $customerNotification->set( 'type', 'reminder_morning_today_afternoon' );
                        $customerNotification->set( 'created', $date->format('Y-m-d H:i:s') );
                        $customerNotification->save();
                    }
                }
            }
        }
    }
}
