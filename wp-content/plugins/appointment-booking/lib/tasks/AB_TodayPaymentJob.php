<?php
 
class AB_TodayPaymentJob extends AB_ScheduleJob {

    public function run() {
        $notification = $this->wpdb->get_row( 'SELECT * FROM ab_notifications WHERE slug = "event_today_payment" AND active = 1' );

        if ( $notification ) {
            $date = new DateTime();
            if ( !$this->wpdb->get_var( "SELECT id FROM ab_email_notification WHERE type = 'today_payment_job' AND DATE(NOW()) = DATE(created)" ) ) {
                if ( $date->format('H') >= 21 ) {
                    $rows = $this->wpdb->get_results( '
                        SELECT p.*, c.*, pa.*, a.*, s.*, st.*
                        FROM ab_payment p
                        LEFT JOIN ab_payment_appointment pa ON pa.payment_id = p.id
                        LEFT JOIN ab_customer c ON c.id = p.customer_id
                        LEFT JOIN ab_appointment a ON a.id = pa.appointment_id
                        LEFT JOIN ab_service s ON s.id = a.service_id
                        LEFT JOIN ab_staff st ON st.id = a.staff_id
                        WHERE DATE(NOW()) = DATE(p.created)' );

                    $payments = '<table>';
                    foreach ( $rows as $row ) {
                        $payments .= '<tr>';
                        $payments .= sprintf( '<td>%s<td>', $row->full_name );
                        $payments .= sprintf( '<td>%s<td>', $row->title );
                        $payments .= sprintf( '<td>%s<td>',  $row->name );
                        $payments .= sprintf( '<td>%s<td>',  $row->total );
                        $payments .= '</tr>';
                    }
                    $payments .= '</table>';

                    $replacement = new AB_NotificationReplacement();
                    $replacement->setTodayPayments( $payments );
                    $message = $replacement->replace( $notification->message );

                    if ( wp_mail( get_option( 'admin_email' ), $notification->subject, $message ) ) {
                        $customerNotification = new AB_Email_Notification();
                        $customerNotification->set( 'type', 'today_payment_job' );
                        $customerNotification->set( 'created', $date->format('Y-m-d H:i:s') );
                        $customerNotification->save();
                    }
                }
            }
        }
    }
}
