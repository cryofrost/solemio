<?php

/**
 * Class AB_CommonUtils
 *
 */
class AB_CommonUtils {

    /**
     * Get e-mails of wp-admins
     *
     * @return array
     */
    public static function getAdminEmails() {
        return array_map(
            create_function( '$a', 'return $a->data->user_email;' ),
            get_users( 'role=administrator' )
        );
    } // getAdminEmails

    /**
     * Generates email's headers FROM: Sender Name < Sender E-mail >
     *
     * @return string
     */
    public static function getEmailHeaderFrom() {
        $from_name  = get_option( 'ab_settings_sender_name' );
        $from_email = get_option( 'ab_settings_sender_email' );
        $from = $from_name . ' <' . $from_email . '>';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: '.$from.'' . "\r\n";

        return $headers;
    } // getEmailHeaderFrom

    /**
     * Format price based on currency settings (Settings -> Payments).
     *
     * @param  string $price
     * @return string
     */
    public static function formatPrice( $price ) {
        $result = '';
        switch ( get_option( 'ab_paypal_currency' ) ) {
            case 'EUR' :
                $result = '€' . $price;
                break;
            case 'USD' :
                $result = '$' . $price;
                break;
            case 'AUD' :
                $result = 'A$' . $price;
                break;
            case 'CAD' :
                $result = 'C$' . $price;
                break;
            case 'JPY' :
                $result = '¥' . $price;
                break;
            case 'GBP' :
                $result = '£' . $price;
                break;
            case 'DKK' :
                $result = $price . ' kr';
                break;
            case 'SEK' :
                $result = $price . ' kr';
                break;
            case 'RUB' :
                $result = $price . ' руб.';
                break;
        } // switch

        return $result;
    } // formatPrice

    /**
     * Format DateTime by User Settings
     *
     * @param string $dateTime
     *
     * @return string $dateTime
     */
    public static function getFormattedDateTime( $dateTime ) {
        if ( $dateTime ) {
            $dateTime = date_i18n( get_option( 'date_format' ), strtotime( $dateTime ) ) . ', ' .
                date_i18n( get_option( 'time_format' ), strtotime( $dateTime ) );
        }

        return $dateTime;
    } // getFormattedDateTime

    /**
     * Get saved booking-data, using in Payment Cancelling via PayPal
     *
     * @return array
     */
    public static function getTemporaryBookingData() {
        $tmp_booking_data = array();

        if ( isset( $_SESSION[ 'tmp_booking_data' ] ) ) {
            $tmp_booking_data = unserialize( $_SESSION[ 'tmp_booking_data' ] );
            if ( is_array( $tmp_booking_data ) ) {
                $tmp_booking_data = (object)$tmp_booking_data;
            }
            // accessing private properties of AB_UserBookingData instance
            $tmp_booking_data = get_object_vars( json_decode( preg_replace(
                '/\\\\u([0-9a-f]{4})|'.get_class( $tmp_booking_data ).'/i', '', json_encode( (array) $tmp_booking_data ) )
            ) );
        }

        return $tmp_booking_data;
    } // getTemporaryBookingData


    /**
     * Fill send_notifications_cron.sh with actual data
     */
    public static function rewriteCronScript( $install = true ) {
        $cron_sh  = plugin_dir_path( __FILE__ ) . 'send_notifications_cron.sh';
        $cron_php = plugin_dir_path( __FILE__ ) . 'send_notifications_cron.php';

        if ( $install ) { // on installing plugin
            $cron_sh_content = file_get_contents( $cron_sh );
            if ( preg_match( '/doc_root/', $cron_sh_content ) ) {
                $cron_sh_content = preg_replace( '/doc_root/', $_SERVER[ 'DOCUMENT_ROOT' ], $cron_sh_content );
            }
            if ( preg_match( '/full_path/', $cron_sh_content ) ) {
                $cron_sh_content = preg_replace( '/full_path/', $cron_php, $cron_sh_content );
            }
            file_put_contents( $cron_sh, $cron_sh_content );
        } else { // on uninstalling plugin
            file_put_contents( $cron_sh, "#!/bin/sh\ncd doc_root\nphp -f full_path" );
        }
    } // rewriteCronScript

    /**
     * Plugin activation hooks no longer fire for updates, so we need to handle this manually
     *
     * @see http://make.wordpress.org/core/2010/10/27/plugin-activation-hooks-no-longer-fire-for-updates/
     * @return int
     */
    public static function cronScriptShouldBeRewritten() {
        return preg_match( '/doc_root/', file_get_contents( plugin_dir_path( __FILE__ ) . 'send_notifications_cron.sh' ) );
    } // cronScriptShouldBeRewritten

} // AB_CommonUtils