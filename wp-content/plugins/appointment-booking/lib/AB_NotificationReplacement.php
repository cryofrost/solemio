<?php
 
class AB_NotificationReplacement {

    private $client_name = '';

    private $client_phone = '';

    private $client_email = '';

    private $client_notes = '';

    private $staff_name = '';

    private $appointment_time = '';

    private $service_name = '';

    private $new_appointment_link = '';

    private $company_name = '';

    private $company_logo = '';

    private $company_address = '';

    private $company_phone = '';

    private $company_website = '';

    private $next_day_agenda = '';

    private $today_payments = '';

    public function __construct() {
        $this->company_name    = get_option( 'ab_settings_company_name' );
        $this->company_logo    = get_option( 'ab_settings_company_logo_url' );
        $this->company_address = nl2br( get_option( 'ab_settings_company_address' ) );
        $this->company_phone   = get_option( 'ab_settings_company_phone' );
        $this->company_website = get_option( 'ab_settings_company_website' );
    }

    public function setAppointmentTime( $appointment_time ) {
        $this->appointment_time = $appointment_time;
    }

    public function setClientName( $client_name ) {
        $this->client_name = $client_name;
    }

    public function setClientPhone( $client_phone ) {
        $this->client_phone = $client_phone;
    }

    public function setClientEmail( $client_email ) {
        $this->client_email = $client_email;
    }

    public function setClientNotes( $client_notes ) {
        $this->client_notes = $client_notes;
    }

    public function setNewAppointmentLink( $new_appointment_link ) {
        $this->new_appointment_link = $new_appointment_link;
    }

    public function setServiceName( $service_name ) {
        $this->service_name = $service_name;
    }

    public function setNextDayAgenda( $agenda ) {
        $this->next_day_agenda = $agenda;
    }

    public function setTodayPayments( $today_payments ) {
        $this->today_payments = $today_payments;
    }

    public function setStaffName( $staff_name ) {
        $this->staff_name = $staff_name;
    }

    public function replaceSubject( $text ) {
        $replacement = array(
            '[[COMPANY_NAME]]'  => $this->company_name,
//            '[[TOMORROW_DATE]]' => ''
        );

        return str_replace( array_keys( $replacement ), array_values( $replacement ), $text );
    }

    public function replace( $text ) {
        $company_logo = isset( $this->company_logo ) ? $this->company_logo : '';
        if ( ! empty( $company_logo ) ) {
            $this->company_logo = '<img src="' . $company_logo . '" />';
        }

        $replacement = array(
            '[[STAFF_NAME]]'         => $this->staff_name,
            '[[CLIENT_NAME]]'        => $this->client_name,
            '[[CLIENT_PHONE]]'       => $this->client_phone,
            '[[CLIENT_EMAIL]]'       => $this->client_email,
            '[[CLIENT_NOTES]]'       => $this->client_notes,
            '[[APPOINTMENT_TIME]]'   => date_i18n( get_option( 'time_format' ), strtotime( $this->appointment_time ) ),
            '[[APPOINTMENT_DATE]]'   => date_i18n( get_option( 'date_format' ), strtotime( $this->appointment_time ) ),
            '[[SERVICE_NAME]]'       => $this->service_name,
            '[[COMPANY_NAME]]'       => $this->company_name,
            '[[COMPANY_LOGO]]'       => $this->company_logo,
            '[[COMPANY_ADDRESS]]'    => $this->company_address,
            '[[COMPANY_PHONE]]'      => $this->company_phone,
            '[[COMPANY_WEBSITE]]'    => $this->company_website,
            '[[NEXT_DAY_AGENDA]]'    => $this->next_day_agenda,
            '[[TODAY_PAYMENTS]]'     => $this->today_payments
        );

        return str_replace( array_keys( $replacement ), array_values( $replacement ), $text );
    }
}
