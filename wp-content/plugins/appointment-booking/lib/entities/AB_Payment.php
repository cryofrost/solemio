<?php

/**
 * Class AB_Payment
 */
class AB_Payment extends AB_Entity {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->table_name = 'ab_payment';
        $this->schema = array(
            'id'          => array(),
            'created'     => array( 'format' => '%s' ),
            'type'        => array( 'format' => '%s', 'default' => 'paypal' ),
            'customer_id' => array( 'format' => '%d' ),
            'token'       => array( 'format' => '%s', 'default' => '' ),
            'status'      => array( 'format' => '%s' ),
            'transaction' => array( 'format' => '%s', 'default' => '' ),
            'total'       => array( 'format' => '%d' ),
        );
        parent::__construct();
    }

}