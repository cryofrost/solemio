<?php

/**
 * Class AB_Payment_Appointment
 */
class AB_Payment_Appointment extends AB_Entity {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->table_name = 'ab_payment_appointment';
        $this->schema = array(
            'id'              => array(),
            'date'            => array( 'format' => '%s' ),
            'payment_id'      => array( 'format' => '%d' ),
            'appointment_id'  => array( 'format' => '%d' ),
            'amount'          => array( 'format' => '%d' ),
        );
        parent::__construct();
    }

}
