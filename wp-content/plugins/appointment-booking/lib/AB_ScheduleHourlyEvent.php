<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

include 'tasks/AB_ScheduleJob.php';
include 'tasks/AB_TodayPaymentJob.php';
include 'tasks/AB_AgendaNextDayJob.php';
include 'tasks/AB_ReminderEveningAfterJob.php';
include 'tasks/AB_ReminderEveningNextMorningJob.php';
include 'tasks/AB_ReminderMorningTodayAfternoonJob.php';
include 'entities/AB_Email_Notification.php';

class AB_ScheduleHourEvent {

    /**
     * @var array|AB_ScheduleJob[]
     */
    private $collection = array();

    public function __construct() {
        $this->collection = array(
            new AB_TodayPaymentJob(),
            new AB_AgendaNextDayJob(),
            new AB_ReminderEveningAfterJob(),
            new AB_ReminderEveningNextMorningJob(),
            new AB_ReminderMorningTodayAfternoonJob()
        );
    }

    public function run() {
        foreach ( $this->collection as $job ) {
            $job->run();
        }
    }
}
