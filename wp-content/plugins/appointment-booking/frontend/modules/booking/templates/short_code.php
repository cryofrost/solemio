<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php /** @var AB_UserBookingData $userData */ ?>
<?php $form_id = uniqid(); ?>
<div id="ab-booking-form-<?php echo $form_id ?>" class="ab-booking-form" style="overflow: hidden"></div>
<script type="text/javascript">
    jQuery(function ($) {
        $('#ab-booking-form-<?php echo $form_id ?>').appointmentBooking({
            ajaxurl: '<?php echo admin_url('admin-ajax.php'); ?>',
            ab_attributes: <?php echo $attributes ?>,
            last_step: <?php echo (int)$booking_finished  ?>,
            cancelled: <?php echo (int)$booking_cancelled  ?>,
            form_id: '<?php echo $form_id ?>',
            l10n: '<?php echo get_locale() ?>'
        });
    });
</script>