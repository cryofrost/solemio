<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="ab-progress-tracker<?php if ( $this->payment_disabled ) echo ' ab-progress-tracker-four-steps'?>">
    <?php
        // Show Progress Tracker if enabled in settings
        if ( intval( get_option( 'ab_appearance_show_progress_tracker' ) == 1 ) ) {
            _e( $progress_tracker, 'ab' ) ;
        }
    ?>
</div>
<div style="margin-bottom: 15px!important;" class="ab-row-fluid">
  <?php
    $local_payment = get_option( 'ab_local_mode' ) == 1;
    $paypal_payment= get_option('ab_paypal_type') != 'disabled';
    _e( $message, 'ab' );
  ?>
</div>
<?php if ( $local_payment ) : ?>
    <div class="ab-row-fluid ab-list">
        <label>
            <input type="radio" class="ab-local-payment" checked="checked" name="payment-method-<?php echo $form_id ?>" value="local"/>
            <?php _e( 'I will pay locally', 'ab' ) ?>
        </label>
    </div>
<?php endif ?>
<?php if ( $paypal_payment ) : ?>
    <div class="ab-row-fluid ab-list">
        <label>
            <input type="radio" class="ab-paypal-payment"<?php if ( !$local_payment ) echo ' checked="checked" ' ?>name="payment-method-<?php echo $form_id ?>" value="paypal"/>
            <?php _e( 'I will pay now with PayPal', 'ab' ) ?>
            <input id="tmp_form_id" type="hidden" value="<?php echo $form_id ? $form_id : '' ?>" />
        </label>
    </div>
    <div class="ab-paypal-payment-button ab-row-fluid ab-nav-steps" <?php if ( $local_payment ) echo ' style="display:none" ' ?>>
        <?php $paypal->renderForm( $form_id ) ?>
    </div>
<?php endif ?>
<?php if ( $local_payment ) : ?>
    <div class="ab-local-pay-button ab-row-fluid ab-nav-steps">
	    <button class="ab-left ab-to-third-step ladda-button orange zoom-in" style="margin-right: 10px;">
		    <span class="ab_label"><?php _e( 'Back', 'ab' ) ?></span><span class="spinner"></span>
	    </button>
	    <button class="ab-right ab-final-step ladda-button orange zoom-in">
		    <span class="ab_label"><?php _e( 'Next', 'ab' ) ?></span><span class="spinner"></span>
	    </button>
    </div>
<?php endif ?>