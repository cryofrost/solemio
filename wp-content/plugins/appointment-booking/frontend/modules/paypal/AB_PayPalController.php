<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

include AB_PATH .'/lib/entities/AB_Payment.php';
include AB_PATH .'/lib/entities/AB_Payment_Appointment.php';

class AB_PayPalController extends AB_Controller {

    public function __construct() {
        parent::__construct();

        $this->paypal = new PayPal();

        // customer accepted the order on PayPal and return their info
        add_action( 'ab_paypal_order_accepted', array ( $this, 'paypalResponseSuccess' ) );
        // customer canceled the order on PayPal and redirected to it
        add_action( 'ab_paypal_order_cancel', array ( $this, 'paypalResponseCancel' ) );
    }

    public function paypalExpressCheckout() {
        $form_id = $this->getParameter( 'form_id' );
        if ( $form_id ) {
            // create a paypal object
            $paypal = new PayPal();
            $userData = new AB_UserBookingData( $form_id );
            $userData->load();

            if ( $userData->hasData() && $userData->getServiceId() ) {
                $employee = new AB_Staff();
                $employee->load( $userData->getStaffId() );

                $service = new AB_Service();
                $service->load( $userData->getServiceId() );

                $price = $this->getWpdb()->get_var( $this->getWpdb()->prepare(
                    'SELECT price FROM ab_staff_service WHERE staff_id = %d AND service_id = %d',
                        $employee->get( 'id' ), $service->get( 'id' )
                ) );

                // get the products information from the $_POST and create the Product objects
                $product = new stdClass();
                $product->name  = $service->get( 'title' );
                $product->desc  = $service->getTitleWithDuration();
                $product->price = $price;
                $product->qty   = 1;
                $paypal->addProduct($product);

                // and send the payment request
                $paypal->send_EC_Request( $form_id );
            } elseif ( isset( $_SESSION[ 'appointment_booking' ][ $form_id ], $_SESSION[ 'tmp_booking_data'] ) &&
                    $_SESSION[ 'appointment_booking' ][ $form_id ][ 'cancelled' ] === true
            ) {
                $tmp_booking_data = AB_CommonUtils::getTemporaryBookingData();

                if ( !empty( $tmp_booking_data ) ) {
                    $employee = new AB_Staff();
                    $employee->load( $tmp_booking_data[ 'staff_id' ][ 0 ] );

                    $service = new AB_Service();
                    $service->load( $tmp_booking_data[ 'service_id' ] );

                    $price = $this->getWpdb()->get_var( $this->getWpdb()->prepare( '
                            SELECT price FROM ab_staff_service WHERE staff_id = %d AND service_id = %d',
                        $employee->get( 'id' ), $service->get( 'id' )
                    ) );

                    // get the products information from the $_POST and create the Product objects
                    $product = new stdClass();
                    $product->name  = $service->get( 'title' );
                    $product->desc  = $service->getTitleWithDuration();
                    $product->price = $price;
                    $product->qty   = 1;
                    $paypal->addProduct($product);

                    // and send the payment request
                    $paypal->send_EC_Request( $form_id );
                }
            }
        }
    }

    /**
     * Customer created the token on PayPal and redirected to it
     * Create the order in the DB
     *
     * @param array $data
     */
    public function paypalCheckoutSuccess( $data ) {
        list( $response, $form_id ) = $data;

        // need session to get Total and Token

        $token = $_SESSION[ 'appointment_booking' ][ $form_id ][ 'pay_pal_response' ][ 0 ][ 'TOKEN' ];

        $userData = new AB_UserBookingData( $form_id );
        $userData->load();

        if ( $userData->hasData() && $userData->getServiceId() ) {
            /** @var AB_Appointment $appointment */
            $appointment = $userData->save();

            $payment = new AB_Payment();
            @$payment->set( 'token', urldecode($token) );
            $payment->set( 'status', 'Pending' );
            $payment->set( 'total', isset($_SESSION['ab_payment_total']) ? urlencode($_SESSION['ab_payment_total']) : '0.00' );
            $payment->set( 'customer_id', $appointment->get( 'customer_id' ) );
            $payment->set( 'created', date('Y-m-d H:i:s') );
            $payment->save();

            $paymentAppointment = new AB_Payment_Appointment();
            $paymentAppointment->set( 'date', date('Y-m-d H:i:s') );
            $paymentAppointment->set( 'payment_id', $payment->get( 'id' ) );
            $paymentAppointment->set( 'appointment_id', $appointment->get( 'id' ) );
            $paymentAppointment->set( 'amount', $payment->get( 'total' ) );
            $paymentAppointment->save();

            if ( isset( $_SESSION[ 'tmp_booking_data' ] ) ) {
                unset( $_SESSION[ 'tmp_booking_data' ] );
            }
            $_SESSION[ 'tmp_booking_data' ] = serialize( $userData );

            $userData->clean();
            $userData->setPaymentId( $payment->get( 'id' ) );
        } elseif ( isset( $_SESSION[ 'appointment_booking' ][ $form_id ], $_SESSION[ 'tmp_booking_data'] ) &&
            @$_SESSION[ 'appointment_booking' ][ $form_id ][ 'cancelled' ] === true
        ) {
            $tmp_booking_data = AB_CommonUtils::getTemporaryBookingData();

            if ( !empty( $tmp_booking_data ) ) {
                $userData = new AB_UserBookingData( $form_id );
                $userData->loadTemporaryForExpressCheckout();

                if ( $userData->hasData() && $userData->getServiceId() ) {
                    /** @var AB_Appointment $appointment */
                    $appointment = $userData->save();

                    $payment = new AB_Payment();
                    $payment->set( 'token', urldecode($response['TOKEN']) );
                    $payment->set( 'status', 'Pending' );
                    $payment->set( 'total', isset($_SESSION['ab_payment_total']) ? urlencode($_SESSION['ab_payment_total']) : '0.00' );
                    $payment->set( 'customer_id', $appointment->get( 'customer_id' ) );
                    $payment->set( 'created', date('Y-m-d H:i:s') );
                    $payment->save();

                    $paymentAppointment = new AB_Payment_Appointment();
                    $paymentAppointment->set( 'date', date('Y-m-d H:i:s') );
                    $paymentAppointment->set( 'payment_id', $payment->get( 'id' ) );
                    $paymentAppointment->set( 'appointment_id', $appointment->get( 'id' ) );
                    $paymentAppointment->set( 'amount', $payment->get( 'total' ) );
                    $paymentAppointment->save();

                    if ( isset( $_SESSION[ 'tmp_booking_data' ] ) ) {
                        unset( $_SESSION[ 'tmp_booking_data' ] );
                    }
                    $_SESSION[ 'tmp_booking_data' ] = serialize( $userData );

                    $userData->clean();
                    $userData->setPaymentId( $payment->get( 'id' ) );
                    $userData->setBookingFinished( true );
                }

            }
        }
    }

    /**
     * Express Checkout 'RETURNURL' process
     *
     * @param $data
     */
    public function paypalResponseSuccess( $data ) {
        self::paypalCheckoutSuccess($data);

        list( $details, $form_id ) = $data;

        $userData = new AB_UserBookingData( $form_id );
        $payment_id = $userData->getPaymentId();

        if ( $payment_id ) {
            $payment = new AB_Payment();
            $payment->load( $payment_id );
            $payment->set( 'transaction', urlencode( $details["TRANSACTIONID"] ) );
            $payment->set( 'status', 'Completed' );
            $payment->save();
        }
        $userData->setBookingFinished( true );
        @wp_safe_redirect( home_url( remove_query_arg( array( 'action', 'token', 'PayerID', 'form_id' ) ) ) );
        exit;
    }

    /**
     * Express Checkout 'CANCELURL' process
     *
     * @param $form_id
     */
    public function paypalResponseCancel( $form_id ) {
        $userData = new AB_UserBookingData( $form_id );
        $userData->load();
        $payment_id = $userData->getPaymentId();

        if ( $payment_id ) {
            $payment = new AB_Payment();
            $payment->load( $payment_id );
            $payment->set( 'status', 'Canceled' );
            $payment->save();
        }
        $userData->setBookingCancelled( true );
        @wp_safe_redirect( home_url( remove_query_arg( array( 'action', 'token', 'PayerID', 'form_id' ) ) ) );
        exit;
    }
}